<?php

return [
    'role_structure' => [
        'super_admin' => [
            'users' => 'c,r,u,d',
            'posts' => 'c,r,u,d',
            'articles' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'tags' => 'c,r,u,d',
            'activities' => 'c,r,u,d',
            'activity-posts' => 'c,r,u,d',
            'teams' => 'c,r,u,d',
            'memberes' => 'c,r,u,d',
            'board' => 'c,r,u,d',
            'uploader' => 'c,r,u,d',
            'testimonial' => 'c,r,u,d',
            'setting' => 'r,u',
        ],
        'moderator' => [],
    ],
    'permission_structure' => [

    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
