
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MUFIX</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(url('logo/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(url('logo/apple-icon-60x60.png')); ?>"> 
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(url('logo/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(url('logo/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(url('logo/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(url('logo/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(url('logo/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(url('logo/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(url('logo/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(url('logo/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(url('logo/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(url('logo/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(url('logo/favicon-16x16.png')); ?>">  
    <link rel="manifest" href="<?php echo e(url('manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(url('logo/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff">
    
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/dropzone.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/notify.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/bootstrap-datepicker.min.css')); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/font-awesome.min.css')); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/ionicons.min.css')); ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/jquery-jvectormap.css')); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/AdminLTE.min.css')); ?>">
    <!--  Select 2  -->
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/AdminLTE.min.css')); ?>">
    <!--  Datatabel  -->
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/datatable.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/select2.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/trix.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/lightbox.min.css')); ?>">
    
    
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/_all-skins.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dashboard/assets/css/mystyle.css')); ?>">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/layout/header.blade.php ENDPATH**/ ?>