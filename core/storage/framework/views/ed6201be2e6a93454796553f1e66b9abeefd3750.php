<?php
use Illuminate\Support\Facades\Route;
?>
<div id="overlayer" class="shader-gradient"></div>











<div id="loader-wrapper">
    <svg version="1.1" id="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
         y="0px"
         width="40%" height="40%" viewBox="0 0 219.29 212.717" style="enable-background:new 0 0 219.29 212.717;"
         xml:space="preserve">
                    <g id="orang">
                        <polygon class="st0" points="27.259,98.274 15.954,108.941 27.259,119.607 37.954,108.941 	"/>
                        <polygon class="st0" points="24.527,80.715 55.658,106.732 28.337,137.92 102.265,104.27 	"/>
                    </g>
        <g id="orang2">
            <polygon class="st1" points="163.023,142.954 151.619,142.13 151.831,153.902 163.094,154.221 	"/>
            <polygon class="st1" points="174.19,130.324 142.33,133.3 145.369,165.824 112.033,110.018 	"/>
        </g>
        <g id="Red">
            <polygon class="st2" points="107.385,16.488 96.081,27.154 107.385,37.821 118.081,27.154 	"/>
            <polygon class="st2" points="135.112,23.155 107.084,52.488 77.779,23.155 106.446,99.155 	"/>
        </g>
        <g id="Red2">
            <polygon class="st3" points="65.915,147.923 54.498,147.305 54.921,159.07 66.187,159.188 	"/>
            <polygon class="st3" points="75.481,172.046 75.688,140.047 43.022,139.837 101.867,112.214 	"/>
        </g>
        <g id="Green">
            <polygon class="st4" points="108.632,172.046 97.327,182.712 108.632,193.379 119.327,182.712 	"/>
            <polygon class="st4" points="78.416,187.059 106.571,157.848 135.748,187.307 107.413,111.184 	"/>
        </g>
        <g id="green2">
            <polygon class="st5" points="162.41,51.498 150.987,50.998 151.532,62.759 162.8,62.759 	"/>
            <polygon class="st5" points="139.779,39.13 139.779,71.13 172.444,71.13 113.779,99.13 	"/>
        </g>
        <g id="labin">
            <polygon class="st6" points="187.31,93.216 176.004,103.882 187.31,114.549 198.004,103.882 	"/>
            <polygon class="st6" points="189.749,132.547 160.159,104.792 189.222,75.218 113.488,104.583 	"/>
        </g>
        <g id="labin2">
            <polygon class="st7" points="63.19,51.147 51.756,51.191 52.861,62.914 64.115,62.376 	"/>
            <polygon class="st7" points="41.626,69.771 73.59,71.295 75.146,38.666 100.32,98.598 	"/>
        </g>
            </svg>
</div>

<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>


    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

        <div class="container">
            <div class="row align-items-center">

                <div class="col-6 col-xl-2">
                    <h1 class="mb-0 site-logo">
                        <a href="<?php echo e(route('home')); ?>" class="h2 mb-0" style="font-weight: bold">
                            <img class="navbar-brand " style="width: 60px;height: 60px;object-fit:contain" src=" <?php echo e(asset('setting/'.$setting->general_site_logo)); ?>">

                        </a>
                    </h1>
                </div>
                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative  text-right " role="navigation">

                        <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">

                            <li><a href="<?php echo e(route('home')); ?>" class="nav-link <?php echo e(((Route::currentRouteName())=='home')?'active' : ''); ?>">Home</a></li>
                            <li><a href="<?php echo e(route('about')); ?>" class="nav-link <?php echo e(((Route::currentRouteName())=='about')?'active' : ''); ?>">About Us</a></li>
                            <li class="has-children">
                                <a href="#" class="nav-link <?php echo e(((Route::currentRouteName())=='activities')?'active' : ''); ?>">Activities</a>
                                <ul class="dropdown">
                                    <?php $__currentLoopData = $activities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(route('blog_activity',$activity->id)); ?>" class="nav-link"><?php echo e($activity->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                            <li><a href="<?php echo e(route('blog')); ?>" class="nav-link <?php echo e(((Route::currentRouteName())=='blog')?'active' : ''); ?>">Blog</a></li>
                            <li><a href="<?php echo e(route('contact')); ?>" class="nav-link">Contact</a></li>
                          
<?php if(@env('APP_ENV')==='local'): ?>
<?php if(auth()->guard()->guest()): ?>
<li><a href="<?php echo e(route('login')); ?>" class="nav-link">Login</a></li>
<?php else: ?>
<li><a href="<?php echo e(route('welcome')); ?>" class="nav-link">Dashboard</a></li>
<?php endif; ?>
<?php endif; ?>
                            
                               
                        </ul>
                    </nav>
                </div>


                <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3 " style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right main-color"><span class="icon-menu h3"></span></a></div>

            </div>
        </div>

    </header>


        <div class="site-blocks-cover overlay" style="background-image: url(<?php echo e(asset('setting/'.$setting->community_main_image)); ?>);" data-aos="fade" id="home-section">
<?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/site/layout/navbar.blade.php ENDPATH**/ ?>