<?php $__env->startSection('content'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Categories
                <small><span class="badge btn-danger"><?php echo e($categories->count()); ?></span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="<?php echo e(route('categories.index')); ?>">Categories</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <div class="col-md-7" style="margin-bottom:15px">
                    <a href="<?php echo e(route('categories.create')); ?>" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Category
                    </a>
                </div>

                <!-- Form column -->
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Category Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-condensed">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Category</th>
                                    <th>Progress</th>
                                    <th >Posts</th>
                                    <th>Controllers</th>
                                </tr>
                                <input type="hidden" id="csrf" value="<?php echo e(csrf_token()); ?>">
                                <input type="hidden" id="model" value="categories">
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <tr>
                                     <td><?php echo e($index+1); ?></td>
                                     <td><?php echo e($category->name); ?></td>
                                     <td>
                                         <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" style="width: <?php echo e((($category->posts->count())/(($posts==0)?1:$posts))*100); ?>%"></div>
                                         </div>
                                     </td>
                                     <td><span class="badge bg-red"><?php echo e((($category->posts->count())/(($posts==0)?1:$posts))*100); ?>%</span></td>
                                     <td>
                                         <a class="btn btn-sm btn-success text-bold" href="<?php echo e(route('categories.edit',$category->id)); ?>">Edit <span class="fa fa-edit"></span></a>


                                         <div class="modal modal-danger fade" id="remove_board_<?php echo e($category->id); ?>" style="display: none;">
                                             <div class="modal-dialog">
                                                 <div class="modal-content">
                                                     <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                             <span aria-hidden="true">×</span></button>
                                                         <h4 class="modal-title">Delete Item</h4>
                                                     </div>
                                                     <div class="modal-body">
                                                         <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                     </div>
                                                     <form action="<?php echo e(route('categories.destroy',$category->id)); ?>" method="post" id="board_remove_<?php echo e($category->id); ?>">
                                                         <?php echo csrf_field(); ?>
                                                         <?php echo method_field('delete'); ?>
                                                     </form>
                                                     <div class="modal-footer">
                                                         <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                         <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_<?php echo e($category->id); ?>').submit()">Sure</button>
                                                     </div>
                                                 </div>
                                                 <!-- /.modal-content -->
                                             </div>
                                             <!-- /.modal-dialog -->
                                         </div>
                                         <button data-toggle="modal" data-target="#remove_board_<?php echo e($category->id); ?>" class="btn btn-sm btn-danger text-bold"  data-value="<?php echo e($category->id); ?>">Remove <span class="fa fa-trash"></span></button>

                                     </td>
                                 </tr>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/categories/index.blade.php ENDPATH**/ ?>