<header class="main-header">

    
    <a href="<?php echo e(route('home')); ?>" class="logo">
        
        <span class="logo-mini"><b>H</b>M</span>
        
        <span class="logo-lg"><b>Elsheref</b> v1.0</span>
    </a>

    
    <nav class="navbar navbar-static-top">
        
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                
                
                <!-- Notifications: style can be found in dropdown.less -->
                
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo e(asset('uploads/users/'.Auth::user()->image)); ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo e(auth()->user()->first_name); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo e(asset('uploads/users/'.Auth::user()->image)); ?>" class="img-circle" alt="User Image">

                            <p>
                                <?php echo e(Auth::user()->first_name.' '.Auth::user()->last_name); ?>

                                <small><?php echo e(Auth::user()->getRoles()[0]); ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo e(route('users.show_profile')); ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo e(route('logout')); ?>" class="btn btn-default btn-flat"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                
            </ul>
        </div>

    </nav>
</header>

<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
    <?php echo csrf_field(); ?>
</form>
<?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/layout/navbar.blade.php ENDPATH**/ ?>