<?php $__env->startSection('content'); ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('welcome')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="<?php echo e(route('users.index')); ?>">Users</a></li>
                <li class="active">Add</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-md-12">
                <?php echo $__env->make('dashboard.layout.error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New User</h3>
                        </div><!-- /.box-header -->


                        <!-- form start -->
                        <form role="form" method="post" action="<?php echo e(route('users.store')); ?>" enctype="multipart/form-data" >
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('post'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" placeholder=" Enter   First Name" value="<?php echo e(old('first_name')); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" placeholder=" Enter   Last Name" value="<?php echo e(old('last_name')); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email"> Email </label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter Email " value="<?php echo e(old('email')); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="avatars">Avatar</label>
                                    <input type="file" class="form-control" name="image" id="avatars">
                                </div>
                                <div class="form-group">
                                    <label for="phone"> Phone</label>
                                    <input type="text" class="form-control" name="phone" placeholder=" Enter    Phone" value="<?php echo e(old('phone')); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" placeholder=" Enter   Password">
                                </div>
                                <div class="form-group">
                                    <label for="confirmationpassword">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                                </div>

                                <div class="form-group">
                                    <label for="permissions">Permissions</label>
                                </div>
                            </div><!-- /.box-body -->



                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <?php
                                        function map($char){
                                            if ($char=='c'){
                                                return 'Create';
                                            }elseif ($char=='r'){
                                                return 'Read';
                                            }elseif ($char=='u'){
                                                return 'Update';
                                            }elseif ($char=='d'){
                                                return 'Delete';
                                            }elseif ($char=='e'){
                                                return 'Restore';
                                            }elseif ($char=='t'){
                                                return 'Trash';
                                            }elseif ($char=='i'){
                                                return 'ShowTrashed';
                                            }
                                        }
                                        ?>
                                        <?php $__currentLoopData = $config = config('laratrust_seeder.role_structure.super_admin'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="<?php echo e((strtolower($k)=='users')?'active':''); ?>"><a href="#<?php echo e($k); ?>" data-toggle="tab" class=""><?php echo e(ucfirst($k)); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                    <span class="clearfix"></span>
                                    <div class="tab-content">
                                        <?php $__currentLoopData = $config = config('laratrust_seeder.role_structure.super_admin'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="tab-pane <?php echo e((strtolower($k)=='users')?'active':''); ?>" id="<?php echo e($k); ?>">
                                                <div style="margin-right: 0px;padding-right: 0px">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                        <?php

                                              foreach (explode(',',$item) as $element){
                                                  $operation=map(strtolower($element));?>

                                                            <div class="form-group pull-left" style="margin-left: 10px">
                                                                <div class="checkbox-inline">

                                                                    <input type="checkbox" id="<?php echo e(strtolower($operation.'_'.$k)); ?>" value="<?php echo e(strtolower($operation.'_'.$k)); ?>" <?php if(in_array(strtolower($operation.'_'.$k),(array) old('permissions'))): ?> checked <?php endif; ?> name="permissions[]">
                                                                    <label for="<?php echo e(strtolower($operation.'_'.$k)); ?>"><?php echo e($operation.' '.$k); ?></label>
                                                                </div>
                                                            </div>

                                                <?php }?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div><!-- /.tab-pane -->
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </div><!-- /.tab-content -->
                                </div>
                            </div><!-- /.col -->






                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save</button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/users/create.blade.php ENDPATH**/ ?>