<?php $__env->startSection('content'); ?>
<?php $__env->startPush('js'); ?>
    <script src="https://cdn.tiny.cloud/1/4b2rd40qpdelgfmi3oofgocpnxs28fndbr734f0c12ctqmk0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#editor1',
            convert_urls: false,
            height:600,
            statusbar: false,

            plugins: 'emoticons directionality image  code print preview fullpage  searchreplace autolink directionality  visualblocks visualchars fullscreen image link    table charmap hr pagebreak nonbreaking  toc insertdatetime advlist lists textcolor wordcount   imagetools    contextmenu colorpicker textpattern media ',
            toolbar: 'ltr rtl emoticons formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat |undo redo | image code| link fontsizeselect  | ',


        });

    </script>
<?php $__env->stopPush(); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Posts
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="<?php echo e(route('posts.index')); ?>">Posts</a></li>
                <li class="active">Add</li>
            </ol>
        </section>
        <!--Main Content-->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <a href="<?php echo e(route('posts.index')); ?>" class="btn btn-primary btn-block margin-bottom">Back to Posts Index</a>

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Categories</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(route('categories.show',$category->id)); ?>"><i class="fa fa-bars"></i> <?php echo e($category->name); ?>

                                            <span class="label label-primary pull-right"><?php echo e($category->posts->count()); ?></span></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Posts</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="<?php echo e(route('posts.type',1)); ?>">
                                        <i class="fa fa-folder-o text-green"></i>Published
                                        <span class="label label-success pull-right"><?php echo e($published); ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo e(route('posts.type',0)); ?>">
                                        <i class="fa fa-archive text-blue"></i> Dreafted
                                        <span class="label label-primary pull-right"><?php echo e($drafted); ?></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tags</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(route('tags.show',$tag->id)); ?>"><i class="fa fa-tags"></i> <?php echo e($tag->name); ?>

                                            <span class="label label-warning pull-right"><?php echo e($tag->posts->count()); ?></span></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <?php echo $__env->make('dashboard.layout.error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">Add New Post
                                <small></small>
                            </h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                        title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body pad">
                            <form action="<?php echo e(route('posts.store')); ?>" method="POST" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="form-group">
                                    <label for="tags">Title</label>
                                    <input class="form-control" name="title" placeholder="Enter Post Title" value="<?php echo e(old('title')); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="tags">Description</label>
                                    <textarea class="form-control" name="description" rows="5" placeholder="Enter Post Description"><?php echo old('description'); ?></textarea>
                                </div>
                                <div class="form-group" hidden>
                                    <label for="direction">Direction</label>
                                  <select class="form-control" name="dir">
                                      <option selected disabled>Select Direction</option>
            
                                          <option value="ltr" <?php if(old('dir')==='ltr'): ?> selected <?php endif; ?>>LTR</option>
                                          <option value="rtl" <?php if(old('dir')==='rtl'): ?> selected <?php endif; ?>>RTL</option>
                                  
                                  </select>
                                </div>
                                <div class="form-group">
                                    <label for="tags">Category</label>
                                  <select class="form-control" name="category_id">
                                      <option selected disabled>Select Category</option>
                                  <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value="<?php echo e($category->id); ?>" <?php if(old('category_id')==$category->id): ?> selected <?php endif; ?>>
                                              <?php echo e($category->name); ?>

                                          </option>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </select>
                                </div>
                                <!-- /.form group -->
                                <div class="form-group">
                                    <label for="tags">Tags</label>
                                    <select class="form-control js-example-tokenizer" id="tags" multiple name="tags[]">

                                        <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($tag->id); ?>" <?php if(in_array($tag->id,(array)old('tags'))): ?> selected <?php endif; ?>>
                                                <?php echo e($tag->name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <textarea id="editor1" name="content" rows="10" cols="80" placeholder="Enter Your Post Here"><?php echo old('content'); ?></textarea>
                                <div class="form-group" style="margin-top: 10px">
                                    <div class="btn btn-warning btn-file">
                                        <i class="fa fa-paperclip"></i> Attachment
                                        <input type="file" name="image">
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane"></i> Publish</button>
                                        <label class="switch">
                                            <input type="checkbox" name="status">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/posts/create.blade.php ENDPATH**/ ?>