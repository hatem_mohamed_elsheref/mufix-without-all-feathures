





















<div class="modal modal-danger fade" id="remove_item" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Delete Item</h4>
            </div>
            <div class="modal-body">
               <h3> <P>Do You Sure To Remove This Item?</P></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline" id="remove_sure">Sure</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/layout/models.blade.php ENDPATH**/ ?>