<?php $__env->startSection('content'); ?>


    <a href="#about-section" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
    </a>
    </div>

    <div class="site-section cta-big-image" id="about-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">About Us</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
                   
                        <img loading="lazy" src="<?php echo e(asset('setting/'.$setting->community_main_image)); ?>" alt="Image" class="img-fluid img-rounded" style="border-radius: 10px ">
                   
                </div>
                <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
                    <div class="mb-4">
                        <h3 class="h3 mb-4 text-black main-color-text"><?php echo e($setting->community_title); ?></h3>
                        <p><?php echo e($setting->community_description); ?>.</p>
                    </div>
                    <div class="mb-4">
                        <ul class="list-unstyled ul-check success">
                            <li><?php echo e($setting->community_feature_1); ?></li>
                            <li><?php echo e($setting->community_feature_2); ?></li>
                            <li><?php echo e($setting->community_feature_3); ?></li>
                            <li><?php echo e($setting->community_feature_4); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--mix-->
    <section class="site-section" id="portfolio-sections">


        <div class="container">

            <div class="row mb-3">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Team 2020</h2>
                </div>
            </div>

            <div class="row justify-content-center mb-5" data-aos="fade-up">
                <div id="filters1" class="filters1">
                </div>
                <div id="filters" class="filters text-center button-group col-md-10">
                    <button class="btn btn-primary active" data-filter="*" id="all_btn_filter">All</button>
                    <button class="btn btn-primary" data-filter=".board" id="board_btn_filter">Board</button>
                    <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <button class="btn btn-primary" data-filter=".<?php echo e(strtolower($team->name)); ?>"><?php echo e($team->name); ?></button>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>

            <div id="posts" class="row no-gutter center-block">
                <?php $__currentLoopData = $memberes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $member->team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-6 col-lg-2  mb-4 item <?php echo e(strtolower($item->name)); ?> flex-fill text-center" data-aos-delay="">
                            <div class="team-member">
                                <figure class="center-block">
                                    <ul class="social">
                                        <li><a href="<?php echo e($member->fb); ?>"><span class="icon-facebook"></span></a></li>
                                        <li><a href="<?php echo e($member->tw); ?>"><span class="icon-twitter"></span></a></li>
                                        <li><a href="<?php echo e($member->linkedin); ?>"><span class="icon-linkedin"></span></a></li>

                                    </ul>
                                    <img loading="lazy" src="<?php echo e(asset('uploads/memberes/site/'.$member->image)); ?>" alt="Image" class="img-fluid xyz" style="width: 180px;height: 200px">
                                </figure>
                                <div class="p-3" style="height:120px">
                                    <h3 class="main-color-text" style="font-size:15px"><?php echo e($member->name); ?></h3>
                  <span class="position_" style="font-size:9px;word-spacing:0px;text-transform:uppercase;color:#cccccc"><?php echo e($item->name); ?> <?php echo e($member->role); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $board; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-6 col-lg-2 mb-4 item board flex-fill text-center" data-aos-delay="" hidden>
                        <div class="team-member">
                            <figure>
                                <ul class="social">
                                    <li><a href="<?php echo e($member->fb); ?>"><span class="icon-facebook"></span></a></li>
                                    <li><a href="<?php echo e($member->tw); ?>"><span class="icon-twitter"></span></a></li>
                                    <li><a href="<?php echo e($member->linkedin); ?>"><span class="icon-linkedin"></span></a></li>
                                </ul>
                                <img loading="lazy" src="<?php echo e(asset('uploads/memberes/site/'.$member->member[0]->image)); ?>" alt="Image" class="img-fluid xyz" style="width: 180px;height: 200px">
                            </figure>
                            <div class="p-3" style="height:120px">
                                <h3 class="main-color-text"  style="font-size:15px"><?php echo e($member->member[0]->name); ?></h3>
                                <span class="position_" style="font-size:9px;word-spacing:1px;text-transform:uppercase;color:#cccccc">Leader <?php echo e($member->team[0]->name); ?></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
             
            </div>
        </div>
<butt
     <?php echo $memberes->render(); ?>

    </section>

    <!--Our Services-->
    <section class="site-section border-bottom bg-light" id="services-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Our Services</h2>
                </div>
            </div>
            <div class="row align-items-stretch text-center">
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4 " data-aos="fade-up">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-startup"></span></div>
                        <div>
                            <h3 class="main-color-text">Business Consulting</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-graphic-design"></span></div>
                        <div>
                            <h3 class="main-color-text">Market Analysis</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-settings"></span></div>
                        <div>
                            <h3 class="main-color-text">User Monitoring</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary  main-color flaticon-idea"></span></div>
                        <div>
                            <h3 class="main-color-text">Insurance Consulting</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-smartphone"></span></div>
                        <div>
                            <h3 class="main-color-text">Financial Investment</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-head"></span></div>
                        <div>
                            <h3 class="main-color-text">Financial Management</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Yoytube Channel-->
    <section class="site-section" id="about-section1">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Youtube Channel</h2>
                </div>
            </div>
            <div class="row mb-5 text-center">

                <div class="col-lg-5 ml-auto mb-5 order-1 order-lg-2" data-aos="fade" data-aos="fade-up" data-aos-delay="">
                    <iframe width="100%" height="100%" src="<?php echo e($setting->youtube_video); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-lg-6 order-2 order-lg-1" data-aos="fade">

                    <div class="row">



                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color <?php echo e($setting->youtube_feature_2_icon); ?>"></span></div>
                                <div>
                                    <h3 class="main-color-text"><?php echo e($setting->youtube_feature_1_title); ?></h3>
                                    <p><?php echo e($setting->youtube_feature_1); ?>.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color <?php echo e($setting->youtube_feature_2_icon); ?>"></span></div>
                                <div>
                                    <h3 class="main-color-text"><?php echo e($setting->youtube_feature_2_title); ?></h3>
                                    <p><?php echo e($setting->youtube_feature_2); ?>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Application-->
    <section class="site-section" id="about-section111">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Android Application</h2>
                </div>
            </div>
            <div class="row mb-5 text-center">

                <div class="col-lg-5 ml-auto mb-5 order-1 order-lg-2" data-aos="fade" data-aos="fade-up" data-aos-delay="" style="text-align: center">
                    <img loading="lazy" src="<?php echo e(asset('setting/'.$setting->app_image)); ?>" alt="Image" class="img-fluid rounded application-image" >
                </div>
                <div class="col-lg-6 order-2 order-lg-1" data-aos="fade">
                    <div class="row">
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color <?php echo e($setting->app_feature_1_icon); ?>"></span></div>
                                <div>
                                    <h3 class="main-color-text"><?php echo e($setting->app_feature_1_title); ?></h3>
                                    <p><?php echo e($setting->app_feature_1); ?>.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color <?php echo e($setting->app_feature_2_icon); ?>"></span></div>
                                <div>
                                    <h3 class="main-color-text"><?php echo e($setting->app_feature_2_title); ?></h3>
                                    <p><?php echo e($setting->app_feature_2); ?>.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color <?php echo e($setting->app_feature_3_icon); ?>"></span></div>
                                <div>
                                    <h3 class="main-color-text"><?php echo e($setting->app_feature_3_title); ?></h3>
                                    <p><?php echo e($setting->app_feature_3); ?>.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color <?php echo e($setting->app_feature_4_icon); ?>"></span></div>
                                <div>
                                    <h3 class="main-color-text"><?php echo e($setting->app_feature_4_title); ?></h3>
                                    <p><?php echo e($setting->app_feature_4); ?>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Testimonial-->
    <section class="site-section testimonial-wrap" id="testimonials-section" data-aos="fade">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center">
                    <h2 class="section-title mb-3">Testimonials</h2>
                </div>
            </div>
        </div>
        <div class="slide-one-item home-slider owl-carousel">
            <?php $__currentLoopData = $testimonial; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div>
                    <div class="testimonial">

                        <blockquote class="mb-5">
                            <p>&ldquo;<?php echo e($testi->feedback); ?>&rdquo;</p>
                        </blockquote>

                        <figure class="mb-4 d-flex align-items-center justify-content-center">
                            <div><img loading="lazy" src="<?php echo e(asset('uploads/testimonial/site/'.$testi->image)); ?>" alt="Image" class="w-50 img-fluid mb-3" style="object-fit: cover;width: 50px;height: 50px"></div>
                            <p><?php echo e($testi->name); ?></p>
                        </figure>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </section>

    <!--Latest Posts-->
    <section class="site-section" id="blog-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Latest  Posts</h2>
                </div>
            </div>

            <div class="row blog-center">
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6 col-lg-4 mb-4 mb-lg-4 " data-aos="fade-up" data-aos-delay="100">
                        <div class="h-entry ">
                           <div style="width: 350px;height: 300px;margin: auto">
                        
                                <a href="<?php echo e(route('post',$post->id)); ?>">
                                    <img loading="lazy" src="<?php echo e(asset('uploads/posts/site/'.$post->image)); ?>" alt="Image" class="img-fluid xyz img-responsive" style="width: 100%;height: 100%;">
                                </a>
                            </div>
                            <h2 class="font-size-regular" style="margin-top: 10px"><a href="<?php echo e(route('post',$post->id)); ?>"><?php echo e($post->title); ?></a></h2>
                            <div class="meta mb-4"><?php echo e($post->user->first_name); ?> <span class="mx-2">&bullet;</span> <?php echo e(date_format(date_create($post->created_at),'M - D - Y')); ?><span class="mx-2">&bullet;</span> <a href="<?php echo e(route('blog_cat',$post->category->id)); ?>"><?php echo e($post->category->name); ?></a></div>
                            <p><?php echo substr($post->description,0,120); ?></p>
                            <?php if($post->dir==='rtl'): ?> 
                            <p style="text-align:end"><a href="<?php echo e(route('post',$post->id)); ?>">قراءة المزيد</a></p>
                             <?php else: ?> 
                             <p><a href="<?php echo e(route('post',$post->id)); ?>">Continue Reading...</a></p>
                             <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>

    <div id="projectFacts" class="sectionClass">
        <div class="fullWidth eight columns">
            <div class="projectFactsWrap ">
                <div class="item wow fadeInUpBig animated animated" data-number="20" style="visibility: visible;">
                    <i class="fa fa-plane"></i>
                    <p id="number1" class="number">20</p>
                    <span></span>
                    <p>Trips</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-number="100" style="visibility: visible;">
                    <i class="fa fa-camera"></i>
                    <p id="number2" class="number">100</p>
                    <span></span>
                    <p>Sessions</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-number="80" style="visibility: visible;">
                    <i class="fa fa-microphone"></i>
                    <p id="number3" class="number">80</p>
                    <span></span>
                    <p>Events</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-number="50" style="visibility: visible;">
                    <i class="fa fa-users"></i>
                    <p id="number4" class="number">50</p>
                    <span></span>
                    <p>Trainning </p>
                </div>
            </div>
        </div>
    </div>

    <!--Slider-->
    <div class="site-section cta-big-image" id="">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Memories</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5 your-class"  data-aos-delay="">
                  
                    <?php $__currentLoopData = $memories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $memory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div style="width: 320px;height: 320px;" ><img  loading="lazy" style=" margin:0 1px;width:350px" src="<?php echo e(asset('uploads/general_images/site/'.$memory->image)); ?>"  alt="Image" class="img-fluid xyz" style="width: 100%;height: 100%;border-radius:unset"></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    let memberesPaginator=document.getElementsByClassName('pagination')[0];
    memberesPaginator.className='pagination justify-content-center';
    memberesPaginator.style.marginLeft=null;
    
    console.log(memberesPaginator);
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('site.layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/site/index.blade.php ENDPATH**/ ?>