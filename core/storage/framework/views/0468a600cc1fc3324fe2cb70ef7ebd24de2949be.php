<?php $__env->startSection('content'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small><span class="badge btn-danger"><?php echo e($users->count()); ?></span></small>

            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('welcome')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="<?php echo e(route('users.index')); ?>">Users</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="<?php echo e(route('users.create')); ?>" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New User
                    </a>
                </div>
            </div>
            <input type="hidden" id="csrf" value="<?php echo e(csrf_token()); ?>">
            <input type="hidden" id="model" value="users">
            <div class="row" >
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Users Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Avatar</th>
                                    <th>Controllers</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($user->id); ?></td>
                                        <td><?php echo e($user->first_name.' '.$user->last_name); ?></td>
                                        <td><?php echo e($user->email); ?></td>
                                        <td><?php echo e($user->phone); ?></td>

                                        <td style="text-align: center"><img src="<?php echo e(asset('uploads/users/').'/'.$user->image); ?>" class="img-circle" style="width: 60px;height: 60px;"></td>
                                        <td>
                                            <a class="btn btn-sm btn-success text-bold" href="<?php echo e(route('users.edit',$user->id)); ?>">Edit <span class="fa fa-edit"></span></a>

                                            <div class="modal modal-danger fade" id="remove_board_<?php echo e($user->id); ?>" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="<?php echo e(route('users.destroy',$user->id)); ?>" method="post" id="board_remove_<?php echo e($user->id); ?>">
                                                            <?php echo csrf_field(); ?>
                                                            <?php echo method_field('delete'); ?>
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_<?php echo e($user->id); ?>').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            
                                            <button data-toggle="modal" data-target="#remove_board_<?php echo e($user->id); ?>" class="btn btn-sm btn-danger text-bold"  data-value="<?php echo e($user->id); ?>">Remove <span class="fa fa-trash"></span></button>

                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Avatar</th>
                                    <th>Controllers</th>
                                </tr>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/users/index.blade.php ENDPATH**/ ?>