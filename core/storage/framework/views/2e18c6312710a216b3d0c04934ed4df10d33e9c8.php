<?php if($errors->any()): ?>
    <div class="callout callout-danger">
        <h4><span class="fa fa-bullhorn"></span> Warnning</h4>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <p><i class="fa fa-minus"></i> <?php echo e($error); ?></p>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php endif; ?>
<?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/layout/error.blade.php ENDPATH**/ ?>