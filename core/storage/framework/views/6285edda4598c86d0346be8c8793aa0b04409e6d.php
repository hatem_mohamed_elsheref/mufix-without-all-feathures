<?php $__env->startSection('content'); ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Posts
                <small><span class="badge btn-danger"><?php echo e($posts->count()); ?></span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="<?php echo e(route('posts.index')); ?>">Posts</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="<?php echo e(route('posts.create')); ?>" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Post
                    </a>
                </div>
            </div>
            <div class="row" >
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Posts Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" id="csrf" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" id="model" value="posts">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($post->id); ?>.</td>
                                        <td><?php echo e($post->title); ?></td>
                                        <td><?php echo e($post->user->first_name); ?></td>
                                        <td><?php echo e($post->category->name); ?></td>
                                        <td><img src="<?php echo e(asset('uploads/posts/dashboard/'.$post->image)); ?>" style="width: 100px;height: 50px;"></td>
                                        <td><span class="label   <?php echo e(($post->status)?'label-success':'label-danger'); ?>"><?php echo e(($post->status)?'published':'Drafted'); ?></span></td>
                                        <td>

                                            <a class="btn btn-sm btn-success text-bold" href="<?php echo e(route('posts.edit',$post->id)); ?>">Edit <span class="fa fa-edit"></span></a>
                                           <div class="modal modal-danger fade" id="remove_form_<?php echo e($post->id); ?>" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="<?php echo e(route('posts.destroy',$post->id)); ?>" method="post" id="remove_item_<?php echo e($post->id); ?>">
                                                            <?php echo csrf_field(); ?>
                                                            <?php echo method_field('delete'); ?>
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('remove_item_<?php echo e($post->id); ?>').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                            
                                            <button data-toggle="modal" data-target="#remove_form_<?php echo e($post->id); ?>" class="btn btn-sm btn-danger text-bold"  data-value="<?php echo e($post->id); ?>">Remove <span class="fa fa-trash"></span></button>

                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\mufix\core\resources\views/dashboard/posts/index.blade.php ENDPATH**/ ?>