<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    protected $fillable=[
        'name','role','fb','tw','insta','linkedin','start','end','image'
    ];

    public function team(){
        return $this->belongsToMany('App\Team');
    }
    public function board(){
        return $this->belongsToMany('App\Board');
    }


}
