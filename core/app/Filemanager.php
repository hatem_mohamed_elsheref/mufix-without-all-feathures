<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filemanager extends Model
{
    protected $fillable=[
        'activity_id',
        'status',
        'image'
    ];
    public function activity(){
        return $this->belongsTo('App\Activity');
    }
}
