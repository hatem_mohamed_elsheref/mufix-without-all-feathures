<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityPost extends Model
{
    protected $fillable=[
        'title','description','content','status','image','user_id','activity_id','dir',
    ];
    public function activity(){
        return $this->belongsTo('App\Activity');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
