<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable=['name'];

    public function activityPosts(){
        return $this->hasMany('App\ActivityPost');
    }
    public function images(){
        return $this->hasMany('App\Filemanager');
    }
}
