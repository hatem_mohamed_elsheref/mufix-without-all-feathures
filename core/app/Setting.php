<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable=[
            'general_site_name',
            'general_site_logo',
            'community_title',
            'community_description',
            'community_feature_1',
            'community_feature_2',
            'community_feature_3',
            'community_feature_4',
            'community_main_image',
            'communication_fb',
            'communication_tw',
            'communication_inst',
            'communication_email',
            'communication_address',
            'youtube_feature_1_icon',
            'youtube_feature_1_title',
            'youtube_feature_1',
            'youtube_feature_2_icon',
            'youtube_feature_2_title',
            'youtube_feature_2',
            'youtube_video',
            'app_feature_1_icon',
            'app_feature_1_title',
            'app_feature_1',
            'app_feature_2_icon',
            'app_feature_2_title',
            'app_feature_2',
            'app_feature_3_icon',
            'app_feature_3_title',
            'app_feature_3',
            'app_feature_4_icon',
            'app_feature_4_title',
            'app_feature_4',
            'app_image'
    ];
}
