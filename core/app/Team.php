<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable=['name'];

    public function memberes(){
        return $this->belongsToMany('App\Member');
    }
    public function board(){
        return $this->belongsToMany('App\Board');
    }
}
