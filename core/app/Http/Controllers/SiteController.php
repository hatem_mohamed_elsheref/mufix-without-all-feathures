<?php

namespace App\Http\Controllers;
use App\Activity;
use App\ActivityPost;
use App\Http\Controllers\Dashboard\NotificationTrait;
use Illuminate\Http\Request;
use App\Board;
use App\Filemanager;
use App\Member;
use App\Setting;
use App\Tag;
use App\Team;
use App\Post;
use App\Testimonial;
use App\Category;
use Illuminate\Support\Facades\Cache;
class SiteController extends Controller
{
    use NotificationTrait;
    public function index(){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $CurrentYear=date('Y',time());

        $CurrentMemberes=Member::where('start','<=',$CurrentYear)->where('end','>=',$CurrentYear)->orderByDesc('created_at')->paginate(12);

        $data=array('setting'=>$setting,'memberes'=>$CurrentMemberes,'teams'=>Team::all(),'board'=>Board::where('year',$CurrentYear)->get());
        
        $data['testimonial']=Cache::rememberForever('testimonial', function () {
            return  Testimonial::where('status',true)->get();
        });
        $data['memories']=Cache::rememberForever('memories', function () {
            /*  $data['memories']=Filemanager::where('status',true)->where('activity_id',0)->get();*/
            return  Filemanager::where('status',true)->get();
        });
        $data['activities']=Cache::rememberForever('activities', function () {
            return  Activity::all();
        });
        $data['posts']=Post::where('status',true)->orderBy('id','desc')->take(3)->get();
        return view('site.index',$data);
    }
    public function getPost($id){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $post=Post::find($id);
        if (!$post){
            self::NotFound();
            return redirect(route('home'));
        }
        if (!$post->status){
            self::NotFound();
            return redirect(route('home'));
        }


        $data=array('setting'=>$setting,'post'=>$post,'categories'=>Category::all());
        $data['activities']=Activity::all();
        return view('site.post',$data);
    }
    public function getPostByCategory($id){
        $category=Category::find($id);
        if(!$category){
            self::NotFound();
            return redirect(route('home'));
        }
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $posts=$category->posts()->paginate(6);
        //view of collection of bosts as a result
        $data=array('setting'=>$setting,'posts'=>$posts);
        $data['type']=$category->name.' Posts';
        $data['activities']=Activity::all();
        return view('site.search',$data);
    }
    public function getPostByTag($id){
        $tag=Tag::find($id);
        if (!$tag){
            self::NotFound();
            return redirect(route('home'));
       }
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $posts=$tag->posts()->paginate(6);
        //view of collection of bosts as a result
        $data=array('setting'=>$setting,'posts'=>$posts);
        $data['type']=$tag->name.' Posts';
        $data['activities']=Activity::all();
        return view('site.search',$data);
    }
    public function blog(){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $posts=Post::orderBy('id','desc')->paginate(6);
        $data=array('setting'=>$setting,'posts'=>$posts);
        $data['activities']=Activity::all();
        return view('site.blog',$data);
    }
    public function activityBlog($id){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $activityblog=Activity::find($id);
        if (!$activityblog){
             self::NotFound();
             return redirect(route('home'));
        }
        $activityPosts=$activityblog->activityPosts()->paginate(6);

        $data=array('setting'=>$setting,'posts'=> $activityPosts);
        $data['activities']=Activity::all();
        $data['type']=$activityblog->name.' Posts';
        return view('site.activity',$data);
    }
    public function activityPost($id){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $activitypost=ActivityPost::find($id);
        if (!$activitypost){
            self::NotFound();
            return redirect(route('home'));
        }

        if (!$activitypost->status){
            self::NotFound();
            return redirect(route('home'));
        }

        $data=array('setting'=>$setting,'post'=> $activitypost);
        $data['activities']=Activity::all();
        return view('site.post_activity',$data);
    }
    public function about($year=null){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $CurrentYear=($year==null)?date('Y',time()):$year;

        $CurrentMemberes=Member::where('start','<=',$CurrentYear)->where('end','>=',$CurrentYear)->orderByDesc('created_at')->paginate(1);

        $data=array('memberes'=>$CurrentMemberes,'teams'=>Team::all(),'board'=>Board::where('year',$CurrentYear)->get());
        $data['setting']=$setting;
        $data['activities']=Activity::all();

        return view('site.about',$data);
    }
    public function contact(){
        $setting=Cache::rememberForever('settings', function () {
            return  Setting::find(1);
        });
        $data=array('setting'=>$setting);
        $data['activities']=Activity::all();

        return view('site.contact',$data);
    }
    /*
 public function test(){
        $setting=Setting::find(1);
        $data=array('setting'=>$setting);
        $data['activities']=Activity::all();

        return view('site.test',$data);
    }*/
}
