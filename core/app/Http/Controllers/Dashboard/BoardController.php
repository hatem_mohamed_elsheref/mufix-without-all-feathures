<?php

namespace App\Http\Controllers\Dashboard;


use App\Http\Controllers\Controller;
use App\Team;
use Illuminate\Http\Request;
use App\Member;
use App\Board;
use Illuminate\Support\Facades\DB;

class BoardController extends Controller
{
    use NotificationTrait;

    public function __construct()
    {
        $this->middleware('authorized:read_board')->only('index');
        $this->middleware('authorized:create_board')->only('create');
        $this->middleware('authorized:create_board')->only('store');
        $this->middleware('authorized:update_board')->only('edit');
        $this->middleware('authorized:update_board')->only('update');
        $this->middleware('authorized:delete_board')->only('destroy');
    }
    public function index(){
        $boards =Board::all();
//        dd($boards);
        return view('dashboard.board.index',compact('boards'));
    }
    public function create(){
        return view('dashboard.board.create',['teams'=>Team::all(),'memberes'=>Member::all()]);
    }
    public function store(Request $request){

        $request->validate([
            'team_id'=>'required|integer',
            'member_id'=>'required|integer',
            'year'=>'required|date_format:Y',
        ]);
        $_date=$request->except('team_id','member_id');

        $tmp=Board::all()->where('year',$request->year);
        foreach ($tmp as $t){
            if ($t->team[0]->id==$request->team_id){
                return back()->withErrors('Team Already Has Leader');
            }
        }

        $board=Board::create($_date);
        $board->member()->attach([$request->member_id]);
        $board->team()->attach([$request->team_id]);
        if ($board){
            self::SuccessCreate();
        }else{
            self::FailOperation();
        }
        return redirect(route('board.index'));
    }
    public function edit(Board $board){
        return view('dashboard.board.edit',['teams'=>Team::all(),'memberes'=>Member::all(),'board'=>$board]);
    }
    public function update(Request $request,$id){

        $request->validate([
            'team_id'=>'required|integer',
            'member_id'=>'required|integer',
            'year'=>'required|date_format:Y',
        ]);
        $_date=$request->except('team_id','member_id');

        $tmp=Board::all()->where('year',$request->year);
        foreach ($tmp as $t){
            if ($t->team[0]->id==$request->team_id){
                return back()->withErrors('Team Already Has Leader');
            }
        }

        $board=Board::find($id);
        $board->year=$request->year;
        $board->save();
        $board->member()->sync([$request->member_id]);
        $board->team()->sync([$request->team_id]);
        if ($board){
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }
        return redirect(route('board.index'));
    }
    public function destroy($id){
        $board=Board::find($id);
        $board->member()->detach();
        $board->team()->detach();
        $board->delete();
        self::SuccessDelete();
        return redirect(route('board.index'));
    }
}
