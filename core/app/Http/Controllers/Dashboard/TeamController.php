<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Team;
class TeamController extends Controller
{
    use NotificationTrait;
    public function __construct()
    {
        $this->middleware('authorized:read_teams')->only('index');
        $this->middleware('authorized:create_teams')->only('create');
        $this->middleware('authorized:create_teams')->only('store');
        $this->middleware('authorized:update_teams')->only('edit');
        $this->middleware('authorized:update_teams')->only('update');
        $this->middleware('authorized:delete_teams')->only('destroy');
    }
   public function index(){
       return view('dashboard.team.index',['teams'=>Team::all(),'memberes'=>Member::all()->count()]);
   }
   public function create(){
       return view('dashboard.team.create');
   }

   public function store(Request $request){
       $request->validate([
          'name'=>'required|string|max:191|unique:teams,name'
       ]);

       $team=Team::create($request->all());
       if ($team){
           self::SuccessCreate();
       }else{
           self::FailOperation();
       }
       return redirect(route('team.index'));
   }
   public function edit(Team $team){

       return view('dashboard.team.edit',['team'=>$team]);

   }
   public function update(Request $request,Team $team){
       $request->validate([
           'name'=>'required|string|max:191|unique:teams,name,'.$team->id
       ]);

       $team->update($request->all());
       if ($team){
           self::SuccessUpdate();
       }else{
           self::FailOperation();
       }
       return redirect(route('team.index'));
   }
   public function destroy(Team $team){
       $team->memberes()->detach();
       $boards=DB::table('board_team')->select('board_id')->where('team_id',$team->id)->get();
       $ids=[];
       foreach ($boards as $b){
           array_push($ids,$b->board_id);
       }
       if ($boards){
           DB::table('boards')->whereIn('id',$ids)->delete();
           DB::table('board_member')->whereIn('board_id',$ids)->delete();
           $team->board()->detach();
       }
       $team->delete();
       self::SuccessDelete();
       return redirect(route('team.index'));
   }
}
