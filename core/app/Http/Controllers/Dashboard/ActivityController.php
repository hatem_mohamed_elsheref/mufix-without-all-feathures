<?php

namespace App\Http\Controllers\Dashboard;

use App\ActivityPost;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Activity;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
class ActivityController extends Controller
{

    use NotificationTrait;

    public function __construct()
    {
        $this->middleware('authorized:read_activities')->only(['index']);
        $this->middleware('authorized:create_activities')->only(['create','store']);
        $this->middleware('authorized:update_activities')->only(['edit','update']);
        $this->middleware('authorized:delete_activities')->only('destroy');
    }

    public function index(){
        return view('dashboard.activities.index')->with('activities',Activity::all());
    }

    public function show(Activity $activity){
        $posts=$activity->activityPosts;
        return view('dashboard.activities.reports.search',compact('posts'));
    }
    public function create(){
        $activities=Activity::all();
        $published=ActivityPost::where('status',1)->count();
        $drafted=ActivityPost::where('status',0)->count();
        return view('dashboard.activities.create',[
            'activities'=>$activities,
            'published'=>$published,
            'drafted'=>$drafted
        ]);
    }
    public function store(Request $request){
        $request->validate([
            'name'=>'required|unique:activities,name'
        ]);
        $activity=Activity::create($request->all());
        Cache::forget('activities');
        if ($activity){

            self::SuccessCreate();

            return redirect(route('activities.index'));
        }
        else{
            self::FailOperation();

            return redirect(route('activities.create'));
        }
    }
    public function edit(Activity $activity){
        return view('dashboard.activities.edit',compact('activity'));
    }
    public function update(Request $request,Activity $activity){
        $request->validate([
            'name'=>'required|string|max:191|unique:activities,name,'.$activity->id
        ]);
        $activity->update($request->all());
        self::SuccessUpdate();
        Cache::forget('activities');
        return redirect(route('activities.index'));
    }
    public function destroy(Activity $activity){
  
        $Activityposts=ActivityPost::where('activity_id',$activity->id)->get();
        foreach($Activityposts as $post){
            Storage::disk('public_uploads')->delete('activities/dashboard/'.$post->image);
            Storage::disk('public_uploads')->delete('activities/site/'.$post->image);
            $post->delete();
        }
        
        foreach($activity->images as $image){
            Storage::disk('public_uploads')->delete('general_images/dashboard/'.$image->image);
            Storage::disk('public_uploads')->delete('general_images/site/'.$image->image);
            $image->delete();
        }


        $activity->delete();
        self::SuccessDelete();
        Cache::forget('activities');
        return redirect(route('activities.index'));
    }
}
