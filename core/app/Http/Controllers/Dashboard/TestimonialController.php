<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Testimonial;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cache;
class TestimonialController extends Controller
{
    use NotificationTrait;
    public function __construct()
    {
        $this->middleware('authorized:read_testimonial')->only('index');
        $this->middleware('authorized:create_testimonial')->only('create');
        $this->middleware('authorized:create_testimonial')->only('store');
        $this->middleware('authorized:update_testimonial')->only('edit');
        $this->middleware('authorized:update_testimonial')->only('update');
        $this->middleware('authorized:delete_testimonial')->only('destroy');
    }
    public function index(){
        return view('dashboard.testimonials.index')->with('testi',Testimonial::all());
    }
    public function create(){
        return view('dashboard.testimonials.create');
    }
    public function store(Request $request){

        $request->validate([
           'name'=>'required|string|max:191',
           'feedback'=>'required|string',
           'image'=>'required|image'
        ]);

        $img=Image::make($request->file('image'))->resize(300,300,function ($c){
           $c->aspectRatio();
        });
        $img->save('uploads/testimonial/dashboard/'.$request->file('image')->hashName());
        $img->save('uploads/testimonial/site/'.$request->file('image')->hashName());

        $testi=Testimonial::create([
            'name'=>$request->name,
            'feedback'=>$request->feedback,
            'image'=>$request->file('image')->hashName(),
            'status'=>($request->has('status'))?true:false
        ]);
        if ($testi){
            Cache::forget('testimonial');
            self::SuccessCreate();
        }else{
            self::FailOperation();
        }
        return redirect(route('testimonial.index'));
    }
    public function edit($id){
        $test=Testimonial::find($id);
        return view('dashboard.testimonials.edit',compact('test'));
    }
    public function update(Request $request,Testimonial $testimonial){

        $request->validate([
            'name'=>'required|string|max:191',
            'feedback'=>'required|string',
            'image'=>'image'
        ]);

        if ($request->hasFile('image')){
            Storage::disk('public_uploads')->delete('testimonial/dashboard/'.$testimonial->image);
            Storage::disk('public_uploads')->delete('testimonial/site/'.$testimonial->image);
            $img=Image::make($request->file('image'))->resize(300,300,function ($c){
                $c->aspectRatio();
            });
            $img->save('uploads/testimonial/dashboard/'.$request->file('image')->hashName());
            $img->save('uploads/testimonial/site/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=$testimonial->image;
        }

        $t=$testimonial->update([
            'name'=>$request->name,
            'feedback'=>$request->feedback,
            'image'=>$path,
            'status'=>($request->has('status'))?true:false
        ]);
        if ($t){
            Cache::forget('testimonial');
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }
        return redirect(route('testimonial.index'));

    }
    public function destroy($id){
        $test=Testimonial::find($id);
        Storage::disk('public_uploads')->delete('testimonial/dashboard/'.$test->image);
        Storage::disk('public_uploads')->delete('testimonial/site/'.$test->image);
        $test->delete();
        self::SuccessDelete();
//        return true;
        Cache::forget('testimonial');
        return redirect(route('testimonial.index'));
    }
}
