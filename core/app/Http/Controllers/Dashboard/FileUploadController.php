<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Activity;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Filemanager;
use Illuminate\Support\Facades\Cache;
class FileUploadController extends Controller
{
    use NotificationTrait;
    public function __construct()
    {
        $this->middleware('authorized:read_uploader')->only('index');
        $this->middleware('authorized:read_uploader')->only('gallary');
        $this->middleware('authorized:create_uploader')->only('create');
        $this->middleware('authorized:create_uploader')->only('dropzone');
        $this->middleware('authorized:update_uploader')->only('status');
        $this->middleware('authorized:delete_uploader')->only('destroy');
    }
    public function index(){
        $total=Filemanager::all();
    return view('dashboard.filemanager.index',compact('total'));
    }
    public function create(){
        $activities=Activity::all();
        return view('dashboard.filemanager.create',compact('activities'));
    }
    public function gallary(){
        $total=Filemanager::all();
        return view('dashboard.filemanager.gallary',compact('total'));
    }
    public function dropzone(Request $request,$id){

       // dd($request->file('file')->getClientOriginalName());

        $activity=$id;

        $img=Image::make($request->file('file'))->resize(300,300,function ($constraint){
            $constraint->aspectRatio();
        });
        $img->save('uploads/general_images/dashboard/'.$request->file('file')->hashName());
        //$request->file('file')->move(public_path('uploads/general_images/dashboard/'),$request->file('file')->hashName());

        $img=Image::make($request->file('file'));

        $img->save('uploads/general_images/site/'.$request->file('file')->hashName());
        $file=Filemanager::create([
            'status'=>false,
            'activity_id'=>$activity,
            'image'=>$request->file('file')->hashName()
        ]);
       if ($file){
           //self::SuccessCreate();
           Cache::forget('memories');
           echo json_encode(true);
       }
    }
    public function destroy($id){
        $img=Filemanager::findOrFail($id);
        Storage::disk('public_uploads')->delete('general_images/dashboard/'.$img->image);
        Storage::disk('public_uploads')->delete('general_images/site/'.$img->image);
        $img->delete();
        self::SuccessDelete();
//        return response('Success Delete',200);
        Cache::forget('memories');  

        return redirect()->back();
    }
    public function status($id,Request $request){
      //  dd($request);
        $img=Filemanager::find($id);
        if ($img->status==true){
            $img->status=false;
            $img->save();
        }else{
            $img->status=true;
            $img->save();
        }
//        if ($request->status==true)
//        $img->status=true;
//        else
//            $img->status=false;
            Cache::forget('memories');
        self::SuccessOperation();
//        return response('ok',200);
        return redirect()->back();
    }
}
