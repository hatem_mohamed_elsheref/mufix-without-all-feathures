<?php

namespace App\Http\Controllers\Dashboard;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\ActivityPost;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    use NotificationTrait;
    const DEFAULT_IMAGE='default-user.png';
    public function __construct()
    {
        $this->middleware('authorized:read_users')->only('index');
        $this->middleware('authorized:create_users')->only('create');
        $this->middleware('authorized:create_users')->only('store');
        $this->middleware('authorized:update_users')->only('edit');
        $this->middleware('authorized:update_users')->only('update');
        $this->middleware('authorized:delete_users')->only('destroy');
    }

    public function index()
    {
        $users=User::whereRoleIs('moderator')->get();
        return view('dashboard.users.index',compact('users'));
    }

    public function create()
    {
        return view('dashboard.users.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'phone' => 'required|string|max:191|unique:users,phone',
            'email' => 'required|string|email|max:191|unique:users,email',
            'password' => 'required|string|max:191|confirmed',
            'image' => 'image',
            'permissions' =>'required|min:1',
        ]);
        if ($request->hasFile('image') and !empty($request->file('image'))){
            $image=Image::make($request->file('image'));
            $image->save('uploads/users/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=self::DEFAULT_IMAGE;
        }
        $user=User::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>bcrypt($request->password),
            'image'=>$path
        ]);

        $user->attachRole('moderator');
        $user->syncPermissions($request->permissions);

        if ($user){
            self::SuccessCreate();
        }else{
            self::FailOperation();
        }

        return redirect(route('users.index'));
    }

    public function edit(User $user)
    {
        return view('dashboard.users.edit',compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'phone' => 'required|string|max:191|unique:users,phone,'.$user->id,
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'image' => 'image',
            'permissions' =>'required|min:1',
        ]);
        if ($request->hasFile('image') and !empty($request->file('image'))){
            if (!($user->image===self::DEFAULT_IMAGE)){
                Storage::disk('public_uploads')->delete('users/'.$user->image);
            }
            $image=Image::make($request->file('image'));

            $image->save('uploads/users/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=$user->image;
        }
        $user->update([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'image'=>$path
        ]);

        $user->syncPermissions($request->permissions);

        if ($user){
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }

        return redirect(route('users.index'));
    }

    public function destroy($id)
    {

        $user=User::find($id);

        if (!($user->image===self::DEFAULT_IMAGE)){
            Storage::disk('public_uploads')->delete('users/'.$user->image);
        }
        $user->detachPermissions();

        $posts=Post::where('user_id',$user->id)->get();
        foreach($posts as $post){
            Storage::disk('public_uploads')->delete('posts/dashboard/'.$post->image);
            Storage::disk('public_uploads')->delete('posts/site/'.$post->image);
            $post->delete();
        }

        $articles=Article::where('user_id',$user->id)->get();
        foreach($articles as $article){
            Storage::disk('public_uploads')->delete('articles/dashboard/'.$article->image);
            Storage::disk('public_uploads')->delete('articles/site/'.$article->image);
            $article->delete();
        }


        $Activityposts=ActivityPost::where('user_id',$user->id)->get();
        foreach($Activityposts as $post) {
            Storage::disk('public_uploads')->delete('activities/dashboard/' . $post->image);
            Storage::disk('public_uploads')->delete('activities/site/' . $post->image);
            $post->delete();
        }

        $user->delete();
        self::SuccessDelete();
        return redirect(route('users.index'));
    }

    public function showProfile(){
        return view('dashboard.users.profile',['user'=>auth()->user()]);
    }

    public function profile(Request $request){

        $user=auth()->user();
        $request->validate([
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'phone' => 'required|string|max:191|unique:users,phone,'.$user->id,
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'image' => 'image',
            'password' =>'',
        ]);
        if ($request->hasFile('image')){
            if (!($user->image===self::DEFAULT_IMAGE)){
                Storage::disk('public_uploads')->delete('users/'.$user->image);
            }
            $image=Image::make($request->file('image'));

            $image->save('uploads/users/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=self::DEFAULT_IMAGE;
        }
        if ($request->password != null){
            $user->update([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'image'=>$path,
                'password'=>bcrypt($request->password)
            ]);
            auth()->loginUsingId($user->id);
        }else{
            $user->update([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'image'=>$path,
                'password'=>$user->password
            ]);
        }


        if ($user){
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }

        return redirect(route('users.index'));
    }
}
