<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    use NotificationTrait;

    public function __construct()
    {
        $this->middleware('authorized:update_setting')->only('update');
        $this->middleware('authorized:read_setting')->only('index');
    }

    public function index(){
        
        $settings=Cache::rememberForever('settings', function () {
            return  Setting::all()->first();
        });
       
        return view('dashboard.setting.index')->with('setting',$settings);
    }
    public function update(Setting $setting,Request $request){
      
        $request->validate([
            'general_site_name'=>'required|string',
            'general_site_logo'=>'image',
            'community_title'=>'required|string',
            'community_description'=>'required|string',
            'community_feature_1'=>'required|string',
            'community_feature_2'=>'required|string',
            'community_feature_3'=>'required|string',
            'community_feature_4'=>'required|string',
            'community_main_image'=>'image',
            'communication_fb'=>'required|string',
            'communication_tw'=>'required|string',
            'communication_inst'=>'required|string',
            'communication_email'=>'required|string',
            'communication_address'=>'required|string',
            'youtube_feature_1_icon'=>'required|string',
            'youtube_feature_1_title'=>'required|string',
            'youtube_feature_1'=>'required|string',
            'youtube_feature_2_icon'=>'required|string',
            'youtube_feature_2_title'=>'required|string',
            'youtube_feature_2'=>'required|string',
            'youtube_video'=>'required|string',
            'app_feature_1_icon'=>'required|string',
            'app_feature_1_title'=>'required|string',
            'app_feature_1'=>'required|string',
            'app_feature_2_icon'=>'required|string',
            'app_feature_2_title'=>'required|string',
            'app_feature_2'=>'required|string',
            'app_feature_3_icon'=>'required|string',
            'app_feature_3_title'=>'required|string',
            'app_feature_3'=>'required|string',
            'app_feature_4_icon'=>'required|string',
            'app_feature_4_title'=>'required|string',
            'app_feature_4'=>'required|string',
            'app_image'=> 'image'
        ]);



        $default_logo="logo.png";
        $default_community="main.jpg";
        $default_app="app.png";
        if ($request->hasFile('general_site_logo')){
            if ($setting->general_site_logo!=$default_logo){
                Storage::disk('setting')->delete($setting->general_site_logo);
            }
            $img=Image::make($request->file('general_site_logo'));
            $img->save('setting/'.$request->file('general_site_logo')->hashName());
        }
        if ($request->hasFile('community_main_image')){
            if ($setting->community_main_image!=$default_community){
                Storage::disk('setting')->delete($setting->community_main_image);
            }
            $img=Image::make($request->file('community_main_image'));
            $img->save('setting/'.$request->file('community_main_image')->hashName());
        }
        if ($request->hasFile('app_image')){
            if ($setting->app_image!=$default_app){
                Storage::disk('setting')->delete($setting->app_image);
            }
            $img=Image::make($request->file('app_image'));
            $img->save('setting/'.$request->file('app_image')->hashName());
        }

        $validated_data=$request->except(['general_site_logo','community_main_image','app_image']);

        if ($request->hasFile('general_site_logo')){
            $logo_path=$request->file('general_site_logo')->hashName();
        }   else{
            $logo_path=$setting->general_site_logo;
        }
        if ($request->hasFile('community_main_image')){
            $community_path=$request->file('community_main_image')->hashName();
        }   else{
            $community_path=$setting->community_main_image;
        }
        if ($request->hasFile('youtube_video')){
            $youtube_path=$request->file('youtube_video')->hashName();
        }   else{
            $youtube_path=$setting->youtube_video;
        }
        if ($request->hasFile('app_image')){
            $app_path=$request->file('app_image')->hashName();
        }   else{
            $app_path=$setting->app_image;
        }
        $validated_data['general_site_logo']=$logo_path;
        $validated_data['community_main_image']=$community_path;
        $validated_data['app_image']=$app_path;

        $setting->update($validated_data);
        Cache::forget('settings');
           self::SuccessUpdate();
        return redirect(route('setting.index'));
//
    }
}
