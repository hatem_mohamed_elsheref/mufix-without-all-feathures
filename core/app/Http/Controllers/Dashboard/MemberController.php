<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\Member;
use App\Team;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MemberController extends Controller
{
    use NotificationTrait;
    public function __construct()
    {
        $this->middleware('authorized:read_memberes')->only('index');
        $this->middleware('authorized:create_memberes')->only('create');
        $this->middleware('authorized:create_memberes')->only('store');
        $this->middleware('authorized:update_memberes')->only('edit');
        $this->middleware('authorized:update_memberes')->only('update');
        $this->middleware('authorized:delete_memberes')->only('destroy');
    }
    public function index(){
        return view('dashboard.memberes.index',['memberes'=>Member::orderByDesc('id')->get()]);
    }
    public function create(){
        return view('dashboard.memberes.create',['teams'=>Team::all()]);
    }
    public function store(Request $request){

        //dd(phpinfo());
        //dd($request->file('image')->getMaxFileSize()/1024/1024);
       $request->validate([
           'name'=>'required|string|max:191|unique:members,name',
           'start'=>'required|date_format:Y|max:191',
           'end'=>'required|date_format:Y|max:191',
           'image'=>'required|mimes:jpg,png,jpeg|max:50000000',
           'teams'=>'required|array|min:1'
       ]);

//       $_data=$request->except('image');
       $_data=$request->except(['teams','image']);
       $img=Image::make($request->file('image'))->resize(300,300,function($c){
          $c->aspectRatio();
       });
       $img->save('uploads/memberes/dashboard/'.$request->file('image')->hashName());

       $img=Image::make($request->file('image'));
       $img->save('uploads/memberes/site/'.$request->file('image')->hashName());

       $_data['image']=$request->file('image')->hashName();
       $member=Member::create($_data);
       $member->team()->attach($request->teams);
       if ($member){
           self::SuccessCreate();
       }else{
           self::FailOperation();
       }
       return redirect(route('member.index'));
    }
    public function edit(Member $member){
        if (!$member){
            self::NotFound();
            return redirect()->back();
        }
        return view('dashboard.memberes.edit',compact('member'))->with('teams',Team::all());
    }
    public function update(Request $request,Member $member){
        $request->validate([
            'name'=>'required|string|max:191|unique:members,name,'.$member->id,
            'start'=>'required|date_format:Y|max:191',
            'end'=>'required|date_format:Y|max:191',
            'image'=>'image|max:50000',
            'teams'=>'required|array|min:1',
        ]);

        if ($request->hasFile('image')){
            Storage::disk('public_uploads')->delete('memberes/dashboard/'.$member->image);
            Storage::disk('public_uploads')->delete('memberes/site/'.$member->image);
            $img=Image::make($request->file('image'))->resize(300,300,function($c){
                $c->aspectRatio();
            });
            $img->save('uploads/memberes/dashboard/'.$request->file('image')->hashName());

            $img=Image::make($request->file('image'));
            $img->save('uploads/memberes/site/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=$member->image;
        }

//        $_data=$request->except('image');
        $_data=$request->except(['teams','image']);
        $_data['image']=$path;
        $member->update($_data);
        $member->team()->sync($request->teams);
        if ($member){
            self::Successupdate();
        }else{
            self::FailOperation();
        }
        return redirect(route('member.index'));
    }
    public function destroy(Member $member){
        Storage::disk('public_uploads')->delete('memberes/dashboard/'.$member->image);
        Storage::disk('public_uploads')->delete('memberes/site/'.$member->image);
        $member->team()->detach();
        $boards=DB::table('board_member')->select('board_id')->where('member_id',$member->id)->get();
        $ids=[];
        foreach ($boards as $b){
                array_push($ids,$b->board_id);
        }
        if ($boards){
           DB::table('boards')->whereIn('id',$ids)->delete();
           DB::table('board_team')->whereIn('board_id',$ids)->delete();
           $member->board()->detach();
       }
        $member->delete();
        self::SuccessDelete();
        return redirect(route('member.index'));
    }
}
