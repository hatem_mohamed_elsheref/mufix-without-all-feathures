<?php

namespace App\Http\Controllers\Dashboard;

use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
class PostController extends Controller
{
    use NotificationTrait;

    public function __construct()
    {

        // $this->middleware('authorized:read_posts')->only(['index','show']);
        $this->middleware('authorized:read_posts')->only(['index']);
        $this->middleware('authorized:create_posts')->only(['create','store']);
        $this->middleware('authorized:update_posts')->only(['edit','update']);
        $this->middleware('authorized:delete_posts')->only('destroy');
    }
    public function index()
    {
        $posts=Post::with('user','category','tags')->orderByDesc('id')->get();
       return view('dashboard.posts.index',compact('posts'));
    }

    public function create()
    {
        $tags=Tag::all();
        $categories=Category::all();
        $published=Post::where('status',1)->count();
        $drafted=Post::where('status',0)->count();
        return view('dashboard.posts.create',[
            'tags'=>$tags,
            'categories'=>$categories,
            'published'=>$published,
            'drafted'=>$drafted
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|string|max:191|unique:posts,title',
            'description'=>'required|string',
            'content'=>'required|string',
            'image'=>'required|image|mimes:png,jpg,jpeg',
            'category_id'=>'required|integer',
            'tags'=>'required|array|min:1|exists:tags,id',
            'dir'=>'in:ltr,rtl'
        ]);

        if($request->has('dir') and in_array($request->has('dir'),['ltr','rtl'])){
                $dir=$request->dir;
        }else{
            $dir='ltr';
        }


        $img_site=Image::make($request->file('image'));
        $img_dashboard=Image::make($request->file('image'));

        $img_dashboard->save('uploads/posts/dashboard/'.$request->file('image')->hashName());
        $img_site->save('uploads/posts/site/'.$request->file('image')->hashName());

        $post=Post::create([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'content'=>$request->input('content'),
            'image'=>$request->file('image')->hashName(),
            'category_id'=>$request->input('category_id'),
            'user_id'=>auth()->user()->id,
            'status'=>$request->has('status')?true:false,
            'dir'=>$dir
        ]);
        if ($post){
            $post->tags()->attach($request->input('tags'));
            self::SuccessCreate();
        }else{
            self::FailOperation();
        }
         return redirect()->route('posts.index');
    }

    // public function show(Post $post)
    // {
        
    //     $tags=Tag::all();
    //     $categories=Category::all();
    //     $published=Post::where('status',1)->count();
    //     $drafted=Post::where('status',0)->count();
    //     return view('dashboard.posts.show',[
    //         'tags'=>$tags,
    //         'categories'=>$categories,
    //         'published'=>$published,
    //         'drafted'=>$drafted,
    //         'post'=>$post
    //     ]);

    // }


    public function type($status){
        $posts=Post::where('status',$status)->orderByDesc('id')->get();
        if($posts){
            return view('dashboard.posts.search',compact('posts'));
        }else{
            abort(404);
        }
    }

    public function edit(Post $post)
    {
        
        $tags=Tag::all();
        $categories=Category::all();
        $published=Post::where('status',1)->count();
        $drafted=Post::where('status',0)->count();
        return view('dashboard.posts.edit',[
            'tags'=>$tags,
            'categories'=>$categories,
            'published'=>$published,
            'drafted'=>$drafted,
            'post'=>$post
        ]);
    }

    public function update(Request $request,Post $post)
    {
       
        $request->validate([
            'title'=>[
                'required','string','max:191',Rule::unique('posts','title')->ignore($post->id)
            ],
            'description'=>'required|string',
            'content'=>'required|string',
            'image'=>'image|mimes:png,jpg,jpeg',
            'category_id'=>'required|integer',
            'tags'=>'array|min:1|exists:tags,id',
            'dir'=>'in:ltr,rtl'
        ]);
            if($request->has('dir') and in_array($request->has('dir'),['ltr','rtl'])){
            $dir=$request->dir;
            }else{
            $dir='ltr';
            }
    
        if ($request->hasFile('image')){
            Storage::disk('public_uploads')->delete('posts/dashboard/'.$post->image);
            Storage::disk('public_uploads')->delete('posts/site/'.$post->image);
        
            $img_site=Image::make($request->file('image'));
            $img_dashboard=Image::make($request->file('image'));

            $img_dashboard->save('uploads/posts/dashboard/'.$request->file('image')->hashName());
            $img_site->save('uploads/posts/site/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=$post->image;
        }



        $post->update([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'content'=>$request->input('content'),
            'image'=>$path,
            'category_id'=>$request->input('category_id'),
            'user_id'=>auth()->user()->id,
            'status'=>$request->has('status')?true:false,
            'dir'=>$dir
        ]);
        if ($post){
            $post->tags()->sync($request->input('tags'));
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }
      
         return redirect()->route('posts.index');
    }

    public function destroy(Post $post)
    {
     
        Storage::disk('public_uploads')->delete('posts/dashboard/'.$post->image);
        Storage::disk('public_uploads')->delete('posts/site/'.$post->image);
        $post->tags()->detach();
        $post->delete();
        self::SuccessDelete();
        return redirect()->route('posts.index');
    }
}

