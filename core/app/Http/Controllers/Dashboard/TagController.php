<?php

namespace App\Http\Controllers\Dashboard;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tag;
use App\Post;

class TagController extends Controller
{
    use NotificationTrait;
    public function __construct()
    {
        $this->middleware('authorized:read_tags')->only('index');
        $this->middleware('authorized:read_tags')->only('show');
        $this->middleware('authorized:create_tags')->only('create');
        $this->middleware('authorized:create_tags')->only('store');
        $this->middleware('authorized:update_tags')->only('edit');
        $this->middleware('authorized:update_tags')->only('update');
        $this->middleware('authorized:delete_tags')->only('destroy');
    }

    public function index(){
        $tags=Tag::all();
        return view('dashboard.tags.index',compact('tags'))->with('posts',Post::all()->count());
    }
    public function show(Tag $tag){
        $posts=$tag->posts;
        return view('dashboard.posts.search',compact('posts'));
    }
    public function create(){
        return view('dashboard.tags.create');
    }
    public function store(Request $request){
        $request->validate([
            'name'=>'required|unique:tags,name'
        ]);
        $Tag=Tag::create($request->all());
        if ($Tag){
            self::SuccessCreate();
            return redirect(route('tags.index'));
        }
        else{
            self::FailOperation();
            return redirect(route('tags.create'));
        }
    }
    public function edit(Tag $tag){
        return view('dashboard.tags.edit',compact('tag'));
    }
    public function update(Request $request,Tag $Tag){
        $request->validate([
            'name'=>'required|string|max:191|unique:tags,name,'.$Tag->id
        ]);
        $Tag->update($request->all());
        self::SuccessUpdate();
        return redirect(route('tags.index'));
    }
    public function destroy(Tag $tag){
        $tag->posts()->detach();
        $tag->articles()->detach();
        $tag->delete();
        self::SuccessDelete();
        return redirect(route('tags.index'));
    }
}
