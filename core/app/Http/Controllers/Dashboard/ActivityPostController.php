<?php

namespace App\Http\Controllers\Dashboard;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

use App\ActivityPost;
use App\Activity;
class ActivityPostController extends Controller
{

    use NotificationTrait;


    public function __construct()
    {
//        $this->middleware('authorized:read_activity-posts')->only(['index','show']);
        $this->middleware('authorized:read_activity-posts')->only(['index']);
        $this->middleware('authorized:create_activity-posts')->only(['create','store']);
        $this->middleware('authorized:update_activity-posts')->only(['edit','update']);
        $this->middleware('authorized:delete_activity-posts')->only('destroy');
    }
    public function index()
    {

        $posts=ActivityPost::orderByDesc('id')->get();
//        dd($posts);
        return view('dashboard.activities.reports.index')->with('posts',$posts);
    }

    public function create()
    {
        $activities=Activity::all();
        $published=ActivityPost::where('status',1)->count();
        $drafted=ActivityPost::where('status',0)->count();
       
        return view('dashboard.activities.reports.create',[
            'activities'=>$activities,
            'published'=>$published,
            'drafted'=>$drafted
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|string|max:191|unique:activity_posts,title',
            'description'=>'required|string',
            'content'=>'required|string',
            'image'=>'required|image|mimes:png,jpg,jpeg',
            'activity_id'=>'required|integer',
            'dir'=>'in:ltr,rtl'
        ]);
        if($request->has('dir') and in_array($request->has('dir'),['ltr','rtl'])){
            $dir=$request->dir;
            }else{
            $dir='ltr';
            }

        $img_site=Image::make($request->file('image'));
        $img_dashboard=Image::make($request->file('image'));
        $img_dashboard->save('uploads/activities/dashboard/'.$request->file('image')->hashName());
        $img_site->save('uploads/activities/site/'.$request->file('image')->hashName());


        $activitypost=ActivityPost::create([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'content'=>$request->input('content'),
            'image'=>$request->file('image')->hashName(),
            'activity_id'=>$request->input('activity_id'),
            'user_id'=>auth()->user()->id,
            'status'=>$request->has('status')?true:false,
            'dir'=>$dir
        ]);
        if ($activitypost){
            self::SuccessCreate();
        }else{
            self::FailOperation();
        }

        return redirect(route('activity-posts.index'));
    }

//    public function show(ActivityPost $post)
//    {
//
//        $activities=Activity::all();
//        $published=ActivityPost::where('status',1)->count();
//        $drafted=ActivityPost::where('status',0)->count();
//        return view('dashboard.activities.reports.show',[
//            'activities'=>$activities,
//            'published'=>$published,
//            'drafted'=>$drafted,
//            'post'=>$post
//        ]);
//
//    }


    public function type($status){
        //published or drafted
        $posts=ActivityPost::where('status',$status)->get();
        return view('dashboard.activities.reports.search',compact('posts'));
    }

    public function edit($post)
    {
        $post=ActivityPost::findOrFail($post);
        $activities=Activity::all();
        $published=ActivityPost::where('status',1)->count();
        $drafted=ActivityPost::where('status',0)->count();
        return view('dashboard.activities.reports.edit',[

            'activities'=>$activities,
            'published'=>$published,
            'drafted'=>$drafted,
            'post'=>$post
        ]);
    }

    public function update(Request $request,$post)
    {
        $post=ActivityPost::findOrFail($post);
        $request->validate([
            'title'=>[
                'required','string','max:191',Rule::unique('activity_posts','title')->ignore($post->id)
            ],
            'description'=>'required|string',
            'content'=>'required|string',
            'image'=>'image|mimes:png,jpg,jpeg',
            'activity_id'=>'required|integer',
            'dir'=>'in:ltr,rtl'
        ]);
        if($request->has('dir') and in_array($request->has('dir'),['ltr','rtl'])){
            $dir=$request->dir;
            }else{
            $dir='ltr';
            }
        
        if ($request->hasFile('image')){
            Storage::disk('public_uploads')->delete('activities/dashboard/'.$post->image);
            Storage::disk('public_uploads')->delete('activities/site/'.$post->image);


            $img_site=Image::make($request->file('image'));
            $img_dashboard=Image::make($request->file('image'));

            $img_dashboard->save('uploads/activities/dashboard/'.$request->file('image')->hashName());
            $img_site->save('uploads/activities/site/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=$post->image;
        }



        $post->update([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'content'=>$request->input('content'),
            'image'=>$path,
            'activity_id'=>$request->input('activity_id'),
            'user_id'=>auth()->user()->id,
            'status'=>$request->has('status')?true:false,
            'dir'=>$dir
        ]);
        if ($post){
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }
        return redirect(route('activity-posts.index'));
    }

    public function destroy($post)
    {
        $post=ActivityPost::findOrFail($post);
        Storage::disk('public_uploads')->delete('activities/dashboard/'.$post->image);
        Storage::disk('public_uploads')->delete('activities/site/'.$post->image);
        $post->delete();
        self::SuccessDelete();
        return redirect(route('activity-posts.index'));

    }



}
