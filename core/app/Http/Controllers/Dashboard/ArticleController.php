<?php


namespace App\Http\Controllers\Dashboard;
use App\Category;
use App\Http\Controllers\Controller;
use App\Article;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
    use NotificationTrait;

    const STORAGE='articles';
    public function __construct()
    {
        $this->middleware('authorized:read_articles')->only(['index']);
        $this->middleware('authorized:create_articles')->only(['create','store']);
        $this->middleware('authorized:update_articles')->only(['edit','update']);
        $this->middleware('authorized:delete_articles')->only('destroy');
    }

    public function index()
    {
        $articles=Article::orderBy('id','desc')->get();
        return view('dashboard.articles.index',compact('articles'));
    }

    public function create()
    {
        $tags=Tag::all();
        $categories=Category::all();
        $drafted=Article::where('status',0)->count();
        $published=Article::where('status',1)->count();
        $data=['tags'=>$tags,'categories'=>$categories,'published'=>$published,'drafted'=>$drafted];
        return view('dashboard.articles.create',$data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|string|max:191|unique:articles,title',
            'description'=>'required|string',
            'content'=>'required|string',
            'image'=>'required|image|mimes:png,jpg,jpeg',
            'category_id'=>'required|integer',
            'tags'=>'required|array|min:1|exists:tags,id',
            'dir'=>'in:ltr,rtl'
        ]);

        if($request->has('dir') and in_array($request->has('dir'),['ltr','rtl'])){
            $dir=$request->dir;
        }else{
            $dir='ltr';
        }


        $img_site=Image::make($request->file('image'));
        $img_dashboard=Image::make($request->file('image'));

        $img_dashboard->save('uploads/articles/dashboard/'.$request->file('image')->hashName());
        $img_site->save('uploads/articles/site/'.$request->file('image')->hashName());

        $validatedData=$request->except(['_token','image','dir','tags']);
        $validatedData['dir']=$dir;
        $validatedData['image']=$request->file('image')->hashName();
        $validatedData['status']=$request->has('status')?true:false;
        $validatedData['user_id']=auth()->user()->id;
        $article=Article::create($validatedData);
        if ($article){
            $article->tags()->attach($request->input('tags'));
            self::SuccessCreate();
        }else{
            self::FailOperation();
        }
        return redirect()->route('articles.index');
    }


    public function edit(Article $article)
    {
        $tags=Tag::all();
        $categories=Category::all();
        $drafted=Article::where('status',0)->count();
        $published=Article::where('status',1)->count();
        $data=['tags'=>$tags,'categories'=>$categories,'published'=>$published,'drafted'=>$drafted,'article'=>$article];
        return view('dashboard.articles.edit',$data);
    }

    public function update(Request $request,Article $article)
    {
        $request->validate([
            'title'=>[
                'required','string','max:191',Rule::unique('articles','title')->ignore($article->id)
            ],
            'description'=>'required|string',
            'content'=>'required|string',
            'image'=>'image|mimes:png,jpg,jpeg',
            'category_id'=>'required|integer',
            'tags'=>'array|min:1|exists:tags,id',
            'dir'=>'in:ltr,rtl'
        ]);
        if($request->has('dir') and in_array($request->has('dir'),['ltr','rtl'])){
            $dir=$request->dir;
        }else{
            $dir='ltr';
        }

        if ($request->hasFile('image')){
            Storage::disk('public_uploads')->delete('articles/dashboard/'.$article->image);
            Storage::disk('public_uploads')->delete('articles/site/'.$article->image);

            $img_site=Image::make($request->file('image'));
            $img_dashboard=Image::make($request->file('image'));

            $img_dashboard->save('uploads/articles/dashboard/'.$request->file('image')->hashName());
            $img_site->save('uploads/articles/site/'.$request->file('image')->hashName());
            $path=$request->file('image')->hashName();
        }else{
            $path=$article->image;
        }

        $validatedData=$request->except(['_token','_method','tags','dir','image']);
        $validatedData['image']=$path;
        $validatedData['dir']=$dir;
        $validatedData['user_id']=$article->user_id;
        $validatedData['status']=$request->has('status')?true:false;

        if ($article->update($validatedData)){
            $article->tags()->sync($request->input('tags'));
            self::SuccessUpdate();
        }else{
            self::FailOperation();
        }
        return redirect()->route('articles.index');
    }

    public function destroy(Article $article)
    {

        Storage::disk('public_uploads')->delete('articles/dashboard/'.$article->image);
        Storage::disk('public_uploads')->delete('articles/site/'.$article->image);
        $article->tags()->detach();
        $article->delete();
        self::SuccessDelete();
        return redirect()->route('articles.index');
    }
}