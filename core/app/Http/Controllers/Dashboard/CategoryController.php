<?php

namespace App\Http\Controllers\Dashboard;

use App\Article;
use App\Category;
use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    use NotificationTrait;
    public function __construct()
    {
        $this->middleware('authorized:read_categories')->only('index');
        $this->middleware('authorized:create_categories')->only('create');
        $this->middleware('authorized:create_categories')->only('store');
        $this->middleware('authorized:update_categories')->only('edit');
        $this->middleware('authorized:update_categories')->only('update');
        $this->middleware('authorized:delete_categories')->only('destroy');
    }
    public function index(){
        $categories=Category::all();
         return view('dashboard.categories.index',compact('categories'))->with('posts',Post::all()->count());
    }
 
    public function show($category){

        $cat=Category::find($category);
     
        if($cat){
            $posts=$cat->posts; 
        }else{
            $posts = collect([]);
        }
       
        return view('dashboard.posts.search',compact('posts'));
    }
    public function create(){
        return view('dashboard.categories.create');
    }
    public function store(Request $request){
        $request->validate([
            'name'=>'required|unique:categories,name'
        ]);
        $category=Category::create($request->all());
        if ($category){

            notify()->success('Success Operation!');

            return redirect(route('categories.index'));
        }
        else{
            self::FailOperation();
            return redirect(route('categories.create'));
        }
    }
    public function edit(Category $category){
        return view('dashboard.categories.edit',compact('category'));
    }
    public function update(Request $request,Category $category){
      $request->validate([
         'name'=>'required|string|max:191|unique:categories,name,'.$category->id
      ]);
      $category->update($request->all());
      self::SuccessUpdate();
      return redirect(route('categories.index'));
    }
    public function destroy(Category $category){

        $posts=Post::where('category_id',$category->id)->get();
        foreach($posts as $post){
            Storage::disk('public_uploads')->delete('posts/dashboard/'.$post->image);
            Storage::disk('public_uploads')->delete('posts/site/'.$post->image);
            $post->delete();
        }

        $articles=Article::where('category_id',$category->id)->get();
        foreach($articles as $article){
            Storage::disk('public_uploads')->delete('articles/dashboard/'.$article->image);
            Storage::disk('public_uploads')->delete('articles/site/'.$article->image);
            $article->delete();
        }

        $category->delete();
        self::SuccessDelete();
        return redirect(route('categories.index'));
    }
}
