<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Dashboard\NotificationTrait;
use Closure;

class AuthorizationChecker
{
    use NotificationTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permission)
    {

        if (auth()->user()->hasPermission($permission)){
            return $next($request);
        }
        else{
            self::Authorization();
            return redirect(route('welcome'));
        }

    }
}
