<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
   protected $fillable=['year'];
   public function team(){
       return $this->belongsToMany('App\Team');
   }
   public function member(){
       return $this->belongsToMany('App\Member');

   }
}
