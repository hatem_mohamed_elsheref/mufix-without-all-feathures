@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Board
                <small><span class="badge btn-danger">{{$boards->count()}}</span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('board.index')}}">Board</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <div class="col-md-7" style="margin-bottom:15px">
                    <a href="{{route('board.create')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Board
                    </a>
                </div>

                <!-- Form column -->
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Board Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-condensed">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Team</th>
                                    <th>Leader</th>
                                    <th>Year</th>
                                    <th>Controllers</th>
                                </tr>
                                <input type="hidden" id="csrf" value="{{csrf_token()}}">
                                <input type="hidden" id="model" value="board">
                                @foreach($boards as $index=>$board)
                                    <tr>
{{--                                        {{dd($board->member[0])}}--}}
                                        <td>{{$board->id}}</td>
                                        <td>{{$board->team[0]->name}}</td>
                                      
                                        <td>{{$board->member[0]->name}}</td>
                                        <td>{{$board->year}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-success text-bold" href="{{route('board.edit',$board->id)}}">Edit <span class="fa fa-edit"></span></a>
{{--                                             <button  class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$tag->id}}">Remove <span class="fa fa-trash"></span></button>--}}

                                            <div class="modal modal-danger fade" id="remove_board_{{$board->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="{{route('board.destroy',$board->id)}}" method="post" id="board_remove_{{$board->id}}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_{{$board->id}}').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
{{--                                            <button onclick="remove({{$board->id}})" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$board->id}}">Remove <span class="fa fa-trash"></span></button>--}}
                                            <button data-toggle="modal" data-target="#remove_board_{{$board->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$board->id}}">Remove <span class="fa fa-trash"></span></button>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>


@endsection
