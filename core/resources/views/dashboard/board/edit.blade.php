@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Board
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('board.index')}}">Board</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-sm-7">
                @include('dashboard.layout.error')
                <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Board</h3>
                        </div><!-- /.box-header -->



                        <!-- form start -->
                        <form role="form" method="post" action="{{route('board.update',$board->id)}}">
                            @csrf
                            @method('put')
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="tags">Team</label>
                                    <select class="form-control" name="team_id">
                                        <option selected disabled>Select Team</option>
                                        @foreach($teams as $team)
                                            <option value="{{$team->id}}" @if($board->team[0]->id==$team->id) selected @endif>
                                                {{$team->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form group -->
                                <div class="form-group">
                                    <label for="tags">Member</label>
                                    <select class="form-control" name="member_id">
                                        <option selected disabled>Select Team</option>
                                        @foreach($memberes as $member)
                                            <option value="{{$member->id}}" @if($board->member[0]->id==$member->id) selected @endif>
                                                {{$member->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form group -->
                                <div class="form-group">
                                    <label>Year:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" placeholder="select Year" class="form-control pull-right" id="datepicker" name="year" value="{{$board->year}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success">Edit <i class="fa fa-edit"></i> </button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->

            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>



@endsection
