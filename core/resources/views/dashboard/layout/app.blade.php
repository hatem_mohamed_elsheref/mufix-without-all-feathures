
    @include('dashboard.layout.header')

    @include('dashboard.layout.navbar')

    @include('dashboard.layout.models')


    @include('dashboard.layout.sidebar')

    @yield('content')

    @include('dashboard.layout.footer')
