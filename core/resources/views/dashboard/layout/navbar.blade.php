<header class="main-header">

    {{--  <!-- Logo -->  --}}
    <a href="{{route('home')}}" class="logo">
        {{--  <!-- mini logo for sidebar mini 50x50 pixels -->  --}}
        <span class="logo-mini"><b>H</b>M</span>
        {{--  <!-- logo for regular state and mobile devices -->  --}}
        <span class="logo-lg"><b>Elsheref</b> v1.0</span>
    </a>

    {{--  <!-- Header Navbar: style can be found in header.less -->  --}}
    <nav class="navbar navbar-static-top">
        {{--  <!-- Sidebar toggle button-->  --}}
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        {{--  <!-- Navbar Right Menu -->  --}}
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                {{--  <!-- Messages: style can be found in dropdown.less-->  --}}
                {{--  <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">0</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 0 messages</li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>  --}}
                <!-- Notifications: style can be found in dropdown.less -->
                {{--  <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">0</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 0 notifications</li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>  --}}
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('uploads/users/'.Auth::user()->image)}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{auth()->user()->first_name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset('uploads/users/'.Auth::user()->image)}}" class="img-circle" alt="User Image">

                            <p>
                                {{Auth::user()->first_name.' '.Auth::user()->last_name}}
                                <small>{{Auth::user()->getRoles()[0]}}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('users.show_profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('logout')}}" class="btn btn-default btn-flat"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                {{--  <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>  --}}
            </ul>
        </div>

    </nav>
</header>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
