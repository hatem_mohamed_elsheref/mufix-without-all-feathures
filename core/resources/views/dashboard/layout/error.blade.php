@if($errors->any())
    <div class="callout callout-danger">
        <h4><span class="fa fa-bullhorn"></span> Warnning</h4>
        @foreach($errors->all() as $error)
            <p><i class="fa fa-minus"></i> {{$error}}</p>
        @endforeach
    </div>
@endif
