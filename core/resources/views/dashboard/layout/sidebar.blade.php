<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('uploads/users/'.Auth::user()->image)}}" class="img-circle" alt="User Image" style="width: 50px;height: 50px;object-fit: cover">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name() }}</p>
                {{--  <p>Hatem Mohammed</p>  --}}
                <a href="{{route('welcome')}}"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
            @if(auth()->user()->hasPermission('read_users'))
                <li>
                    <a href="{{route('users.index')}}">
                        <i class="fa fa-shield"></i> <span>Users</span>
                    </a>
                </li>
                @endif

            @if(auth()->user()->hasPermission('read_posts'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil"></i> <span>Blog</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @if(auth()->user()->hasPermission('read_posts'))
                        <li><a href="{{route('posts.index')}}"><i class="fa fa-circle-o"></i> Posts</a></li>
                    @endif
                        @if(auth()->user()->hasPermission('read_categories'))
                            <li><a href="{{route('categories.index')}}"><i class="fa fa-circle-o"></i> Categories</a></li>
                        @endif
                        @if(auth()->user()->hasPermission('read_tags'))
                            <li><a href="{{route('tags.index')}}"><i class="fa fa-circle-o"></i> Tags</a></li>
                        @endif


                </ul>
            </li>
            @endif
            @if(auth()->user()->hasPermission('read_memberes'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Memberes</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @if(auth()->user()->hasPermission('read_teams'))
                        <li><a href="{{route('team.index')}}"><i class="fa fa-circle-o"></i> Teams</a></li>
                    @endif
                    @if(auth()->user()->hasPermission('read_memberes'))
                            <li><a href="{{route('member.index')}}"><i class="fa fa-circle-o"></i> Memberes</a></li>
                    @endif
                    @if(auth()->user()->hasPermission('read_board'))
                            <li><a href="{{route('board.index')}}"><i class="fa fa-circle-o"></i> Board</a></li>
                    @endif
                </ul>
            </li>
            @endif
            @if(auth()->user()->hasPermission('read_activities'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cubes"></i> <span>Activities</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('activities.index')}}"><i class="fa fa-circle-o"></i> Activities</a></li>
                    <li><a href="{{route('activity-posts.index')}}"><i class="fa fa-circle-o"></i> Reports</a></li>

                </ul>
            </li>
            @endif
            @if(auth()->user()->hasPermission('read_uploader'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder-o"></i> <span>File Manager</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('filemanager')}}"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="{{route('gallary')}}"><i class="fa fa-photo"></i>Gallary</a></li>
                    <li><a href="{{route('upload')}}"><i class="fa fa-cloud-upload"></i>Uploader</a></li>

                </ul>
            </li>
            @endif
            @if(auth()->user()->hasPermission('read_testimonial'))
            <li>
                <a href="{{route('testimonial.index')}}">
                    <i class="fa fa-heart"></i> <span>Testimonial</span>
                    <span class="pull-right-container">
{{--              <small class="label pull-right bg-green">5</small>--}}
            </span>
                </a>
            </li>
            @endif
            @if(auth()->user()->hasPermission('read_setting'))
                <li>
                    <a href="{{route('setting.index')}}">
                        <i class="fa fa-cogs"></i> <span>Settings</span>
                    </a>
                </li>
                @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
