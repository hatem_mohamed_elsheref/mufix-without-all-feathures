@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Memberes
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('member.index')}}">Memberes</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="{{route('member.create')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Member
                    </a>
                </div>
            </div>
            <div class="row" >
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Memberes Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" id="csrf" value="{{csrf_token()}}">
                            <input type="hidden" id="model" value="member">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Team</th>
                                    <th>Role</th>
                                    <th>Image</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Controllers</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($memberes as $member)
                                    <tr>
                                        <td>{{$member->name}}</td>
                                        <td>
                                        @foreach($member->team as $index=>$item)
                                               {{ $item->name}}
                                                @if($index+1 <$member->team()->count())
                                                {{','}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$member->role}}</td>
                                        <td><img src="{{asset('uploads/memberes/dashboard/'.$member->image)}}" style="width:80px;height:50px;object-fit:cover"></td>
                                        <td>{{$member->start}}</td>
                                        <td>{{$member->end}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-success text-bold" href="{{route('member.edit',$member->id)}}">Edit <span class="fa fa-edit"></span></a>
{{--                                            <button onclick="remove({{$member->id}})" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$member->id}}">Remove <span class="fa fa-trash"></span></button>--}}
                                            @if(auth()->user()->hasPermission('delete_memberes'))
                                            <div class="modal modal-danger fade" id="remove_board_{{$member->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="{{route('member.destroy',$member->id)}}" method="post" id="board_remove_{{$member->id}}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_{{$member->id}}').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <button data-toggle="modal" data-target="#remove_board_{{$member->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$member->id}}">Remove <span class="fa fa-trash"></span></button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Team</th>
                                    <th>Role</th>
                                    <th>Image</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Controllers</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
