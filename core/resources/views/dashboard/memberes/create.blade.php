@extends('dashboard.layout.app')

@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
               Memberes
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('member.index')}}">Memberes</a></li>
                <li class="active">Add</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-md-12">
                @include('dashboard.layout.error')

                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Member</h3>
                        </div><!-- /.box-header -->

                        <!-- form start -->
                        <form autocomplete="off" role="form" method="post" action="{{route('member.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="box-body">

                                <div class="form-group">
                                    <label>Member Name:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="name" value="{{old('name')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Avatar:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-image"></i>
                                        </div>
                                        <input type="file" class="form-control pull-right" name="image">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Team:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-group"></i>
                                        </div>
                                        <select class="form-control js-example-tokenizer" id="tags" multiple name="teams[]">

                                            @foreach ($teams as $team)
                                                <option value="{{$team->id}}" @if(in_array($team->id,(array)old('teams'))) selected @endif>
                                                    {{$team->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Role In Team:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-road"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="role" value="{{old('role')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Start Year:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker" name="start" value="{{old('start')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>End Year:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker2" name="end" value="{{old('end')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Fabcbook:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-facebook"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="fb" value="{{old('fb')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Twitter:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-twitter"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="tw" value="{{old('tw')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                
                                <!--<div class="form-group">
                                    <label>Instagram:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-instagram"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="insta" value="{{old('insta')}}">
                                    </div>
                                   
                                </div>-->
                                <div class="form-group">
                                    <label>Linked In:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-linkedin"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="linkedin" value="{{old('linkedin')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save</button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>

    @endsection
