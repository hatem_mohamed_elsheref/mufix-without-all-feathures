@extends('dashboard.layout.app')

@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Results
                <small><span class="badge btn-danger">{{$articles->count() }}</span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('articles.index')}}">Articles</a></li>
                <li class="active">Search</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="{{route('articles.create')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Article
                    </a>
                </div>
            </div>
            <div class="row" >
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Articles Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" id="csrf" value="{{csrf_token()}}">
                            <input type="hidden" id="model" value="articles">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $index=>$post)
                                    <tr>
                                        <td>{{$index+1}}.</td>
                                        <td>{{$post->title}}</td>
                                        <td>{{$post->user->name()}}</td>
                                        <td>{{$post->category->name}}</td>
                                        <td><img src="{{asset('uploads/articles/dashboard/'.$post->image)}}" style="width: 100px;height: 50px;"></td>
                                        <td><span class="label   {{($post->status)?'label-success':'label-danger'}}">{{($post->status)?'published':'Drafted'}}</span></td>
                                        <td>
                                            <a class="btn btn-sm btn-success text-bold" href="{{route('articles.edit',$post->id)}}">Edit <span class="fa fa-edit"></span></a>
                                            <div class="modal modal-danger fade" id="remove_form_{{$post->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="{{route('articles.destroy',$post->id)}}" method="post" id="remove_item_{{$post->id}}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('remove_item_{{$post->id}}').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                            <button data-toggle="modal" data-target="#remove_form_{{$post->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$post->id}}">Remove <span class="fa fa-trash"></span></button>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
