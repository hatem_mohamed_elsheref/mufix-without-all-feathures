@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Testimonials
                <small><span class="badge btn-danger">{{$testi->count()}}</span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('testimonial.index')}}">Testimonials</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <div class="col-md-7" style="margin-bottom:15px">
                    <a href="{{route('testimonial.create')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Testimonial
                    </a>
                </div>

                <!-- Form column -->
                <div class="col-md-10">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Testimonial Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-condensed">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Feedback</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                <input type="hidden" id="csrf" value="{{csrf_token()}}">
                                <input type="hidden" id="model" value="testimonial">
                                @foreach($testi as $index=>$test)
                                 <tr>

                                     <td>{{$test->id}}</td>
                                     <td>{{$test->name}}</td>
                                     <td><img src="{{asset('uploads/testimonial/dashboard/'.$test->image)}}" style="width: 100px;height: 50px;"></td>
                                     <td>{{substr($test->feedback,0,80)}}</td>
                                     <td><span class="label  {{($test->status)?'label-success':'label-danger'}}">{{($test->status)?'published':'Drafted'}}</span></td>
                                     <td>
                                         <a class="btn btn-sm btn-success text-bold" href="{{route('testimonial.edit',$test->id)}}">Edit <span class="fa fa-edit"></span></a>
{{--                                         <button data-toggle="modal" data-target="#remove_item" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$tag->id}}">Remove <span class="fa fa-trash"></span></button>--}}
{{--                                         <button onclick="remove({{$test->id}})" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$test->id}}">Remove <span class="fa fa-trash"></span></button>--}}
                                         <div class="modal modal-danger fade" id="remove_board_{{$test->id}}" style="display: none;">
                                             <div class="modal-dialog">
                                                 <div class="modal-content">
                                                     <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                             <span aria-hidden="true">×</span></button>
                                                         <h4 class="modal-title">Delete Item</h4>
                                                     </div>
                                                     <div class="modal-body">
                                                         <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                     </div>
                                                     <form action="{{route('testimonial.destroy',$test->id)}}" method="post" id="board_remove_{{$test->id}}">
                                                         @csrf
                                                         @method('delete')
                                                     </form>
                                                     <div class="modal-footer">
                                                         <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                         <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_{{$test->id}}').submit()">Sure</button>
                                                     </div>
                                                 </div>
                                                 <!-- /.modal-content -->
                                             </div>
                                             <!-- /.modal-dialog -->
                                         </div>
                                         <button data-toggle="modal" data-target="#remove_board_{{$test->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$test->id}}">Remove <span class="fa fa-trash"></span></button>


                                     </td>
                                 </tr>
                             @endforeach
                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>


@endsection
