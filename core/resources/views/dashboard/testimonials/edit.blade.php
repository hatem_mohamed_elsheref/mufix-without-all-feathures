@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Testimonial
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('testimonial.index')}}">Testimonials</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-sm-9">
                @include('dashboard.layout.error')
                <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Testimonial</h3>
                        </div><!-- /.box-header -->



                        <!-- form start -->
                        <form role="form" method="post" action="{{route('testimonial.update',$test->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Person Name</label>
                                    <input type="text" class="form-control" name="name" placeholder=" Enter Person Name" value="{{$test->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Feedback</label>
                                    <textarea class="form-control" rows="5" name="feedback" placeholder=" Enter Person Feedback">{{$test->feedback}}</textarea>
                                </div>
                                <div class="form-group" style="margin-top: 10px">
                                    <div class="btn btn-warning btn-file">
                                        <i class="fa fa-paperclip"></i> Person Avatar
                                        <input type="file" name="image">
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="name">Status</label><br>
                                    <label class="switch">
                                        <input type="checkbox" name="status" @if($test->status)checked @endif>
                                        <span class="slider round"></span>
                                    </label>

                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success">Edit <i class="fa fa-edit"></i> </button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>



@endsection
