@extends('dashboard.layout.app')


@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Setttings
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('setting.index')}}">Setting</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-sm-9">
                @include('dashboard.layout.error')
                <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Setting Managment</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" action="{{route('setting.update',$setting->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="box-body">
                                <div class="callout callout-success"><h4>General Setting!</h4></div>
                                <div class="form-group">
                                    <label for="name"><code>Site Name</code></label>
                                    <input type="text" class="form-control" name="general_site_name" placeholder=" Enter Site Name" value="{{$setting->general_site_name}}">
                                </div>
                                <div class="form-group" style="margin-top: 10px">
                                    <label for="logo"><code>Site Logo</code></label>
                                    <br>
                                    <div class="btn btn-info btn-file">
                                        <i class="fa fa-paperclip"></i> Site Logo
                                        <input type="file" name="general_site_logo" onchange="document.getElementById('logo_tmp_image').src = window.URL.createObjectURL(this.files[0])">
                                    </div>

                                </div>
                                <div class="callout callout-success"><h4>Community Setting!</h4></div>
                                <div class="form-group">
                                    <label for="name"><code>Title For About Page About Community</code></label>
                                    <input type="text" class="form-control" name="community_title" placeholder=" Enter Community Title" value="{{$setting->community_title}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><code>Short Description About Community</code></label>
                                    <textarea class="form-control" name="community_description" placeholder="Enter Description About Community">{!! $setting->community_description !!}</textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1</code></label>
                                            <input type="text" class="form-control" name="community_feature_1" placeholder=" Enter Feature One" value="{{$setting->community_feature_1}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 3</code></label>
                                            <input type="text" class="form-control" name="community_feature_3" placeholder=" Enter Feature Three" value="{{$setting->community_feature_3}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2</code></label>
                                            <input type="text" class="form-control" name="community_feature_2" placeholder=" Enter Feature Two" value="{{$setting->community_feature_2}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 4</code></label>
                                            <input type="text" class="form-control" name="community_feature_4" placeholder=" Enter Feature Four" value="{{$setting->community_feature_4}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 10px">
                                    <label for="logo"><code>Community Main Image</code></label>
                                    <br>
                                    <div class="btn btn-info btn-file">
                                        <i class="fa fa-paperclip"></i> Main Image
                                        <input type="file" name="community_main_image" onchange="document.getElementById('community_tmp_image').src = window.URL.createObjectURL(this.files[0])">
                                    </div>

                                </div>
                                <div class="callout callout-success"><h4>Communication Setting!</h4></div>
                                <div class="form-group">
                                    <label for="fb"><code>Facebook</code></label>
                                    <input type="text" class="form-control" name="communication_fb" placeholder=" Enter Facebook Account" value="{{$setting->communication_fb}}">
                                </div>
                                <div class="form-group">
                                    <label for="tw"><code>Twitter</code></label>
                                    <input type="text" class="form-control" name="communication_tw" placeholder=" Enter Twitter Account" value="{{$setting->communication_tw}}">
                                </div>
                                <div class="form-group">
                                    <label for="ins"><code>Instagram</code></label>
                                    <input type="text" class="form-control" name="communication_inst" placeholder=" Enter Instagram Account" value="{{$setting->communication_inst}}">
                                </div>
                                <div class="form-group">
                                    <label for="ins"><code>Email</code></label>
                                    <input type="text" class="form-control" name="communication_email" placeholder=" Enter Email Account" value="{{$setting->communication_email}}">
                                </div>
                                <div class="form-group">
                                    <label for="ins"><code>Address</code></label>
                                    <input type="text" class="form-control" name="communication_address" placeholder=" Enter Address" value="{{$setting->communication_address}}">
                                </div>
                                <div class="callout callout-success"><h4>Youtube Setting!</h4></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1 Title</code></label>
                                            <input type="text" class="form-control" name="youtube_feature_1_title" placeholder=" Enter Feature One Title" value="{{$setting->youtube_feature_1_title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1</code></label>
                                            <input type="text" class="form-control" name="youtube_feature_1" placeholder=" Enter Feature One" value="{{$setting->youtube_feature_1}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1 Icon</code></label>
                                            <input type="text" class="form-control" name="youtube_feature_1_icon" placeholder=" Enter Feature One Icon" value="{{$setting->youtube_feature_1_icon}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2 Title</code></label>
                                            <input type="text" class="form-control" name="youtube_feature_2_title" placeholder=" Enter Feature Two Title" value="{{$setting->youtube_feature_2_title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2</code></label>
                                            <input type="text" class="form-control" name="youtube_feature_2" placeholder=" Enter Feature Two" value="{{$setting->youtube_feature_2}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2 Icon</code></label>
                                            <input type="text" class="form-control" name="youtube_feature_2_icon" placeholder=" Enter Feature Two Icon" value="{{$setting->youtube_feature_2_icon}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Youtube-Video</code></label>
                                            <input type="text" class="form-control" name="youtube_video" placeholder=" Enter URL" value="{{$setting->youtube_video}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="callout callout-success"><h4>Application Setting!</h4></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1 Title</code></label>
                                            <input type="text" class="form-control" name="app_feature_1_title" placeholder=" Enter Feature One Title" value="{{$setting->app_feature_1_title}}">
                                    </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1</code></label>
                                            <input type="text" class="form-control" name="app_feature_1" placeholder=" Enter Feature One" value="{{$setting->app_feature_1}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 1 Icon</code></label>
                                            <input type="text" class="form-control" name="app_feature_1_icon" placeholder=" Enter Feature One Icon" value="{{$setting->app_feature_1_icon}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2 Title</code></label>
                                            <input type="text" class="form-control" name="app_feature_2_title" placeholder=" Enter Feature Two Title" value="{{$setting->app_feature_2_title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2</code></label>
                                            <input type="text" class="form-control" name="app_feature_2" placeholder=" Enter Feature Two" value="{{$setting->app_feature_2}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 2 Icon</code></label>
                                            <input type="text" class="form-control" name="app_feature_2_icon" placeholder=" Enter Feature Two Icon" value="{{$setting->app_feature_2_icon}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 3 Title</code></label>
                                            <input type="text" class="form-control" name="app_feature_3_title" placeholder=" Enter Feature Three Title" value="{{$setting->app_feature_3_title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 3</code></label>
                                            <input type="text" class="form-control" name="app_feature_3" placeholder=" Enter Feature Three" value="{{$setting->app_feature_3}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 3 Icon</code></label>
                                            <input type="text" class="form-control" name="app_feature_3_icon" placeholder=" Enter Feature Three Icon" value="{{$setting->app_feature_3_icon}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="fb"><code>Feature 4 Title</code></label>
                                            <input type="text" class="form-control" name="app_feature_4_title" placeholder=" Enter Feature Four Title" value="{{$setting->app_feature_4_title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 4</code></label>
                                            <input type="text" class="form-control" name="app_feature_4" placeholder=" Enter Feature Four" value="{{$setting->app_feature_4}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="fb"><code>Feature 4 Icon</code></label>
                                            <input type="text" class="form-control" name="app_feature_4_icon" placeholder=" Enter Feature Four Icon" value="{{$setting->app_feature_4_icon}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-top: 10px">
                                            <label for="logo"><code>Application Logo</code></label>
                                            <br>
                                            <div class="btn btn-info btn-file">
                                                <i class="fa fa-paperclip"></i> App Image
                                                <input type="file" name="app_image" onchange="document.getElementById('app_tmp_image').src = window.URL.createObjectURL(this.files[0])">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success">Save <i class="fa fa-edit"></i> </button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
                <div class="col-sm-3">
                    <ul class="mailbox-attachments clearfix">
                        <li>
                            <label for="name"><code>Site Logo</code></label><br>
                            <span class="mailbox-attachment-icon has-img">
                                <img src="{{asset('setting/'.$setting->general_site_logo)}}" id="logo_tmp_image">
                            </span>
                        </li>
                        <li>
                            <label for="name"><code>Community Main Image</code></label><br>
                            <span class="mailbox-attachment-icon has-img">
                                <img src="{{asset('setting/'.$setting->community_main_image)}}" id="community_tmp_image">
                            </span>
                        </li>
                        <li>
                            <label for="name"><code>Application</code></label><br>
                            <span class="mailbox-attachment-icon has-img">
                                <img src="{{asset('setting/'.$setting->app_image)}}" id="app_tmp_image">
                            </span>
                        </li>
                    </ul>

                </div>
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>




    @endsection
