@extends('dashboard.layout.app')

@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('users.index')}}">Users</a></li>
                <li class="active">Add</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-md-12">
                @include('dashboard.layout.error')

                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New User</h3>
                        </div><!-- /.box-header -->


                        <!-- form start -->
                        <form role="form" method="post" action="{{route('users.store')}}" enctype="multipart/form-data" >
                            @csrf
                            @method('post')
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" placeholder=" Enter   First Name" value="{{old('first_name')}}">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" placeholder=" Enter   Last Name" value="{{old('last_name')}}">
                                </div>
                                <div class="form-group">
                                    <label for="email"> Email </label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter Email " value="{{old('email')}}">
                                </div>
                                <div class="form-group">
                                    <label for="avatars">Avatar</label>
                                    <input type="file" class="form-control" name="image" id="avatars">
                                </div>
                                <div class="form-group">
                                    <label for="phone"> Phone</label>
                                    <input type="text" class="form-control" name="phone" placeholder=" Enter    Phone" value="{{old('phone')}}">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" placeholder=" Enter   Password">
                                </div>
                                <div class="form-group">
                                    <label for="confirmationpassword">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                                </div>

                                <div class="form-group">
                                    <label for="permissions">Permissions</label>
                                </div>
                            </div><!-- /.box-body -->



                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <?php
                                        function map($char){
                                            if ($char=='c'){
                                                return 'Create';
                                            }elseif ($char=='r'){
                                                return 'Read';
                                            }elseif ($char=='u'){
                                                return 'Update';
                                            }elseif ($char=='d'){
                                                return 'Delete';
                                            }elseif ($char=='e'){
                                                return 'Restore';
                                            }elseif ($char=='t'){
                                                return 'Trash';
                                            }elseif ($char=='i'){
                                                return 'ShowTrashed';
                                            }
                                        }
                                        ?>
                                        @foreach($config = config('laratrust_seeder.role_structure.super_admin') as $k=>$item)
                                            <li class="{{(strtolower($k)=='users')?'active':''}}"><a href="#{{$k}}" data-toggle="tab" class="">{{ucfirst($k)}}</a></li>
                                            @endforeach
                                    </ul>
                                    <span class="clearfix"></span>
                                    <div class="tab-content">
                                        @foreach($config = config('laratrust_seeder.role_structure.super_admin') as $k=>$item)
                                            <div class="tab-pane {{(strtolower($k)=='users')?'active':''}}" id="{{$k}}">
                                                <div style="margin-right: 0px;padding-right: 0px">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                        <?php

                                              foreach (explode(',',$item) as $element){
                                                  $operation=map(strtolower($element));?>

                                                            <div class="form-group pull-left" style="margin-left: 10px">
                                                                <div class="checkbox-inline">

                                                                    <input type="checkbox" id="{{strtolower($operation.'_'.$k)}}" value="{{strtolower($operation.'_'.$k)}}" @if(in_array(strtolower($operation.'_'.$k),(array) old('permissions'))) checked @endif name="permissions[]">
                                                                    <label for="{{strtolower($operation.'_'.$k)}}">{{$operation.' '.$k}}</label>
                                                                </div>
                                                            </div>

                                                <?php }?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div><!-- /.tab-pane -->
                                        @endforeach

                                    </div><!-- /.tab-content -->
                                </div>
                            </div><!-- /.col -->






                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Save</button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>

    @endsection
