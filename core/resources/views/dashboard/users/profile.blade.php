@extends('dashboard.layout.app')

@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
               Account
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('users.index')}}">Users</a></li>
                <li class="active">Profile</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-md-12">
                @include('dashboard.layout.error')

                <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Your Account</h3>
                        </div><!-- /.box-header -->


                        <!-- form start -->
                        <form role="form" method="post" action="{{route('users.update_profile')}}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" placeholder=" Enter   First Name" value="{{$user->first_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" placeholder=" Enter   Last Name" value="{{$user->last_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="email"> Email </label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter Email " value="{{$user->email}}">
                                </div>
                                <div class="form-group">
                                    <label for="avatars">Avatar</label>
                                    <input type="file" class="form-control" name="image" id="avatars">
                                </div>
                                <div class="form-group">
                                    <label for="phone"> Phone</label>
                                    <input type="text" class="form-control" name="phone" placeholder=" Enter    Phone" value="{{$user->phone}}">
                                </div>
                                <div class="form-group">
                                    <label for="password"> Password</label>
                                    <input type="password" class="form-control" name="password" placeholder=" Enter  Password" >
                                </div>

                            </div><!-- /.box-body -->






                            <div class="box-footer">
                                <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Edit</button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>

@endsection
