@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small><span class="badge btn-danger">{{$users->count() }}</span></small>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('users.index')}}">Users</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="{{route('users.create')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New User
                    </a>
                </div>
            </div>
            <input type="hidden" id="csrf" value="{{csrf_token()}}">
            <input type="hidden" id="model" value="users">
            <div class="row" >
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Users Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Avatar</th>
                                    <th>Controllers</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($users as $index=>$user)
                                    <tr>
                                        <td>{{$user->id }}</td>
                                        <td>{{$user->first_name.' '.$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>

                                        <td style="text-align: center"><img src="{{asset('uploads/users/').'/'.$user->image}}" class="img-circle" style="width: 60px;height: 60px;"></td>
                                        <td>
                                            <a class="btn btn-sm btn-success text-bold" href="{{route('users.edit',$user->id)}}">Edit <span class="fa fa-edit"></span></a>
{{--                                            <button onclick="remove({{$user->id}})" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$user->id}}">Remove <span class="fa fa-trash"></span></button>--}}
                                            <div class="modal modal-danger fade" id="remove_board_{{$user->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="{{route('users.destroy',$user->id)}}" method="post" id="board_remove_{{$user->id}}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_{{$user->id}}').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            {{--                                            <button onclick="remove({{$board->id}})" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$board->id}}">Remove <span class="fa fa-trash"></span></button>--}}
                                            <button data-toggle="modal" data-target="#remove_board_{{$user->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$user->id}}">Remove <span class="fa fa-trash"></span></button>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Avatar</th>
                                    <th>Controllers</th>
                                </tr>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
