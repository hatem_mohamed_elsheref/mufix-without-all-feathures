@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
               Teams
                <small><span class="badge btn-danger">{{$teams->count()}}</span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('team.index')}}">Team</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <div class="col-md-7" style="margin-bottom:15px">
                    <a href="{{route('team.create')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Add New Team
                    </a>
                </div>

                <!-- Form column -->
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Team Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-condensed">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Team</th>
                                    <th>Progress</th>
                                    <th>Memberes</th>
                                    <th>Controllers</th>
                                </tr>
                                <input type="hidden" id="csrf" value="{{csrf_token()}}">
                                <input type="hidden" id="model" value="team">
                                @foreach($teams as $index=>$team)
                                 <tr>

                                     <td>{{$team->id}}</td>
                                     <td>{{$team->name}}</td>
                                     <td>
                                         <div class="progress progress-xs">
                                             <div class="progress-bar progress-bar-danger" style="width: {{(($team->memberes->count())/(($memberes==0)?1:$memberes))*100}}%"></div>
                                         </div>
                                     </td>
                                     <td><span class="badge bg-red">{{(($team->memberes->count())/(($memberes==0)?1:$memberes))*100}}%</span></td>
                                     <td>
                                         <a class="btn btn-sm btn-success text-bold" href="{{route('team.edit',$team->id)}}">Edit <span class="fa fa-edit"></span></a>
{{--                                         <button onclick="remove({{$team->id}})" class="btn btn-sm btn-danger text-bold" id="remove_cat" data-value="{{$team->id}}">Remove <span class="fa fa-trash"></span></button>--}}
                                         <div class="modal modal-danger fade" id="remove_board_{{$team->id}}" style="display: none;">
                                             <div class="modal-dialog">
                                                 <div class="modal-content">
                                                     <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                             <span aria-hidden="true">×</span></button>
                                                         <h4 class="modal-title">Delete Item</h4>
                                                     </div>
                                                     <div class="modal-body">
                                                         <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                     </div>
                                                     <form action="{{route('team.destroy',$team->id)}}" method="post" id="board_remove_{{$team->id}}">
                                                         @csrf
                                                         @method('delete')
                                                     </form>
                                                     <div class="modal-footer">
                                                         <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                         <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_{{$team->id}}').submit()">Sure</button>
                                                     </div>
                                                 </div>
                                                 <!-- /.modal-content -->
                                             </div>
                                             <!-- /.modal-dialog -->
                                         </div>
                                         <button data-toggle="modal" data-target="#remove_board_{{$team->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$team->id}}">Remove <span class="fa fa-trash"></span></button>

                                     </td>
                                 </tr>
                             @endforeach
                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>


@endsection
