@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
               Teams
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('team.index')}}">Team</a></li>
                <li class="active">Add</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-sm-9">
                   @include('dashboard.layout.error')
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Team</h3>
                        </div><!-- /.box-header -->



                        <!-- form start -->
                        <form role="form" method="post" action="{{route('team.store')}}">
                           @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Team Name</label>
                                    <input type="text" class="form-control" name="name" placeholder=" Enter Team Name" value="{{old('name')}}">
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save <i class="fa fa-plus"></i> </button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>



@endsection
