@extends('dashboard.layout.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Categories
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Categories</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-md-9">

                    <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Category</h3>
                        </div><!-- /.box-header -->

                        <!-- form start -->
                        <form role="form" method="post" action="{{route('tags.update',$tag->id)}}">
                           {{csrf_field()}}
                          {{method_field('PUT')}}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Category Name</label>
                                    <input type="text" class="form-control" name="name" placeholder=" Enter Tag Name" value="{{$tag->name}}">
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success">Update <i class="fa fa-edit"></i> </button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>



@endsection
