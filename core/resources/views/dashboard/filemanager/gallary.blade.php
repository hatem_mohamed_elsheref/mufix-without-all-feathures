@extends('dashboard.layout.app')

@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Gallary
                <small><span class="badge btn-danger"></span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('filemanager')}}">File Manager</a></li>
                <li class="active">Gallary</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="{{route('upload')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                        Upload New
                    </a>
                </div>
            </div>
            <div class="row">
               @foreach($total as $item)
                    <div class="col-6 col-md-4 col-lg-3 col-xl-2" style="height: 300px;">
                        <div class="card"><a href="{{asset('uploads/general_images/dashboard/'.$item->image)}}" data-lightbox="gallery" data-title="{{$item->activity->name??'Memories'}}">
                                <img  src="{{asset('uploads/general_images/dashboard/'.$item->image)}}" alt="Image 1063" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title mb-1">{{date_format(date_create($item->created_at),'Y-M-D')}}</h5>
                                <p class="card-text text-sm text-muted">{{$item->activity->name??'Memories'}}</p>
                            </div>
                        </div>
                    </div>
                   @endforeach

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection

