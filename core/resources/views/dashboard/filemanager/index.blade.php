@extends('dashboard.layout.app')

@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Images
                <small><span class="badge btn-danger">{{$total->count() }}</span></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('filemanager')}}">File Manager</a></li>
                <li class="active">All</li>
            </ol>
        </section>
        <!--Main Content-->
        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-7  mr-bottom-10">
                    <a href="{{route('upload')}}" class="btn btn-sm btn-primary">
                        <span class="fa fa-plus"></span>
                       Upload New
                    </a>
                </div>
            </div>
            <div class="row" >
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Images Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" id="csrf" value="{{csrf_token()}}">
                            <input type="hidden" id="model" value="file">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Activity</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($total as $index=>$item)
                                    <tr>
                                        <td>{{$item->id}}.</td>
                                        <td><img src="{{asset('uploads/general_images/dashboard/'.$item->image)}}" style="width: 100px;height: 50px;"></td>
                                        <td>{{($item->activity_id==0)?'Memories':$item->activity->name}}</td>
                                        <td>{{date_format(date_create($item->created_at),'Y-M-D')}}</td>
                                        <td><span class="label   {{($item->status)?'label-success':'label-danger'}}">{{($item->status)?'published':'Drafted'}}</span></td>

                                        <td>
                                            <a href="{{asset('uploads/general_images/site/'.$item->image)}}" class="btn btn-sm btn-warning text-bold" data-lightbox="gallery" data-title="{{$item->activity->name??'Memories'}}">View <span class="fa fa-eye"></span></a>






                                            <div class="modal modal-primary fade" id="status_board_{{$item->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Change Item Status</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Change This Item Status?</P></h3>
                                                        </div>
                                                        <form action="{{route('file.status',$item->id)}}" method="post" id="board_status_{{$item->id}}">
                                                            @csrf
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('board_status_{{$item->id}}').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>










                                            <div class="modal modal-danger fade" id="remove_board_{{$item->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Delete Item</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h3> <P>Do You Sure To Remove This Item?</P></h3>
                                                        </div>
                                                        <form action="{{route('file.destroy',$item->id)}}" method="post" id="board_remove_{{$item->id}}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-outline" onclick="document.getElementById('board_remove_{{$item->id}}').submit()">Sure</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <button data-toggle="modal" data-target="#remove_board_{{$item->id}}" class="btn btn-sm btn-danger text-bold"  data-value="{{$item->id}}">Remove <span class="fa fa-trash"></span></button>

                                            @if($item->status)
                                                <button data-toggle="modal" data-target="#status_board_{{$item->id}}" class="btn btn-sm btn-info text-bold" id="draft_cat" data-value="{{$item->id}}">Draft <span class="fa fa-archive"></span></button>
                                              @else
                                                <button data-toggle="modal" data-target="#status_board_{{$item->id}}" class="btn btn-sm btn-success text-bold" id="publish_cat" data-value="{{$item->id}}">Publish <span class="fa fa-send"></span></button>

                                            @endif

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Activity</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Controllers</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
