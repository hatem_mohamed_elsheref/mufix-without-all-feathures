@extends('dashboard.layout.app')


@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                File Manager
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('filemanager')}}">Uploader</a></li>
                <li class="active">upload</li>
            </ol>
        </section>
        <!--Main Content-->
        <!--Form For Add-->
        <section class="content">
            <div class="row">
                <!-- Form column -->
                <div class="col-sm-9">
                @include('dashboard.layout.error')
                <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Memories Uploader</h3>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            <form action="{{route('dropzone',0)}}" class="dropzone" enctype="multipart/form-data">
                                @csrf
                            </form>
                        </div>
                    </div><!-- /.box -->
                    @foreach($activities as $item)
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{$item->name}} Uploader</h3>
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                <form action="{{route('dropzone',$item->id)}}" class="dropzone" enctype="multipart/form-data">
                                    @csrf
                                </form>
                            </div>
                        </div><!-- /.box -->
                    @endforeach
                </div><!--/.col (Form) -->
            </div>   <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>




@endsection
