@extends('dashboard.layout.app')


@section('content')



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
               Activity Posts
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('welcome')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('posts.index')}}">Activity Posts</a></li>
                <li class="active">view</li>
            </ol>
        </section>
        <!--Main Content-->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('activity-posts.index')}}" class="btn btn-primary btn-block margin-bottom">Back to Activity Posts Index</a>

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Activities</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($activities as $activity)
                                    <li><a href="{{route('activities.show',$activity->id)}}"><i class="fa fa-bars"></i> {{$activity->name}}
                                            <span class="label label-primary pull-right">{{$activity->activityPosts->count()}}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Activity Posts</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="{{route('activity-posts.type',1)}}">
                                        <i class="fa fa-folder-o text-green"></i>Published
                                        <span class="label label-success pull-right">{{$published}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('activity-posts.type',0)}}">
                                        <i class="fa fa-archive text-blue"></i> Dreafted
                                        <span class="label label-primary pull-right">{{$drafted}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('dashboard/activity-posts/trashed')}}">
                                        <i class="fa fa-trash text-red"></i> Trashed
                                        <span class="label label-danger pull-right">{{$trashed}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <input type="hidden" id="csrf" value="{{csrf_token()}}">
                <input type="hidden" id="model" value="activity-posts/trashed">
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        @if($post->status)
                            <h3 class="box-title"> <span class="text-success"><i class="fa fa-circle"></i> Published</span></h3>
                            @else
                                <h3 class="box-title"> <span class="text-danger"><i class="fa fa-circle"></i> Drafted</span></h3>
                        @endif
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="mailbox-read-info">
                               <p>
                                <h3>{{$post->title}}</h3>
                                <span class="mailbox-read-time pull-left">By {{$post->user->name()}}</span>
                                <span class="mailbox-read-time pull-right">{{date_format(date_create($post->created_at),'Y - m - d')}}</span>
                               </p>
                            </div>
                            <!-- /.mailbox-read-info -->
                            <div class="mailbox-read-message">
                                <p>{!!$post->content!!}</p>
                                <p><br><span class="text-bold text-danger">{{$post->activity->name}}</span></p>
                            </div>
                            <!-- /.mailbox-read-message -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <ul class="mailbox-attachments clearfix">
                                <li>
                                    <span class="mailbox-attachment-icon has-img"><img src="{{asset('uploads/activities/dashboard/'.$post->image)}}" alt="Attachment"></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> Activity Post Image</a>

                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            @if($post->deleted_at!=null)
                                <div class="modal modal-primary fade" id="restore_board_{{$post->id}}" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title">Restore Item</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h3> <P>Do You Sure To Restore This Item?</P></h3>
                                            </div>
                                            <form action="{{route('activity-posts.restore',$post->id)}}" method="post" id="board_restore_{{$post->id}}">
                                                @csrf

                                            </form>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-outline" onclick="document.getElementById('board_restore_{{$post->id}}').submit()">Sure</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <button data-toggle="modal" data-target="#restore_board_{{$post->id}}" class="btn btn-sm btn-danger text-bold"id="restore_item" >Restore <span class="fa fa-refresh"></span></button>
                            @endif
                                <a href="{{route('activity-posts.edit',$post->id)}}" class="btn  btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->


            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>






    @endsection
