@extends('dashboard.layout.app')

@section('content')

    @push('js')
{{--        <script src="https://cdn.tiny.cloud/1/4b2rd40qpdelgfmi3oofgocpnxs28fndbr734f0c12ctqmk0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>--}}
{{--        <script>--}}
{{--            tinymce.init({--}}
{{--                selector: '#editor1',--}}
{{--                convert_urls: false,--}}
{{--                height:600,--}}
{{--                statusbar: false,--}}

{{--                plugins: 'emoticons directionality image  code print preview fullpage  searchreplace autolink directionality  visualblocks visualchars fullscreen image link    table charmap hr pagebreak nonbreaking  toc insertdatetime advlist lists textcolor wordcount   imagetools    contextmenu colorpicker textpattern media ',--}}
{{--                toolbar: 'ltr rtl emoticons formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat |undo redo | image code| link fontsizeselect  | ',--}}


{{--            });--}}

{{--        </script>--}}
<script src="{{asset('dashboard/assets/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>
    @endpush
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Contentlo Header (Page header) -->
        <section class="content-header">
            <h1>
                Posts
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('posts.index')}}">Posts</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <!--Main Content-->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('posts.index')}}" class="btn btn-primary btn-block margin-bottom">Back to Posts Index</a>

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Activities</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($activities as $activity)
                                    <li><a href="{{route('activities.show',$activity->id)}}"><i class="fa fa-bars"></i> {{$activity->name}}
                                            <span class="label label-primary pull-right">{{$activity->activityPosts->count()}}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Posts</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding" style="">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="{{route('posts.type',1)}}">
                                        <i class="fa fa-folder-o text-green"></i>Published
                                        <span class="label label-success pull-right">{{$published}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('posts.type',0)}}">
                                        <i class="fa fa-archive text-blue"></i> Dreafted
                                        <span class="label label-primary pull-right">{{$drafted}}</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    @include('dashboard.layout.error')
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">Edit Post
                                <small></small>
                            </h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip"
                                        title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->


                        <div class="box-body pad">
                            <form action="{{route('activity-posts.update',$post->id)}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{method_field('put')}}
                                <div class="form-group">
                                    <label for="tags">Title</label>
                                    <input class="form-control" name="title" placeholder="Enter Post Title" value="{{$post->title}}">
                                </div>
                                <div class="form-group">
                                    <label for="tags">Description</label>
                                    <textarea class="form-control" name="description" rows="5" placeholder="Enter Post Description">{!! $post->description !!}</textarea>
                                </div>

                                <div class="form-group" hidden>
                                    <label for="direction">Direction</label>
                                  <select class="form-control" name="dir">
                                      <option selected disabled>Select Direction</option>
            
                                          <option value="ltr" @if($post->dir==='ltr') selected @endif>LTR</option>
                                          <option value="rtl" @if($post->dir==='rtl') selected @endif>RTL</option>
                                  
                                  </select>
                                </div>


                                <div class="form-group">
                                    <label for="tags">Activity</label>
                                    <select class="form-control" name="activity_id">
                                        <option selected disabled>Select Activity</option>
                                        @foreach($activities as $activity)
                                            <option value="{{$activity->id}}" @if($post->activity_id==$activity->id) selected @endif>
                                                {{$activity->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form group -->

                                <textarea id="editor1" name="content" rows="10" cols="80" placeholder="Enter Your Post Here">{!! $post->content !!}</textarea>
                                <div class="form-group" style="margin-top: 10px">
                                    <div class="btn btn-warning btn-file">
                                        <i class="fa fa-paperclip"></i> Attachment
                                        <input type="file" name="image">
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Edit</button>

                                        <label class="switch">
                                            <input type="checkbox" name="status" @if($post->status) checked @endif>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <button style="display: none" type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Discard</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
