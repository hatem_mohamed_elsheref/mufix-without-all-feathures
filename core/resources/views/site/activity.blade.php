


@extends('site.layout.app')


@section('content')

    <a href="#blog-section" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
    </a>
    </div>








    <!--Latest Posts-->
    <section class="site-section" id="blog-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">{{$type}}</h2>
                </div>
            </div>

            <div class="row blog-center">
                @forelse($posts as $post)
                    @if(!$post->status)
                        @continue
                        @endif
                    <div class="col-md-6 col-lg-4 mb-4 mb-lg-4 " data-aos="fade-up" data-aos-delay="100">
                        <div class="h-entry">
                            <div style="width: 350px;height: 300px;margin: auto">
                                <a href="{{route('post_activity',$post->id)}}">
                                    <img src="{{asset('uploads/activities/site/'.$post->image)}}" alt="Image" class="img-fluid xyz img-responsive" style="width: 100%;height: 100%;">
                                </a>
                            </div>
                            <h2 class="font-size-regular" style="margin-top: 10px"><a href="{{route('post_activity',$post->id)}}">{{$post->title}}</a></h2>
                            <div class="meta mb-4">{{$post->user->name()}} <span class="mx-2">&bullet;</span> {{date_format(date_create($post->created_at),'M - D - Y')}}<span class="mx-2">&bullet;</span> <a href="#">{{$post->activity->name}}</a></div>
                            <p>{!! substr($post->description,0,120) !!}</p>
                            @if($post->dir==='rtl') 
                            <p style="text-align:end"><a href="{{route('post_activity',$post->id)}}">قراءة المزيد</a></p>
                             @else 
                             <p><a href="{{route('post_activity',$post->id)}}">Continue Reading...</a></p>
                             @endif
                        </div>
                    </div>
                @empty
                    <div class="col-md-6 col-lg-4 mb-4 mb-lg-4 " data-aos="fade-up" data-aos-delay="100">
                        <div class="h-entry">
                            <h2 class="font-size-regular text-center">
                                No Posts Found .
                            </h2>
                        </div>
                    </div>
                @endforelse

            </div>
        </div>
    </section>

    <!--  Start  Pagination-->
    <section class="site-section" id="blog-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 pagination">
                    {!! $posts->render() !!}
                </div>
            </div>
        </div>
    </section>

@endsection
