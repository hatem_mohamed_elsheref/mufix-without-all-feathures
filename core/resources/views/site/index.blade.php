@extends('site.layout.app')

@section('content')


    <a href="#about-section" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
    </a>
    </div>

    <div class="site-section cta-big-image" id="about-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">About Us</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
                   
                        <img loading="lazy" src="{{asset('setting/'.$setting->community_main_image)}}" alt="Image" class="img-fluid img-rounded" style="border-radius: 10px ">
                   
                </div>
                <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
                    <div class="mb-4">
                        <h3 class="h3 mb-4 text-black main-color-text">{{$setting->community_title}}</h3>
                        <p>{{$setting->community_description}}.</p>
                    </div>
                    <div class="mb-4">
                        <ul class="list-unstyled ul-check success">
                            <li>{{$setting->community_feature_1}}</li>
                            <li>{{$setting->community_feature_2}}</li>
                            <li>{{$setting->community_feature_3}}</li>
                            <li>{{$setting->community_feature_4}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--mix-->
    <section class="site-section" id="portfolio-sections">


        <div class="container">

            <div class="row mb-3">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Team 2020</h2>
                </div>
            </div>

            <div class="row justify-content-center mb-5" data-aos="fade-up">
                <div id="filters1" class="filters1">
                </div>
                <div id="filters" class="filters text-center button-group col-md-10">
                    <button class="btn btn-primary active" data-filter="*" id="all_btn_filter">All</button>
                    <button class="btn btn-primary" data-filter=".board" id="board_btn_filter">Board</button>
                    @foreach($teams as $team)
                        <button class="btn btn-primary" data-filter=".{{strtolower($team->name)}}">{{$team->name}}</button>
                    @endforeach

                </div>
            </div>

            <div id="posts" class="row no-gutter center-block">
                @foreach($memberes as $member)
                    @foreach($member->team as $item)
                        <div class="col-6 col-lg-2  mb-4 item {{strtolower($item->name)}} flex-fill text-center" data-aos-delay="">
                            <div class="team-member">
                                <figure class="center-block">
                                    <ul class="social">
                                        <li><a href="{{$member->fb}}"><span class="icon-facebook"></span></a></li>
                                        <li><a href="{{$member->tw}}"><span class="icon-twitter"></span></a></li>
                                        <li><a href="{{$member->linkedin}}"><span class="icon-linkedin"></span></a></li>
{{--                                        <!--<li><a href="{{$member->insta}}"><span class="icon-instagram"></span></a></li>-->--}}
                                    </ul>
                                    <img loading="lazy" src="{{asset('uploads/memberes/site/'.$member->image)}}" alt="Image" class="img-fluid xyz" style="width: 180px;height: 200px">
                                </figure>
                                <div class="p-3" style="height:120px">
                                    <h3 class="main-color-text" style="font-size:15px">{{$member->name}}</h3>
                  <span class="position_" style="font-size:9px;word-spacing:0px;text-transform:uppercase;color:#cccccc">{{$item->name}} {{$member->role}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
                @foreach($board as $member)
                    <div class="col-6 col-lg-2 mb-4 item board flex-fill text-center" data-aos-delay="" hidden>
                        <div class="team-member">
                            <figure>
                                <ul class="social">
                                    <li><a href="{{$member->fb}}"><span class="icon-facebook"></span></a></li>
                                    <li><a href="{{$member->tw}}"><span class="icon-twitter"></span></a></li>
                                    <li><a href="{{$member->linkedin}}"><span class="icon-linkedin"></span></a></li>
                                </ul>
                                <img loading="lazy" src="{{asset('uploads/memberes/site/'.$member->member[0]->image)}}" alt="Image" class="img-fluid xyz" style="width: 180px;height: 200px">
                            </figure>
                            <div class="p-3" style="height:120px">
                                <h3 class="main-color-text"  style="font-size:15px">{{$member->member[0]->name}}</h3>
                                <span class="position_" style="font-size:9px;word-spacing:1px;text-transform:uppercase;color:#cccccc">Leader {{$member->team[0]->name}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach 
             
            </div>
        </div>
<butt
     {!! $memberes->render() !!}
    </section>

    <!--Our Services-->
    <section class="site-section border-bottom bg-light" id="services-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Our Services</h2>
                </div>
            </div>
            <div class="row align-items-stretch text-center">
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4 " data-aos="fade-up">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-startup"></span></div>
                        <div>
                            <h3 class="main-color-text">Business Consulting</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-graphic-design"></span></div>
                        <div>
                            <h3 class="main-color-text">Market Analysis</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-settings"></span></div>
                        <div>
                            <h3 class="main-color-text">User Monitoring</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary  main-color flaticon-idea"></span></div>
                        <div>
                            <h3 class="main-color-text">Insurance Consulting</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-smartphone"></span></div>
                        <div>
                            <h3 class="main-color-text">Financial Investment</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="unit-4">
                        <div class="unit-4-icon mr-4"><span class="text-primary main-color flaticon-head"></span></div>
                        <div>
                            <h3 class="main-color-text">Financial Management</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Yoytube Channel-->
    <section class="site-section" id="about-section1">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Youtube Channel</h2>
                </div>
            </div>
            <div class="row mb-5 text-center">

                <div class="col-lg-5 ml-auto mb-5 order-1 order-lg-2" data-aos="fade" data-aos="fade-up" data-aos-delay="">
                    <iframe width="100%" height="100%" src="{{$setting->youtube_video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-lg-6 order-2 order-lg-1" data-aos="fade">

                    <div class="row">



                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color {{$setting->youtube_feature_2_icon}}"></span></div>
                                <div>
                                    <h3 class="main-color-text">{{$setting->youtube_feature_1_title}}</h3>
                                    <p>{{$setting->youtube_feature_1}}.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color {{$setting->youtube_feature_2_icon}}"></span></div>
                                <div>
                                    <h3 class="main-color-text">{{$setting->youtube_feature_2_title}}</h3>
                                    <p>{{$setting->youtube_feature_2}}.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Application-->
    <section class="site-section" id="about-section111">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Android Application</h2>
                </div>
            </div>
            <div class="row mb-5 text-center">

                <div class="col-lg-5 ml-auto mb-5 order-1 order-lg-2" data-aos="fade" data-aos="fade-up" data-aos-delay="" style="text-align: center">
                    <img loading="lazy" src="{{asset('setting/'.$setting->app_image)}}" alt="Image" class="img-fluid rounded application-image" >
                </div>
                <div class="col-lg-6 order-2 order-lg-1" data-aos="fade">
                    <div class="row">
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color {{$setting->app_feature_1_icon}}"></span></div>
                                <div>
                                    <h3 class="main-color-text">{{$setting->app_feature_1_title}}</h3>
                                    <p>{{$setting->app_feature_1}}.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color {{$setting->app_feature_2_icon}}"></span></div>
                                <div>
                                    <h3 class="main-color-text">{{$setting->app_feature_2_title}}</h3>
                                    <p>{{$setting->app_feature_2}}.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color {{$setting->app_feature_3_icon}}"></span></div>
                                <div>
                                    <h3 class="main-color-text">{{$setting->app_feature_3_title}}</h3>
                                    <p>{{$setting->app_feature_3}}.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="unit-4">
                                <div class="unit-4-icon mr-4 mb-3"><span class="text-primary main-color {{$setting->app_feature_4_icon}}"></span></div>
                                <div>
                                    <h3 class="main-color-text">{{$setting->app_feature_4_title}}</h3>
                                    <p>{{$setting->app_feature_4}}.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--Testimonial-->
    <section class="site-section testimonial-wrap" id="testimonials-section" data-aos="fade">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center">
                    <h2 class="section-title mb-3">Testimonials</h2>
                </div>
            </div>
        </div>
        <div class="slide-one-item home-slider owl-carousel">
            @foreach($testimonial as $testi)
                <div>
                    <div class="testimonial">

                        <blockquote class="mb-5">
                            <p>&ldquo;{{$testi->feedback}}&rdquo;</p>
                        </blockquote>

                        <figure class="mb-4 d-flex align-items-center justify-content-center">
                            <div><img loading="lazy" src="{{asset('uploads/testimonial/site/'.$testi->image)}}" alt="Image" class="w-50 img-fluid mb-3" style="object-fit: cover;width: 50px;height: 50px"></div>
                            <p>{{$testi->name}}</p>
                        </figure>
                    </div>
                </div>
            @endforeach

        </div>
    </section>

    <!--Latest Posts-->
    <section class="site-section" id="blog-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Latest  Posts</h2>
                </div>
            </div>

            <div class="row blog-center">
                @foreach($posts as $post)
                    <div class="col-md-6 col-lg-4 mb-4 mb-lg-4 " data-aos="fade-up" data-aos-delay="100">
                        <div class="h-entry ">
                           <div style="width: 350px;height: 300px;margin: auto">
                        
                                <a href="{{route('post',$post->id)}}">
                                    <img loading="lazy" src="{{asset('uploads/posts/site/'.$post->image)}}" alt="Image" class="img-fluid xyz img-responsive" style="width: 100%;height: 100%;">
                                </a>
                            </div>
                            <h2 class="font-size-regular" style="margin-top: 10px"><a href="{{route('post',$post->id)}}">{{$post->title}}</a></h2>
                            <div class="meta mb-4">{{$post->user->first_name}} <span class="mx-2">&bullet;</span> {{date_format(date_create($post->created_at),'M - D - Y')}}<span class="mx-2">&bullet;</span> <a href="{{route('blog_cat',$post->category->id)}}">{{$post->category->name}}</a></div>
                            <p>{!! substr($post->description,0,120) !!}</p>
                            @if($post->dir==='rtl') 
                            <p style="text-align:end"><a href="{{route('post',$post->id)}}">قراءة المزيد</a></p>
                             @else 
                             <p><a href="{{route('post',$post->id)}}">Continue Reading...</a></p>
                             @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <div id="projectFacts" class="sectionClass">
        <div class="fullWidth eight columns">
            <div class="projectFactsWrap ">
                <div class="item wow fadeInUpBig animated animated" data-number="20" style="visibility: visible;">
                    <i class="fa fa-plane"></i>
                    <p id="number1" class="number">20</p>
                    <span></span>
                    <p>Trips</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-number="100" style="visibility: visible;">
                    <i class="fa fa-camera"></i>
                    <p id="number2" class="number">100</p>
                    <span></span>
                    <p>Sessions</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-number="80" style="visibility: visible;">
                    <i class="fa fa-microphone"></i>
                    <p id="number3" class="number">80</p>
                    <span></span>
                    <p>Events</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-number="50" style="visibility: visible;">
                    <i class="fa fa-users"></i>
                    <p id="number4" class="number">50</p>
                    <span></span>
                    <p>Trainning </p>
                </div>
            </div>
        </div>
    </div>

    <!--Slider-->
    <div class="site-section cta-big-image" id="">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Memories</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5 your-class"  data-aos-delay="">
                  
                    @foreach($memories as $memory)
                        <div style="width: 320px;height: 320px;" ><img  loading="lazy" style=" margin:0 1px;width:350px" src="{{asset('uploads/general_images/site/'.$memory->image)}}"  alt="Image" class="img-fluid xyz" style="width: 100%;height: 100%;border-radius:unset"></div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>


@endsection

@section('js')
<script>
    let memberesPaginator=document.getElementsByClassName('pagination')[0];
    memberesPaginator.className='pagination justify-content-center';
    memberesPaginator.style.marginLeft=null;
    
    console.log(memberesPaginator);
</script>
@endsection
