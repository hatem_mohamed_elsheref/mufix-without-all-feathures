@extends('site.layout.app')

@section('content')


        <div class="container">
            <div class="row align-items-center justify-content-center">


                <div class="col-md-8 mt-lg-5 text-center">
                    <h1 class="text-uppercase" data-aos="fade-up"></h1>
                    <p class="mb-5 desc"  data-aos="fade-up" data-aos-delay="100">

                    </p>

                </div>

            </div>
        </div>

        <a href="#about-section" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
        </a>
    </div>

        <div class="site-section cta-big-image" id="about-section">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-12 text-center" data-aos="fade">
                        <h2 class="section-title mb-3">About Us</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
                        <figure class="circle-bg">
                            <img src="{{storage_path('test.jpg')}}" alt="Image" class="img-fluid img-rounded" style="border-radius: 10px ">
                        </figure>
                    </div>
                    <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
                        <div class="mb-4">
                            <h3 class="h3 mb-4 text-black main-color-text">title</h3>
                            <p>description.</p>
                        </div>
                        <div class="mb-4">
                            <ul class="list-unstyled ul-check success">
                                <li>feature</li>
                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    @endsection