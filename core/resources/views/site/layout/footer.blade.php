
<!--Footer-->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <h2 class="footer-heading mb-4">About Us</h2>
                        <p>{{$setting->community_description}}</p>
                    </div>
                    <div class="col-md-3 ml-auto">
                        <h2 class="footer-heading mb-4">Quick Links</h2>
                        <ul class="list-unstyled">
                            <li><a href="{{route('blog')}}" class="smoothscroll">Blog</a></li>
                            <li><a href="{{route('about')}}" class="smoothscroll">About Us</a></li>
                            <li><a href="{{route('contact')}}" class="smoothscroll">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h2 class="footer-heading mb-4">Follow Us</h2>
                        <a href="{{$setting->communication_fb}}" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                        <a href="{{$setting->communication_tw}}" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                        <a href="{{$setting->communication_inst}}" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h2 class="footer-heading mb-4">Subscribe Newsletter</h2>
                <form action="#" method="post" class="footer-subscribe">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary text-black" type="button" id="button-addon2">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
                <div class="border-top pt-5">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed by <i class="icon-heart text-danger" aria-hidden="true"></i> <a href="https://www.facebook.com/hatem.elsheref.73" target="_blank" >Hatem Mohamed Elsheref | MUFIX Development Team</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>

                </div>
            </div>
            <div class="row">
                <a href="https://arabhosters.com" target="_blank"><img src="/arabhosters.jpg" alt="ArabHosters" style="max-width:80%;margin-bottom:-30px"></a>
            </div>

        </div>
    </div>
</footer>

</div> <!-- .site-wrap -->


<script src="{{asset('site/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('site/assets/js/jquery-ui.js')}}"></script>
<script src="{{asset('site/assets/js/popper.min.js')}}"></script>
<script src="{{asset('site/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('site/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('site/assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('site/assets/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('site/assets/js/aos.js')}}"></script>
<script src="{{asset('site/assets/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('site/assets/js/jquery.sticky.js')}}"></script>
<script src="{{asset('site/assets/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('site/assets/js/slick.js')}}"></script>
<script src="{{asset('site/assets/js/mycustom.js')}}"></script>
<script src="{{asset('site/assets/js/main.js')}}"></script>

<script>
    function go(place,data) {
        window.location.href='/'+place+"/"+data;
    }
    setTimeout(function(){
        $('body').addClass('loaded');
        $("body").css("overflow","auto");
        $('#loader-wrapper').hide();
    }, 1200);
    
    
    $(document).ready(function(){
    $('#all_btn_filter').on('click',function(){
        $('.board').each(function(index,val){
            $(this).attr('hidden',true);
        
        });
    });
});

$(document).ready(function(){
    $('#board_btn_filter').on('click',function(){
        $('.board').each(function(index,val){
            $(this).attr('hidden',false);
        
        });
    });
});
</script>

@include('notify::messages')
<script src="{{asset('dashboard/assets/js/notify.js')}}"></script>
@yield('js')
</body>
</html>
