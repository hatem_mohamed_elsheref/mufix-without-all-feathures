<!doctype html>
<html lang="en" style="" class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface no-generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
<head>
    <title>{{strtoupper($setting->general_site_name)}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="application-name" content="MUFIX"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('site/assets/fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/assets/css/notify.css')}}">

    <link rel="stylesheet" href="{{asset('site/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{asset('site/assets/css/jquery.fancybox.min.css')}}">

    <link rel="stylesheet" href="{{asset('site/assets/css/bootstrap-datepicker.css')}}">

    <link rel="stylesheet" href="{{asset('site/assets/fonts/flaticon/font/flaticon.css')}}">

    <link rel="stylesheet" href="{{asset('site/assets/css/aos.css')}}">


    <link rel="stylesheet" href="{{asset('site/assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/font-awesome.css')}}">

    <link rel="stylesheet" href="{{asset('site/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/mystyle.css')}}">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Comfortaa:display=swap');
        #projectFacts .fullWidth{
            background-image:url({{asset('setting/'.$setting->community_main_image)}});
        }
    
    </style>

    {{-- <link rel="icon" type="image/png" href="logo/apple-icon.png"> --}}
     <link rel="icon" type="image/x-icon" href="{{ url('favicon.ico') }}"> 
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" >
