@extends('site.layout.app')

@section('content')



    <!--  Start Post Title   -->
        <div class="container">
            <div class="row align-items-center justify-content-center">


                <div class="col-md-6 mt-lg-5 text-center">
                    <h1>{{$post->title}}</h1>
                    <p class="post-meta">{{date_format(date_create($post->created_at),'M - D - Y')}} &bull; Posted by <span>{{$post->user->name()}}</span></p>

                </div>

            </div>
        </div>
    </div>
    <!--  End Post Title   -->

    <!--  Start Post    -->
    <section class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 blog-content">
                    <div class="row mb-5">
                        <!--              <div class="col-lg-12">-->
                        <!--                <figure><img src="images/img_3.jpg" alt="Image" class="img-fluid">-->
                        <!--                  <figcaption>This is an image caption</figcaption></figure>-->
                        <!--              </div>-->
                        <div class="col-lg-12">
                            <figure><img src="{{asset('uploads/posts/site/'.$post->image)}}" alt="Image" class="img-fluid post-image">
                                <figcaption>This is an image caption</figcaption></figure>
                        </div>
                    </div>
                    <p class="lead">{!! $post->content !!}</p>
                    <div class="pt-5">
                        <p>Category:  <a href="{{route('blog_cat',$post->category->id)}}">{{$post->category->name}}</a>  Tags:
                            @foreach($post->tags as $index=>$tag)
                                <a href="{{route('blog_tag',$tag->id)}}">
                                    {{$tag->name}}
                                    @if($index+1<$post->tags->count())
                                    ,
                                    @endif
                                </a>
                            @endforeach
                            </p>
                    </div>




                </div>
                <div class="col-md-4 sidebar">

                    <div class="sidebar-box" style="padding: 0px">
                        <div class="categories">
                            <h3>Categories</h3>
                            @foreach($categories as $item)
                                <li><a href="{{route('blog_cat',$item->id)}}">{{$item->name}}<span>({{$item->posts->count()}})</span></a></li>
                            @endforeach

                        </div>
                    </div>
{{--                    <div class="sidebar-box">--}}
{{--                        <img src="{{asset('uploads/posts/dashboard/'.$post->image)}}" alt="Image placeholder" class="img-fluid mb-4">--}}
{{--                        <h3>About The Author</h3>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>--}}

{{--                    </div>--}}


                </div>
            </div>
        </div>
    </section>
    <!--  End Post    -->


@endsection
