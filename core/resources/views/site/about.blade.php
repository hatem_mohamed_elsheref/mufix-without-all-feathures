 @extends('site.layout.app')

@section('content')


        <div class="container">
            <div class="row align-items-center justify-content-center">


                <div class="col-md-8 mt-lg-5 text-center">
                    <h1 class="text-uppercase" data-aos="fade-up"></h1>
                    <p class="mb-5 desc"  data-aos="fade-up" data-aos-delay="100">

                    </p>

                </div>

            </div>
        </div>

        <a href="#about-section" class="mouse smoothscroll">
        <span class="mouse-icon">
          <span class="mouse-wheel"></span>
        </span>
        </a>
    </div>

        <div class="site-section cta-big-image" id="about-section">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-12 text-center" data-aos="fade">
                        <h2 class="section-title mb-3">About Us</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
                        <figure class="circle-bg">
                            <img src="{{asset('setting/'.$setting->community_main_image)}}" alt="Image" class="img-fluid img-rounded" style="border-radius: 10px ">
                        </figure>
                    </div>
                    <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
                        <div class="mb-4">
                            <h3 class="h3 mb-4 text-black main-color-text">{{$setting->community_title}}</h3>
                            <p>{{$setting->community_description}}.</p>
                        </div>
                        <div class="mb-4">
                            <ul class="list-unstyled ul-check success">
                                <li>{{$setting->community_feature_1}}</li>
                                <li>{{$setting->community_feature_2}}</li>
                                <li>{{$setting->community_feature_3}}</li>
                                <li>{{$setting->community_feature_4}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--mix-->
    <section class="site-section" id="portfolio-sections">


        <div class="container">


            <div class="row mb-3">
                <div class="col-12 text-center" data-aos="fade">
                    <h2 class="section-title mb-3">Our Team</h2>
                </div>
            </div>

            <span class="clearfix"></span>
            <div class="row justify-content-center mb-5" data-aos="fade-up">
                <div id="filters1" class="filters1">
                </div>
                <div id="filters" class="filters text-center button-group col-md-10">
            
                    <button class="btn btn-primary active" data-filter="*" id="all_btn_filter">All</button>
                    <button class="btn btn-primary" data-filter=".board" id="board_btn_filter">Board</button>
                    @foreach($teams as $team)
                        <button class="btn btn-primary" data-filter=".{{strtolower($team->name)}}">{{$team->name}}</button>
                    @endforeach

                </div>
            </div>
            <div class="row mb-3">
                <div class="col-12 text-center" data-aos="fade">
                    <div class="input-group" style="width: 22%;float: right;">
                        <select class="custom-select" id="current_year" name="year" onchange="go('about',document.getElementById('current_year').value)">
                            <option selected>Choose...</option>
                            @for($i=2006;$i<=date('Y',time());$i++)
                                {{--                            {{($i==date('Y',time()))?'selected':''}}--}}
                                <option value="{{$i}}" >{{$i}}</option>
                            @endfor
                        </select>

                    </div>
                </div>
            </div>
            <div id="posts" class="row no-gutter text-center">
                @foreach($memberes as $member)
                    @foreach($member->team as $item)
                        <div class="col-6 col-lg-2 mb-4 item {{strtolower($item->name)}}" data-aos-delay="">
                            <div class="team-member">
                                <figure>
                                    <ul class="social">
                                        <li><a href="{{$member->fb}}"><span class="icon-facebook"></span></a></li>
                                        <li><a href="{{$member->tw}}"><span class="icon-twitter"></span></a></li>
                                        <li><a href="{{$member->linkedin}}"><span class="icon-linkedin"></span></a></li>
                                    </ul>
                                    <img src="{{asset('uploads/memberes/site/'.$member->image)}}" alt="Image" class="img-fluid xyz" style="width: 180px;height: 200px">
                                </figure>
                                 <div class="p-3" style="height:120px">
                                    <h3 class="main-color-text" style="font-size:15px">{{$member->name}}</h3>
                  <span class="position_" style="font-size:9px;word-spacing:0px;text-transform:uppercase;color:#cccccc">{{$item->name}} {{$member->role}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
                @foreach($board as $member)
                    <div class="col-6 col-lg-2 mb-4 item board flex-fill text-center" data-aos-delay="" hidden>
                        <div class="team-member">
                            <figure>
                                <ul class="social">
                                    <li><a href="{{$member->fb}}"><span class="icon-facebook"></span></a></li>
                                    <li><a href="{{$member->tw}}"><span class="icon-twitter"></span></a></li>
                                    <li><a href="{{$member->linkedin}}"><span class="icon-linkedin"></span></a></li>
                                </ul>
                                <img src="{{asset('uploads/memberes/site/'.$member->member[0]->image)}}" alt="Image" class="img-fluid xyz" style="width: 180px;height: 200px">
                            </figure>
                             <div class="p-3" style="height:120px">
                                <h3 class="main-color-text"  style="font-size:15px">{{$member->member[0]->name}}</h3>
                                <span class="position_" style="font-size:9px;word-spacing:1px;text-transform:uppercase;color:#cccccc">Leader {{$member->team[0]->name}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {!! $memberes->render() !!}
    </section>
    <div id="projectFacts"></div>
    @endsection
    @section('js')
    <script>
        let memberesPaginator=document.getElementsByClassName('pagination')[0];
        memberesPaginator.className='pagination justify-content-center';
        memberesPaginator.style.marginLeft=null;
        
        console.log(memberesPaginator);
    </script>
    @endsection