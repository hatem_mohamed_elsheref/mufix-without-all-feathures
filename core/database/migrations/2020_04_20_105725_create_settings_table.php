<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('general_site_name');
            $table->string('general_site_logo');
            $table->string('community_title');
            $table->text('community_description');
            $table->string('community_feature_1');
            $table->string('community_feature_2');
            $table->string('community_feature_3');
            $table->string('community_feature_4');
            $table->string('community_main_image');
            $table->string('communication_fb');
            $table->string('communication_tw');
            $table->string('communication_inst');
            $table->string('communication_email');
            $table->string('communication_address');
            $table->string('youtube_feature_1_icon');
            $table->string('youtube_feature_1_title');
            $table->string('youtube_feature_1');
            $table->string('youtube_feature_2_icon');
            $table->string('youtube_feature_2_title');
            $table->string('youtube_feature_2');
            $table->string('youtube_video');
            $table->string('app_feature_1_icon');
            $table->string('app_feature_1_title');
            $table->string('app_feature_1');
            $table->string('app_feature_2_icon');
            $table->string('app_feature_2_title');
            $table->string('app_feature_2');
            $table->string('app_feature_3_icon');
            $table->string('app_feature_3_title');
            $table->string('app_feature_3');
            $table->string('app_feature_4_icon');
            $table->string('app_feature_4_title');
            $table->string('app_feature_4');
            $table->string('app_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
