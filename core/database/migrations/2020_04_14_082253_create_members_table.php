<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('role')->default('Member')->nullable();
            $table->string('image');
            $table->year('start');
            $table->year('end');
            $table->string('fb')->default('#')->nullable();
            $table->string('tw')->default('#')->nullable();
            $table->string('insta')->default('#')->nullable();
            $table->string('linkedin')->default('#')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
