<?php

use Illuminate\Database\Seeder;
use App\Setting;
class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'general_site_name'=>'Mufix',
            'general_site_logo'=>'logo.png',
            'community_title'=>'For the next great business',
            'community_description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo tempora cumque eligendi in nostrum labore omnis quaerat.',
            'community_feature_1'=>'Officia quaerat eaque neque',
            'community_feature_2'=>'Possimus aut consequuntur incidunt',
            'community_feature_3'=>'Lorem ipsum dolor sit amet',
            'community_feature_4'=>'Consectetur adipisicing elit',
            'community_main_image'=>'main.jpg',
            'communication_fb'=>'#',
            'communication_tw'=>'#',
            'communication_inst'=>'#',
            'communication_email'=>'support@mufix.org',
            'communication_address'=>'Shebin-ElKom Menoufia Egypt',
            'youtube_feature_1_icon'=>'fa fa-user',
            'youtube_feature_1_title'=>'Web & Mobile Specialties',
            'youtube_feature_1'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.',
            'youtube_feature_2_icon'=>'fa fa-book',
            'youtube_feature_2_title'=>'Intuitive Thinkers',
            'youtube_feature_2'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.',
            'youtube_video'=>'https://www.youtube.com/embed/9rIC_X7Zg9k',
            'app_feature_1_icon'=>'fa fa-heart',
            'app_feature_1_title'=>'Web & Mobile Specialties',
            'app_feature_1'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.',
            'app_feature_2_icon'=>'fa fa-times',
            'app_feature_2_title'=>'Intuitive Thinkers',
            'app_feature_2'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.',
            'app_feature_3_icon'=>'fa fa-fire',
            'app_feature_3_title'=>'Web & Mobile Specialties',
            'app_feature_3'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.',
            'app_feature_4_icon'=>'fa fa-warning',
            'app_feature_4_title'=>'Intuitive Thinkers',
            'app_feature_4'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.',
            'app_image'=>'app.png'
        ]);
    }
}
