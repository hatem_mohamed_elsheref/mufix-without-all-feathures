<?php

use Illuminate\Database\Seeder;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
            'HTML','CSS','JS','PHP','SQL','MYSQL','PYTHON','VUE JS',
            'REACT JS','ANGULAR','C#','C++','JAVA','JAVAFX','RUBY','.NET',
            'SPRING','LARAVEL','CODEIGNITER','GITHUB','GIT'];
        foreach ($data as $d){
            \App\Tag::create([
                'name'=>$d
            ]);
        }
    }
}
