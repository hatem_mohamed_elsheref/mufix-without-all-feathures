<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user=User::create([
           'first_name'=>'Hatem Mohammed',
           'last_name'=>'Elsheref',
           'email'=>'hatem@app.com',
           'phone'=>'01090703457',
           'password'=>bcrypt('12345')
       ]);
       $user->attachRole('super_admin');
    }
}
