<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(LaratrustSeeder::class);
         $this->call(AdminSeeder::class);
        //  $this->call(TagsSeeder::class);
        //  $this->call(CategoriesSeeder::class);
         $this->call(SettingSeeder::class);
    }
}
