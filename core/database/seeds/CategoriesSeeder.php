<?php


use Illuminate\Database\Seeder;
use App\Post;
class CategoriesSeeder extends Seeder
{



    public function run()
    {
        factory(App\Post::class,10)->create();
       // $data=['Events','Trips','Sessions','Trainning','Workshops'];
        $data=['WEB Development','Android Development','Desktop Development',
            'IOS Development','IOT','AI & ML','AR & VR'];
        foreach ($data as $d){
            \App\Category::create([
                'name'=>$d
            ]);
        }

    }
}
