<?php

Route::group(['prefix'=>'control','middleware'=>['auth']],function (){

//    Dashboard Route
    Route::get('/admin','DashboardController@index')->name('welcome');
//    Users Route
    Route::get('/profile','UserController@showProfile')->name('users.show_profile');
    Route::put('/profile','UserController@profile')->name('users.update_profile');
    Route::resource('/users','UserController')->except('show');
    //    Posts Route
    Route::get('/posts/type/{status}','PostController@type')->name('posts.type');//show all posts by status
    Route::resource('/posts','PostController')->except(['show']);
//    Categories Route
    Route::resource('/categories','CategoryController');
//    Tags Resource
    Route::resource('/tags','TagController');
//    Activity Route
    Route::resource('/activities','ActivityController');
//    Activity Posts Route
    Route::get('/activity-posts/type/{slug}','ActivityPostController@type')->name('activity-posts.type');//show all posts by status
    Route::resource('/activity-posts','ActivityPostController')->except('show');
//    Testimonial Route
    Route::resource('/testimonial','TestimonialController')->except('show');
    //Upload Images Route
    Route::get('/file/home','FileUploadController@index')->name('filemanager');
    Route::get('/file/gallary','FileUploadController@gallary')->name('gallary');
    Route::get('/file/uploader','FileUploadController@create')->name('upload');
    Route::post('/file/uploader/{slug}','FileUploadController@dropzone')->name('dropzone');
    Route::post('/file/{id}','FileUploadController@status')->name('file.status');
    Route::delete('/file/{id}','FileUploadController@destroy')->name('file.destroy');
//    Team Route
    Route::resource('/team','TeamController')->except('show');
//    Memberes Route
    Route::resource('/member','MemberController')->except('show');
//  Board Route
    Route::resource('/board','BoardController')->except('show');
//  Setting Route
    Route::resource('/setting','SettingController')->only(['index','update']);

    Route::resource('/articles','ArticleController')->except(['show']);
});

//
//Route::get('/posts','PostController@index')->name('posts.index');//index
//Route::get('/posts-type/{status}','PostController@type')->name('posts.type');//status
////Route::get('/posts/{post}','PostController@show')->name('posts.show');//show
//Route::get('/posts/create','PostController@create')->name('posts.create');//create
//Route::post('/posts','PostController@store')->name('posts.store');//store
//Route::get('/posts/{post}/edit','PostController@edit')->name('posts.type ');//edit
//Route::put('/posts/{post}','PostController@update')->name('posts.update');//update
//Route::delete('/posts/{post}','PostController@destroy')->name('posts.destroy');//delete
//

