<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/reset-cache', function () {
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('key:generate');
    return redirect()->route('home');
 });

Auth::routes(['register'=>false]);

Route::get('/index','SiteController@index')->name('home');//site
Route::get('/home','SiteController@index')->name('home');//site
Route::get('/','SiteController@index')->name('home');//site

Route::get('/about/{year?}','SiteController@about')->name('about');//site

Route::get('/blog','SiteController@blog')->name('blog');//site

Route::get('/posts/tag/{slug}','SiteController@getPostByTag')->name('blog_tag');//site

Route::get('/posts/category/{slug}','SiteController@getPostByCategory')->name('blog_cat');//site

Route::get('/post/{id}','SiteController@getPost')->name('post');//site

Route::get('/activity-blog/{slug}','SiteController@activityBlog')->name('blog_activity');//site

Route::get('/activity-post/{id}','SiteController@activityPost')->name('post_activity');//site

Route::get('/contact','SiteController@contact')->name('contact');//site


