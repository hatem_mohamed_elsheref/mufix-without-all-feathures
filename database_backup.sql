-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2020 at 09:04 PM
-- Server version: 10.2.33-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mufix_website_2020_2021`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'HR', '2020-09-13 08:46:23', '2020-09-13 08:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `activity_posts`
--

CREATE TABLE `activity_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dir` enum('ltr','rtl') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ltr',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_posts`
--

INSERT INTO `activity_posts` (`id`, `title`, `description`, `content`, `image`, `user_id`, `activity_id`, `status`, `dir`, `created_at`, `updated_at`) VALUES
(1, 'Cairo ict by hatem 2020', 'Trips', '<p>Hell cairo</p>', 'gocBiXVps0p6u8MKfsiYb3T7vWUe6ywX8fhvSFvV.png', 1, '1', 1, 'ltr', '2020-09-13 08:54:09', '2020-09-13 08:58:52'),
(2, 'this is my second post from laptop in internet explorer browser', 'this is my second post from laptop in internet explorer browser this is normal description', '<p>hello y osra b leader alk</p>', 'Jpau9Dc2vMf0Z37kHoBT6UDC4ZyiKWBR3ToA99Sa.jpeg', 1, '1', 1, 'ltr', '2020-09-13 08:58:38', '2020-09-13 08:58:47'),
(3, 'Florence', 'Shewaet nasapeen', '<p>Hello every one</p>', 'PEjPR6eWTWcBjda0l0ZuVge4Zaq1mA99u0NLJ1n0.png', 1, '1', 1, 'ltr', '2020-09-13 09:16:27', '2020-09-13 09:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dir` enum('ltr','rtl') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ltr',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `content`, `category_id`, `user_id`, `image`, `status`, `dir`, `created_at`, `updated_at`) VALUES
(1, 'Hacking', 'dasdad', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><br /><br /></p>\r\n<div id=\"gsTopBarTitleWrap\" class=\"hideOverflow gsTopBarTitleWrap\" style=\"box-sizing: inherit; max-width: 100%; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; display: inline-block; margin-bottom: 8px; color: #444444; font-family: system-ui, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; text-align: center;\">\r\n<div id=\"faviconWrap\" class=\"faviconWrap\" style=\"box-sizing: inherit; display: inline-block; vertical-align: bottom; margin-bottom: -1px; margin-right: 10px;\"><img id=\"gsTopBarImg\" class=\"gsTopBarImg\" style=\"box-sizing: inherit; height: 16px; width: 16px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEyklEQVRYR72XXWgcVRTH/2d2N213bWmNYjTFoijailYrpEXzsKmYune6W5u6UkERfRCpDyr6UCzoipVC8QPFj0IFX3xpYyrJZu5ttmmMJFK7bfygCBXJgyISrFEKTZumM3PkjjvrZLKbTFL1wLKz9+P8f/fec8+ZJfyPlslkVjJzq2EY41LKI1qa/kt90zRXEFGamTfqD4A1Ab2ilDL3rwK0t7enEolEa0XsPgB3ATDqLTKVSjVeFkA+n2+YmJjYAECvTn/0cyLqrsbj8aZ5ARQKBaNcLq/TKyQivcJWAMmogqFxZ6WUy+cE2LRp023xeNw/wzSA5QsUDE/7REr52AyAbDZ7g+u6WlCvsA1A0wIFLwEoM/NRImoE8EzIz5NSyo99ABJCfADgcQBLFijoMvO3AAaI6Kht20OlUmlC+xJC9AIwg34Nw1jV29v7sweQyWR2E9GuBQifBqBXOGDb9mBfX98fYR/pdDqeTCZ1+9JA36iU8qZqHhBCnARwdwSAnyorHIjFYgM9PT2/zjUnk8lsIKJjwXFEtN+yrKeCADsB7KnhzCaiouu6SgsrpUbnEgz3m6a5i5l3B9uZebtS6sC0TCiEGAZwbw2BUzoglVJn5iteOf+jlRzhT2fbtptKpdJv0wAqd/wjAE/UEDoipWwPt+e6+ZpEHAUQ8uziMAzsPCToF39cOp1enEwm/wSwODD3lJTyDv/3tGs4G0QsFltdLBZ10HmWt7jJBT4HcGvA+btdJj3r/85kMjph6R0I2jtSyudqAujGOhCTjuM0+1HecZivJccTv2Xa2QLlQyatDwDMuF1EtMWyrJ66AD7EiRMnCq7r7vDOieh1KeXb+rmeuOeQsKNL0Ie+cyGEjn5dH3xzpqamGvv7+8/OChBYwSL9rJS6qL83HzzfvKgBx5FY0hzaVhDj099TeGSwjWzdl8vlltq2re9/PDC2LKWs7tDfzBEt183XxSfP/EhXXD2j+ITFtUshxGYAxZD7PVLKl4JtkQC0eIKmjiHWcP0MXvtC7/iyJVv9lQe2/y0Az08TI7rfsqz+eQHki9zsGl7A3RwWd0aHcbG09wdynbSUcizYL4TQdWFtoO1iKpVa0dnZeSEygCdOGATBy9tB0+JTpb2A6+hmfT3bfIhsNnuV4zg60VR3mJkHlVK6uk6zukfgJZkYhiOI+w6rEKZp5pn5YFCJmV9WSr0WGWCbxbo26BoRMu6a3PfgOXZsXbrD5kEAeAXA0yGAVqXUl5EBOiw+SaEK6Ud7+otX3XK5XC9ta4gGADf6YkR0bmxs7MqRkRH9khLtCDokv0CMN6pOQvd8jtoR1lFSSlFjx/4JkkKBjVPr8YAedPtxHP5+DeJOCg8RYBKji86ju/Nh8iLOt6gQzPyiUurNugAdklcSo6/6x4Ew2CVoRsTWchAFgpnXKaW+qQmQ6+aliRi+Dke76+LOz7L0Xa1J4bY5ICZaWlqWFQoFtybANov12+p7oU77ko1VPVtozleuCMdxWkq5ut5CqMPi9wnwql7VGPu7NpP3zjYfq+zEEIB7qsFLtNGyLJ1JaxptLfJaw8AIgFhlxIHxJB4N5/b5gJimqXNEm+u6+5RSX80218uEOuU6MWyPORjqzFJ5PmKXO/Yv6u0TUW1E8AsAAAAASUVORK5CYII=\" /></div>\r\n&nbsp;<span id=\"gsTopBarTitle\" class=\"gsTopBarTitle\" style=\"box-sizing: inherit; font-size: 20px; cursor: default;\">laravel redirect back to post route, i get MethodNotAllowedHttpException</span></div>\r\n<div id=\"mouseposition-extension-element-full-container\" style=\"position: fixed; top: 0px; left: 0px; right: 0px; bottom: 0px; pointer-events: none; z-index: 2147483647; font-weight: 400;\">\r\n<div id=\"mouseposition-extension-element-rect-display\" style=\"display: none; position: absolute; background: rgba(255, 255, 255, 0.7); outline: black solid 1px; font-size: 12px; z-index: 2147483647; justify-content: center; align-items: center; user-select: none; cursor: default; color: #000000; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; width: 0px; height: 0px;\">\r\n<pre style=\"flex: 1 1 0%; text-align: center; background-color: rgba(255, 255, 255, 0.7); color: #000000; min-width: 42px; min-height: 12px; transition: all 1s ease 0s;\">&nbsp;</pre>\r\n</div>\r\n<pre id=\"mouseposition-extension-element-coordinate-display\" style=\"position: absolute; display: none; background: #ffffff; font-size: 12px; line-height: 14px; border-radius: 3px; border-width: 1px; border-color: #222222 black #333333; border-style: solid; padding: 3px; z-index: 2147483647; color: #222222; user-select: none; cursor: default; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\"></pre>\r\n</div>\r\n</body>\r\n</html>', 2, 1, 'mK2yjqmQZpGbyH1hQ43DEYxwuc1CfpmF99v6AAvg.jpeg', 0, 'ltr', '2020-09-12 19:33:32', '2020-09-12 19:33:32'),
(2, 'What Ai ?', '💥حاسبات و معلومات الكلية دي سمعت عنها؟😄\r\nهى اللي بتطلع كل الناس دي المهتمين بال \"Computer Science\" \r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر\r\nبرضه مش فاهم 🙄\r\nالأول خليني اقولك ان احنا مش ب', 'خبار السوشيال ميديا معاكوا أي ..؟ 😄\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔\r\n💥حاسبات و معلومات الكلية دي سمعت عنها؟😄\r\nهى اللي بتطلع كل الناس دي المهتمين بال \"Computer Science\" \r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر\r\nبرضه مش فاهم 🙄\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)\r\nاحنا اللي بنعمل الابليكاشنز و ال \"websites\" زي موقع نتيجة الثانوية العامة كدا😃\r\nاللي عامل الموقع دا \"software engineer\" ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات\r\nطب بندرس ايه في الكلية..؟ 🤔\r\nبندرس لغات برمجة زي \"c++\" و \"java\" و حاجات تانية زي \"Data structure\" و \"Algorithms\"\r\nوكل دي حاجات مجرد Tools بتساعدك عشان تبدأ تدخل أي مجال أنت حابه 💖\r\n ممكن متبقاش فاهم المصطلحات دي دلوقتي 😅\r\nلكن من الاخر احنا بنفضل في الكلية ناخد حاجات تساعدنا في ال \"Problem solving\" و في الشغل بعد الكلية كمان\r\nو بناخد \"basics\" لكل \"Technology\" واحنا بنقولك من دلوقتي ان احنا بناخد \"basics\" عشان تبقى عارف ان الكلية معتمدة على ال \"Self Study\" خصوصًا ان مجالنا دا \"dynamic\" يعني بتفضل طول عمرك تتعلم و ما أكثرها ال \"materials\" الموجودة على النت و المواقع اللي بتقدم كورسات كويسة جدََا \"online\"\r\nكمان لازم تعرف إن مواد الكلية  مهمة جدََا، أنت بتدرس في الكلية مواد كتير وكل مادة ليها هدف بتساعدك بشكل كبير جدََا لما تيجي تبدأ في مجال معين زي مثلََا أنت بتاخد Math 1 و احصا1 واحصا2  وهكذا المواد دي بتساعك في مجالات كتير زي ال \"Machine Learning\" ودا مجال كبير جدََا\r\nبيستخدم في التقنيات الحديثة *زي عربية تسلا أو صواريخ الفضاء * كل دول و أكتر حاجات تقدر تكودها وتكتبها 👨🏻‍💻\r\nبتسأل نفسك هو مجال الكلية دا ليه شغل ولا لأ!! 🤔\r\nأنت لو دخلت على جوجل وعملت سيرش عن الوظائف المطلوب دايمََاا هتلاقي المبرمج مكتسح، ودا مش علشان هما اللي مصممين الموقع ولا حاجة لأ 😂 دا لأن فعلََا هي من أكتر الوظائف المطلوبة حاليََا 🔥\r\nطب ايه شروط الالتحاق بالكلية!! 🤔\r\n💥الكلية بتدخلها من شعبة علمي رياضة و السنة اللي فاتت اخدت من حد أدنى 90.7% بيختلف حسب تنسيق كل سنة\r\n و فيه اقسام خاصة لعلمي علوم المتاح في جامعة المنوفية هو \"bioinformatics\" هنبقا نتكلم عنه في بوستات جاية😌\r\n\r\n💥الكلية مش بتحتاج امتحان قدرات أو حاجة و هى بتنزل في التنسيق عادي بس مش بتنزل لطلاب علوم طالب علمي علوم لازم يروح يحول اوراقه للكلية\r\n💥مش ضروري خالص ابدََا ابدََا يكون عندك خلفية عن البرمجة أو الكمبيوتر بشكل عام أنت بتدخل الكلية بتعرف كل حاجة❤️\r\n\r\n📎الكلية الدراسة فيها اربع سنين و فيها أقسام تخصص هنبقا نتكلم عنها في بوستات جاية🙏 و التخصص بيكون في سنة تالتة أو رابعة على حسب كل جامعة و في جامعة المنوفية من سنة تالتة\r\n\r\n📎الدراسة بتكون بالانجليزي و دا مش هيكون عائق خالص بالمناسبة و هتتعود بعد فترة قصيرة جدََا 😅\r\n\r\n#FCI\r\n#Software_Engineer\r\n#MUFIX_Communit', 4, 1, '9ioTyT9RP9Dzq83MwmtulffFh7bGL5T8aOb6Ai6T.jpeg', 0, 'ltr', '2020-09-12 22:26:09', '2020-09-12 22:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article_tag`
--

INSERT INTO `article_tag` (`id`, `tag_id`, `article_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `year` year(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `year`, `created_at`, `updated_at`) VALUES
(29, 2020, '2020-05-01 18:20:44', '2020-05-01 18:20:44'),
(39, 2020, '2020-05-05 11:29:18', '2020-05-05 11:29:18'),
(41, 2019, '2020-05-06 23:04:30', '2020-05-06 23:04:30'),
(42, 2020, '2020-05-06 23:04:44', '2020-05-06 23:04:44'),
(43, 2018, '2020-05-06 23:06:16', '2020-05-06 23:06:16'),
(44, 2018, '2020-05-06 23:09:59', '2020-05-06 23:09:59'),
(45, 2019, '2020-05-06 23:10:29', '2020-05-06 23:10:29'),
(46, 2020, '2020-05-06 23:11:29', '2020-05-06 23:11:29'),
(47, 2019, '2020-05-06 23:14:37', '2020-05-06 23:14:37'),
(48, 2020, '2020-05-06 23:14:52', '2020-05-06 23:14:52'),
(49, 2019, '2020-05-06 23:18:43', '2020-05-06 23:18:43'),
(50, 2020, '2020-05-06 23:18:54', '2020-05-06 23:18:54'),
(51, 2019, '2020-05-06 23:19:55', '2020-05-06 23:19:55'),
(52, 2018, '2020-05-06 23:20:45', '2020-05-06 23:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `board_member`
--

CREATE TABLE `board_member` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_id` int(11) NOT NULL,
  `board_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_member`
--

INSERT INTO `board_member` (`id`, `member_id`, `board_id`, `created_at`, `updated_at`) VALUES
(29, 30, 29, NULL, NULL),
(39, 29, 39, NULL, NULL),
(41, 122, 41, NULL, NULL),
(42, 122, 42, NULL, NULL),
(43, 122, 43, NULL, NULL),
(44, 123, 44, NULL, NULL),
(45, 123, 45, NULL, NULL),
(46, 123, 46, NULL, NULL),
(47, 124, 47, NULL, NULL),
(48, 124, 48, NULL, NULL),
(49, 125, 49, NULL, NULL),
(50, 125, 50, NULL, NULL),
(51, 30, 51, NULL, NULL),
(52, 30, 52, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `board_team`
--

CREATE TABLE `board_team` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` int(11) NOT NULL,
  `board_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_team`
--

INSERT INTO `board_team` (`id`, `team_id`, `board_id`, `created_at`, `updated_at`) VALUES
(29, 11, 29, NULL, NULL),
(39, 7, 39, NULL, NULL),
(41, 10, 41, NULL, NULL),
(42, 10, 42, NULL, NULL),
(43, 10, 43, NULL, NULL),
(44, 7, 44, NULL, NULL),
(45, 9, 45, NULL, NULL),
(46, 9, 46, NULL, NULL),
(47, 12, 47, NULL, NULL),
(48, 12, 48, NULL, NULL),
(49, 8, 49, NULL, NULL),
(50, 8, 50, NULL, NULL),
(51, 11, 51, NULL, NULL),
(52, 11, 52, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Android Development', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(3, 'Desktop Development', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(4, 'IOS Development', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(5, 'IOT', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(6, 'AI & ML', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(8, 'News', '2020-09-04 06:26:59', '2020-09-04 06:26:59'),
(9, 'Hatem Elsheref', '2020-09-11 11:30:35', '2020-09-11 11:30:35'),
(10, 'Cloud Computing', '2020-09-11 11:31:36', '2020-09-11 11:31:36'),
(11, 'VR & DASDSDASD', '2020-09-11 11:32:10', '2020-09-11 11:32:24'),
(12, 'Game Development', '2020-09-11 11:33:22', '2020-09-11 11:33:22'),
(16, 'ICT', '2020-09-12 05:15:15', '2020-09-12 05:15:15'),
(17, 'admin3', '2020-09-12 18:11:51', '2020-09-12 18:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `filemanagers`
--

CREATE TABLE `filemanagers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activity_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `filemanagers`
--

INSERT INTO `filemanagers` (`id`, `activity_id`, `status`, `image`, `created_at`, `updated_at`) VALUES
(16, 0, 1, 'oFR1perQormcbimVtVN0EU4XtDDAmCicEwOxxQ24.jpeg', '2020-08-23 22:07:15', '2020-08-23 22:08:05'),
(17, 0, 1, 'aZxicpqyzqnsaDbMVhkYG5byGn6zwG1ZPkdPqZXD.jpeg', '2020-08-23 22:07:25', '2020-08-23 22:08:08'),
(18, 0, 1, 'AoRRwtKtBadVIHfS2ZvOlHSBkpyjDIqicwagcs8A.jpeg', '2020-08-23 22:07:27', '2020-08-23 22:08:10'),
(19, 0, 1, '1cEWTpEZHiUxklAGu35w1m53LGCYtplrE5s2nJCh.jpeg', '2020-08-23 22:07:36', '2020-08-23 22:08:13'),
(20, 0, 1, 'KWBQpZfrHBbysB8UzjpfwxDhQzHUWAsVCtdbClAj.jpeg', '2020-08-23 22:07:44', '2020-08-23 22:08:21'),
(21, 0, 1, 'PqcWdMHFNLtCHyc8MXou66He8bny11ZARoUysOkU.jpeg', '2020-08-23 22:07:44', '2020-08-23 22:08:17'),
(22, 0, 1, 'aV9OyCB7tCnsxjfbUgGCPn40Pg6R1h8Oif9POqfr.jpeg', '2020-08-23 22:09:22', '2020-09-02 16:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Member',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` year(4) NOT NULL,
  `end` year(4) NOT NULL,
  `fb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `tw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `insta` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `role`, `image`, `start`, `end`, `fb`, `tw`, `insta`, `linkedin`, `created_at`, `updated_at`) VALUES
(14, 'Islam El Hosary', 'MUFIX CO-Leader', 'xHea0P6P7VRpHQUojFJzYKjr90gbVT381oRIumdg.jpeg', 2006, 2009, 'https://m.facebook.com/islamelhosaryofficial/', 'https://twitter.com/islamelhosary?s=08', NULL, 'https://www.linkedin.com/in/islamelhosary', '2020-04-30 06:29:31', '2020-05-04 10:42:30'),
(26, 'Hatem Mohamed', 'Member', 'zLRuPaxxEJAbbV7G3Eyoci6lgMH9EIwd9KhTsSNv.jpeg', 2019, 2021, '#', '#', '#', '#', '2020-04-30 17:33:56', '2020-05-05 15:30:14'),
(29, 'Ahmed Saber', 'Leader', 'qvMvl4Ejfuj2o1GoWqJaPoOAkVMJer4TQAEbS1g1.jpeg', 2017, 2021, NULL, NULL, '#', NULL, '2020-04-30 17:42:56', '2020-06-16 01:59:42'),
(30, 'Ali El-Zenary', 'Leader', 'Ep0GnNFld2xrFcRVdDplhljLZkBIXcqVgL8fCTdN.jpeg', 2016, 2021, 'https://www.facebook.com/ali.zenary/', 'https://twitter.com/ali_zenary', '#', 'https://www.linkedin.com/in/ali-zenary/', '2020-05-01 18:19:23', '2020-06-16 02:16:11'),
(31, 'Mostafa Abdelnaser', 'Team Leader', '3GP0h1whDV8NdTMHR42oDKg5tECvHr3pjgFyWgMo.jpeg', 2007, 2009, 'https://www.facebook.com/mostafa.abdelnaser.mohammed', 'https://twitter.com/mostafaelnaser1', '#', 'https://www.linkedin.com/in/mostafatabal/', '2020-05-01 18:41:44', '2020-05-01 18:41:44'),
(32, 'Ahmed Samir', 'Member', 'q19V1ESDf0S8OypelPLj7QwH0ERg03HCNsybLWQ9.jpeg', 2008, 2011, 'https://www.facebook.com/a7medsamiir', '#', '#', 'linkedin.com/in/asaamir/', '2020-05-01 18:44:36', '2020-06-18 15:04:15'),
(33, 'Abdallah Abuouf', 'Team leader', 'sq37Bj4Fm98D7b5w09hVrfaNWEXUDWmKQYAGjPCi.jpeg', 2008, 2011, 'https://www.facebook.com/abdallah.abuouf', 'https://twitter.com/abdallah_abuouf', '#', 'http://linkedin.com/in/abuouf/', '2020-05-01 18:47:00', '2020-05-01 18:47:00'),
(34, 'Muhammad Shebl', 'Mufix Co-Leader', 'hBNOr3jh0V0coLPd0fZyflqRIEGFgsIuB3nKBinc.jpeg', 2009, 2013, 'https://www.facebook.com/MuhammadShebl', '#', '#', 'eg.linkedin.com/in/muhammadshebl', '2020-05-01 18:49:22', '2020-06-28 12:25:45'),
(35, 'Mahmoud  Elsayed', 'Member', 'Gr9biHPplHYWdEN0wAH71LVbgqY2vMjJtTNX2tPl.jpeg', 2009, 2011, 'https://www.facebook.com/mmea369', '#', '#', 'https://www.linkedin.com/in/mahmoud-mohamed-elsayed-4b0b84a8', '2020-05-01 18:52:21', '2020-06-28 12:26:16'),
(36, 'Osama El-Far', 'Member', 'hDqISc68cdWiaulFKuVcfpGnE7nFlrGhdCOnytWe.jpeg', 2010, 2013, 'https://www.facebook.com/osama.alfar.1', '#', '#', 'https://www.linkedin.com/in/osama-elfar', '2020-05-01 18:54:12', '2020-06-28 12:27:19'),
(37, 'Hany  Elmouhr', 'Member', '4erYMPi4oJ8V85ZDjEdQeo0uInesN1SB8oZ4VGNc.jpeg', 2010, 2013, 'https://www.facebook.com/hany.elmouhr', 'https://twitter.com/Hany_muhamed', '#', 'https://www.linkedin.com/in/hany-mohammed-515a2131/', '2020-05-01 18:57:15', '2020-05-01 18:57:15'),
(38, 'Mohamed Nofal', 'Leader', 'wDnfDfAfSMvgdfmmL7FtMXyEdRlxwgcAIhWdU0QP.jpeg', 2010, 2013, 'https://www.facebook.com/mohamed.nofal.712', 'https://twitter.com/Mohamd_Nofal', '#', 'https://linkedin.com/in/mohamed-nofal-11995333/', '2020-05-01 19:01:50', '2020-05-04 11:52:06'),
(39, 'Mohamed  kotb', 'Leader', 'cZJkidqqBeDAedE33SS2Hbb4rIvuufRGk7hVGJrs.jpeg', 2012, 2015, 'https://www.facebook.com/mohamedAhmedAlikotb', 'https://twitter.com/mohamedkotb2020', '#', 'www.linkedin.com/in/mohamed-kotb', '2020-05-01 19:03:59', '2020-05-05 09:53:02'),
(40, 'Nessma Adel', 'Leader', '4n2Q8BzbLwRQNxfEVdmqgDR1SUrWO11QrUwlTdBy.png', 2012, 2016, 'https://www.facebook.com/nessmaaadel986', '#', '#', 'https://www.linkedin.com/in/nessma-adel-b8095068', '2020-05-01 19:05:51', '2020-06-28 12:28:11'),
(41, 'Asmaa Reda', 'Leader', 'XFkkDsd8WgpQi2RZZEgS29ITmwNdcqv7jmwbqgPx.jpeg', 2012, 2015, 'https://www.facebook.com/asmaa.reda.79', 'https://twitter.com/asmaareda12', '#', 'eg.linkedin.com/in/AsmaaReda1.', '2020-05-01 19:10:49', '2020-05-05 11:48:58'),
(42, 'Amr  Saud', 'Co-Leader', 'uZ7rWU6TEpnyxDvEug4IkYoq5Q2lkfb0ln0kAuGa.jpeg', 2013, 2014, 'https://www.facebook.com/Amr.Hassan.1994', '#', '#', '#', '2020-05-01 19:18:20', '2020-06-28 12:28:41'),
(43, 'Saad Alenany', 'Member', '6FoKXZExvlNCl9AJYc3Q88r9j4R3fFYnkrsefMut.jpeg', 2014, 2018, 'https://www.facebook.com/eng.saad.alenany', 'http://www.twitter.com/EngSaad_Alenany', '#', 'https://www.linkedin.com/in/saad-alenany-a95168b4/', '2020-05-01 19:20:23', '2020-05-01 19:20:23'),
(44, 'Ahmed Mahfouz', 'Leader', 'tmezToogsDAu2siVsi9icduX243OYmERyEG7xMOo.png', 2012, 2015, 'https://www.facebook.com/amahfouz50', 'https://twitter.com/amahfouz50', '#', 'https://www.linkedin.com/in/amahfouz50/', '2020-05-01 19:33:43', '2020-05-01 19:33:43'),
(45, 'Hoda Ammar', 'Co-Leader', '6uZwzjJArX0qTRHCANYyTysEVOal2OJ7Hn3IizqZ.jpeg', 2014, 2018, 'https://www.facebook.com/Hodaahmed95', 'https://twitter.com/Ho_0Da', '#', 'https://www.linkedin.com/in/hodaammar95/', '2020-05-01 19:36:47', '2020-05-01 19:36:47'),
(46, 'Ahmed Mesalm', 'Member', 'VKQBuo7m4QhZ1X9DXGgHVxNxCJDEXCkfn4XBnea2.jpeg', 2014, 2016, 'https://www.facebook.com/ahmedmesalm11', 'https://twitter.com/AhmedMesalm222', '#', '#', '2020-05-02 16:05:44', '2020-06-28 12:29:01'),
(47, 'Mohamed Hossam', 'Leader', 'KHQXoOubqHrK78O9lYFRTLuxJKusxQOaIOD7S8w2.jpeg', 2014, 2017, 'https://www.facebook.com/Mohammed.hossam095', 'https://twitter.com/hossam__95', '#', 'https://www.linkedin.com/in/mohammedhossam95/', '2020-05-02 16:07:55', '2020-05-02 16:07:55'),
(48, 'Mahmoud Elbeltagy', 'Team leader', 'zqKVtAhDk7vGKWnTo8OR5EwvWUpYobTA7qiZcx89.jpeg', 2014, 2017, 'https://www.facebook.com/mahmoud96elbltagy', 'https://twitter.com/mahmoudelbltag2', '#', 'https://www.linkedin.com/in/mahmoudelbltagy/', '2020-05-02 16:12:55', '2020-05-02 16:12:55'),
(49, 'Mahmoud Wageeh', 'Co-Leader', 'PQEdCar0XIIYLnj0h8aZiGSbY9yP5ycKcbBtSGHx.jpeg', 2015, 2017, 'https://www.facebook.com/MahmoudWageeh94', 'https://twitter.com/MahmoudWageeh94', '#', 'https://www.linkedin.com/in/mahmoud-wageeh-a8377a99/', '2020-05-02 16:15:57', '2020-05-02 16:15:57'),
(50, 'Mahmoud kaboh', 'Member', 'aGn7TeFwufMC3a2C6m2G6SQJYvpgXmmTgOuO0LI3.jpeg', 2015, 2017, 'https://www.facebook.com/mahmoud.kabouh', 'https://twitter.com/MahmoudKaboh?s=09', '#', 'https://www.linkedin.com/in/mahmoud-kaboh-3398a414b', '2020-05-02 16:17:59', '2020-05-05 09:39:27'),
(51, 'Mohamed  Moawad', 'Member', 'CAK2GBQX48q8a8gJY4xe9t3HyWNzyzTmdkxnlJ7E.jpeg', 2015, 2017, 'https://www.facebook.com/m7mdfashkl', 'https://twitter.com/m7md_fashkl', '#', 'https://www.linkedin.com/in/m7md-fashkl/', '2020-05-02 16:20:52', '2020-05-02 16:20:52'),
(52, 'Mohamed Hammam', 'Member', 'PpZ0gBpIlozszGeryb0zOw58h0eXGb6hJeJVBqhX.jpeg', 2015, 2018, 'https://www.facebook.com/mohamed.hammam.pro', 'https://twitter.com/m_hammam_pro?fbclid=IwAR04Hv5BX5ZpLLsLn0ExoJNF-qkRVXypiVE6UA5DqgL2ft-brj-WWzd333E', '#', 'https://www.linkedin.com/in/mohamed-hammam-378902133/?fbclid=IwAR0EYIQJMqRV63lhY7zUpEArNfthT2HRqFzKEyuEKyMzCRnXp1LumYgVGEk', '2020-05-02 16:25:33', '2020-05-02 16:25:33'),
(53, 'Waleed Allam', 'Member', 'BJpGBlrliwZwNdJPj8AnNeQyTCkxZZGuy4G9le3P.jpeg', 2015, 2018, 'https://www.facebook.com/soft20199', '#', '#', '#', '2020-05-02 16:27:10', '2020-06-28 12:29:34'),
(54, 'Ahmed Samak', 'Member', 'KJSCtsywKwtv5La4l9lacABMHwBP2nKwg59IZ4uQ.jpeg', 2015, 2018, 'https://www.facebook.com/AhmedFishy', 'https://twitter.com/Ghostswhim', '#', 'https://www.linkedin.com/in/ahmed-samak-a580b4110/', '2020-05-02 16:30:09', '2020-05-02 16:30:09'),
(55, 'Mohamed Abdallah', 'Leader', 'hQnJhnLOkXiJlrCsLc6gwOGSybnJPpCFjMYj5B1Y.jpeg', 2015, 2017, 'https://www.facebook.com/mo7amed.3bdalla7', 'https://twitter.com/mo7amd_3bdalla7', '#', 'https://www.linkedin.com/in/mo7amed-3bdalla7/', '2020-05-02 16:33:11', '2020-05-05 09:30:48'),
(56, 'Sara Gamal', 'Member', 'XcN17nzuN2bfJXzVt9CtlYANwLVlnhVFnhLWJ4vm.jpeg', 2015, 2017, 'https://www.facebook.com/prosarahgamal', 'https://twitter.com/PROSarahGamal', '#', 'https://www.linkedin.com/in/prosarahgamal/', '2020-05-02 16:36:26', '2020-05-02 16:36:26'),
(57, 'Mahmoud Qussai', 'Team Leader', 'rPCCeC4PCWzRuWWAO1dfGQqeqVAbKIYyQff3kQEL.jpeg', 2015, 2018, 'https://www.facebook.com/Qussai.08', '#', '#', 'https://www.linkedin.com/in/mahmoud-qussai/', '2020-05-02 16:38:28', '2020-06-28 12:21:09'),
(58, 'Hager AbdElHakeem', 'Member', 'OTE14L1MPGk9Yj914gIoWIfJi7neYh5IHCsdTobL.jpeg', 2015, 2018, 'https://www.facebook.com/hager.abdelhakeem.9', 'https://twitter.com/HagerHakeem?s=08', '#', 'https://www.linkedin.com/in/hagar-abdelhakeem-548469116', '2020-05-02 16:41:35', '2020-05-05 09:51:14'),
(59, 'Mohamed El-ksas', 'Member', 'vCKhJuDGDobPWSnCDLsgGYQm0HNAUTeDpO4rqzkz.jpeg', 2016, 2019, 'https://www.facebook.com/medo.elksas', '#', '#', '#', '2020-05-02 16:44:24', '2020-06-28 12:22:51'),
(60, 'Mohamed Sameh', 'Member', 'jzIKOFORAj9DVY1KlJD5e8UOrN4NkkL07gaoiObP.png', 2017, 2019, 'https://www.facebook.com/thelastmanwell', '#', '#', 'https://www.linkedin.com/in/mohamedsamehmohamed/', '2020-05-02 16:46:58', '2020-06-28 12:34:54'),
(61, 'Mohamed Safwat', 'Memeber', '7eB2tSL4mloC9mqEzdQqU58DTnwMzq54p2CRMCZG.jpeg', 2017, 2019, 'https://www.facebook.com/profile.php?id=100007820736769', 'https://twitter.com/Mohamed_Sifo98?s=09', '#', '#', '2020-05-02 16:49:01', '2020-06-28 12:34:33'),
(62, 'Fatma Adel', 'Member', '1av4orMCbFBhJ7riSWCbRjfOSLXEy5Z0zD2AecVF.jpeg', 2017, 2019, 'https://www.facebook.com/fatma.adel.5621149', '#', '#', '#', '2020-05-02 16:50:49', '2020-06-28 12:34:11'),
(64, 'Khaled Kassem', 'Member', 'V8dslt12GBABKTPVEKsXOX5NISLsySL5t2i0ntrB.jpeg', 2017, 2018, 'https://www.facebook.com/profile.php?id=100005366115899', 'https://twitter.com/khaledkassem_', '#', 'https://www.linkedin.com/in/khaled-kassem/', '2020-05-02 16:54:08', '2020-05-02 16:54:08'),
(65, 'Abd-Elazez shendy', 'Member', 'lHxfIz03h337IjgJxAlG0jfeTnFIbwY0EywfRw5m.jpeg', 2017, 2019, 'https://www.facebook.com/zezoshindy', 'https://twitter.com/Abdelazez_10', '#', '#', '2020-05-02 16:56:38', '2020-06-28 12:33:41'),
(67, 'Khloud Hafez', 'Member', 'DQcoxigbkyNbJD9fEl9P4VA64I3uFhkTjZHOPLaL.jpeg', 2017, 2019, 'https://www.facebook.com/profile.php?id=100009335942641', 'https://twitter.com/lovelygirl506', '#', 'https://www.linkedin.com/in/khloud-hafez-44219a165/', '2020-05-02 17:00:10', '2020-05-05 10:50:18'),
(68, 'Manal Emad', 'Member', 'gB4htBOvZcxZR2VeYuBiLAHOfRT5CTgIwggzuRHb.jpeg', 2016, 2018, 'https://www.facebook.com/mony.moon.33', '#', '#', '#', '2020-05-02 17:01:52', '2020-06-28 12:32:02'),
(69, 'Amr AbdElaaty', 'Member', '16nUB0FlI1bBelNhsYolVIzyh16Pwml92QvfBKLm.jpeg', 2017, 2017, 'https://www.facebook.com/profile.php?id=100003936100620', '#', '#', '#', '2020-05-02 17:03:40', '2020-06-28 12:32:35'),
(70, 'Mahmoud Tarek', 'Member', 'ni7jpIKn4cFLmLNTSUEftsWGTej4pi2FVVMhbNYm.jpeg', 2017, 2019, 'https://www.facebook.com/mahmoud.tarek.3538039', '#', '#', '#', '2020-05-02 17:05:53', '2020-06-28 12:20:54'),
(71, 'Hassan  Elkhadrawy', 'Member', 'xhquOfPKp2nIaglMyqTMsxRRFnZPl2SLFoCn2hL3.jpeg', 2017, 2019, 'https://www.facebook.com/hassan.elkhadrawy', '#', '#', 'https://www.linkedin.com/in/hassan-ragab-33129b140', '2020-05-02 17:07:56', '2020-06-28 12:33:18'),
(72, 'Roa\'a Gamal', 'Leader', 'vkLQGqWZnRyO0PUam9cV1ly6nZcjQ31KTNbyQv1s.jpeg', 2016, 2018, 'https://www.facebook.com/roaa.gamal.3', 'https://twitter.com/Roaa__Gamal', '#', 'https://www.linkedin.com/in/roa-a-gamal-784a61139/', '2020-05-02 17:11:04', '2020-05-05 10:05:28'),
(73, 'Hager  Yassin', 'Member', '2WQbUOGE48NpvfhHBFQnVNwQGF8bv7yoILQU0vm8.jpeg', 2016, 2018, 'https://www.facebook.com/hager.yassin.988', 'https://mobile.twitter.com/KOKO99504129', '#', 'https://www.linkedin.com/in/hager-yassin-329b7813b', '2020-05-02 17:13:24', '2020-05-02 17:13:24'),
(74, 'Mohamed Elmallah', 'Member', 'eQIR6FBLLxqzLEu0hV0hz6QjcGTPoeEjXSlXqnHv.jpeg', 2010, 2015, 'https://www.facebook.com/mallahsoft', 'https://twitter.com/MallaHSOFT', '#', 'https://www.linkedin.com/in/mohamed-el-mallah-a6738a8a/', '2020-05-04 11:13:31', '2020-05-04 11:13:31'),
(75, 'Mohammed Barbary', 'Mufix Team Leader', 'DGhP5wT1xYheBhUTKeriVynrM3zqV0tCxFK1aWr1.jpeg', 2010, 2013, 'https://www.facebook.com/mohamed.barbary.5', '#', '#', 'https://www.linkedin.com/in/mohamedbarbary/', '2020-05-04 11:27:51', '2020-06-28 12:27:41'),
(76, 'Ahmed AbdElRaouf', 'Member', 'IrFgbSwNFGMH578We8GPnkgE6BEmTNhzYJJ1wssa.jpeg', 2016, 2017, 'https://www.facebook.com/ahmed.abohgel', '#', '#', 'https://www.linkedin.com/in/ahmedmohamedabdelraauf/', '2020-05-05 10:00:34', '2020-06-28 12:31:04'),
(77, 'Hossam AbdElkader', 'Member', 'BduJYuksh3iTEhILZ4eYdBRK3IiQcU9ZiVBSyxe1.jpeg', 2016, 2020, 'https://www.facebook.com/hossam203055', 'https://twitter.com/hossam203055', '#', 'https://www.linkedin.com/in/hossam203055', '2020-05-05 10:16:56', '2020-05-05 10:20:28'),
(78, 'Mohab Said', 'Member', 'M4wqIydRqo7NulaNoAgaZi7C1Fk6m0pe5S7D5D9u.jpeg', 2016, 2017, 'https://www.facebook.com/profile.php?id=100006274270996', 'https://twitter.com/hooba_Saeed', '#', 'https://www.linkedin.com/in/mohab-saeed-161549148/', '2020-05-05 10:25:50', '2020-05-05 10:25:50'),
(79, 'Salma Afify', 'Member', 'YQWoZErf2X5QV2h6mlvgNlYQ5VQhbElPAgd3ky6o.jpeg', 2017, 2020, 'https://www.facebook.com/salma.afify.923', 'https://twitter.com/salmaafifyali?s=08', '#', 'https://www.linkedin.com/in/salma-afify-6995a8174', '2020-05-05 10:35:03', '2020-05-05 10:35:03'),
(80, 'Tasbeeh Eissa', 'Member', '0p4QX7Rd0S8g28IqRIS2IRaFBpcjGmyl621PBIZt.jpeg', 2020, 2020, 'https://www.facebook.com/tasbeeh.eissa.7', 'https://twitter.com/TasbeehEissa?s=09', '#', 'https://www.linkedin.com/in/tasbeeh-eissa-b96703199', '2020-05-06 15:43:33', '2020-05-06 15:43:33'),
(82, 'Amira Mahdy', 'Member', 'tAzTTneCbuIjYq1NWOjdCrtwlwCt93CgEWwp6kO8.jpeg', 2020, 2021, 'https://www.facebook.com/mraalmahdy', 'https://mobile.twitter.com/amiraa_mahdy', '#', 'https://www.linkedin.com/in/amira-mahdy-ba24aa1a3', '2020-05-06 16:14:01', '2020-05-06 16:14:01'),
(83, 'Abdallah Hany', 'Member', '8vYvoTmtzWXcXti6DB8t4ZQLEQs4w0tjFGx7W4Pq.jpeg', 2020, 2021, 'https://m.facebook.com/abdallah.hany.rashed', '#', '#', '#', '2020-05-06 16:17:19', '2020-06-22 22:18:06'),
(84, 'Sherief Mohamed', 'Member', 'CuTp1ED9zflIzKH83iOewCiGctN7WQ9O3WWfweQ5.jpeg', 2020, 2021, 'https://www.facebook.com/sherief.mohamed.754', '#', '#', '#', '2020-05-06 16:19:20', '2020-06-22 22:20:09'),
(85, 'Mazen ElMadawy', 'Member', 'fmxcdJOC8x1nP4OzA5jepBe6HjJ9IGJRXP1df8qn.jpeg', 2020, 2021, 'https://www.facebook.com/S.EngMazen', 'https://twitter.com/MazenElMadawy14?s=09', '#', '#', '2020-05-06 16:21:48', '2020-06-22 22:20:26'),
(86, 'Maram El-shafie', 'Member', 'm6WD5bdppTpnOpPQjcqL2BLo2EU6pxl6zpleHeWs.jpeg', 2020, 2021, 'https://www.facebook.com/anabanota0100', 'https://twitter.com/maram_saber?s=09', '#', '#', '2020-05-06 16:23:31', '2020-09-12 23:51:38'),
(87, 'Hager Ibrahim', 'Member', 'BHm14s7LKZ8xwzpb96dhQWQEDXhzihXhPykG8aJ0.png', 2020, 2021, 'https://www.facebook.com/hager.ibrahim.1460', 'https://twitter.com/hagerib23065897?s=09', '#', '#', '2020-05-06 16:26:50', '2020-06-22 22:21:02'),
(88, 'Ashrakat Ahmed', 'Member', 'REtIvuiyCnx8vlOsUrbZTiIqFKttlawBnzUF6ex0.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100005105918987', '#', '#', 'https://www.linkedin.com/in/ashrakat-ahmad-8496791a8', '2020-05-06 16:28:18', '2020-06-22 22:21:16'),
(89, 'yasmeen Mostafa', 'Member', '1M0WGktxGjvaMeP7tljXCK767YsLMZZz2xYa4Xtn.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100005185236043', 'https://twitter.com/search?q=%40yasmeen53707823&s=08', '#', '#', '2020-05-06 16:29:48', '2020-06-22 22:21:31'),
(90, 'Amira Ahmed', 'Member', 'arW0kWrgdYzFkiLLtMlJI8pep5cEfu3Q6EReKVDs.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100009765162015', '#', '#', '#', '2020-05-06 16:30:50', '2020-06-22 22:21:57'),
(91, 'Ayaat Adel', 'Member', 'lAtWdsjBBFQVg45DylmaaF1qzbFD0qwXB82ccrgE.jpeg', 2020, 2021, 'https://www.facebook.com/ayaat.adel.37', 'https://twitter.com/ayaatadel5?s=08', '#', '#', '2020-05-06 16:32:29', '2020-06-22 22:22:08'),
(92, 'Karim Mahmoud', 'Member', '9xvJdna9Nz3BJ02rpXoW4HOv4C5rXfrUfhXou0PY.jpeg', 2020, 2021, 'https://www.facebook.com/kareem.aboagiza', 'https://mobile.twitter.com/AgizaKemo', '#', 'https://www.linkedin.com/in/karim-mohmoud-abo-agiza-29684b197', '2020-05-06 16:33:52', '2020-05-06 16:33:52'),
(93, 'Toka Newer', 'Member', 'VwtOl4ZAEseEXPvmxgsGRThzaDlDRQY0zUhFgswv.jpeg', 2018, 2021, 'https://www.facebook.com/toka.newer', 'https://twitter.com/toka_newer', '#', 'https://www.linkedin.com/in/toka-newer-77553a17a/', '2020-05-06 16:35:22', '2020-05-06 17:32:03'),
(94, 'Kholoud Alhamzawy', 'Member', 'IhYpASoi6kjuU1W6YBjBljBEUxvp5W2zD4xDP7TJ.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100002892888616', '#', '#', 'http://linkedin.com/in/kholoud-alhamzawy-731702198', '2020-05-06 16:36:56', '2020-06-22 22:22:48'),
(95, 'Sohaila Habib', 'Member', 'vucrvJL3CnJ2T0kSDWZ1rRw7A1sRcXKzJfLyhOXF.jpeg', 2020, 2021, 'https://www.facebook.com/sohaila.habib.129', '#', '#', '#', '2020-05-06 16:38:12', '2020-06-22 22:25:07'),
(96, 'Yasmen Maged', 'Member', 'fRBpA57oLGSIcvuPBzt9yZA7vsgB37c5X6kfdNcZ.jpeg', 2020, 2021, 'https://www.facebook.com/gasmen.maged', 'https://twitter.com/yasmen_maged?s=09', '#', 'https://www.linkedin.com/in/yasmen-maged-905704198', '2020-05-06 16:39:32', '2020-05-06 16:39:32'),
(97, 'Asmaa Wasel', 'Member', 'CAJ23MysnvngasDBBhX9r9QSrVRdqDNrXtFrabGQ.jpeg', 2020, 2021, 'https://www.facebook.com/asma.mohammed.5070276', 'https://twitter.com/Asmaa60648461?s=09', '#', 'https://www.linkedin.com/in/asmaa-wasel-9b46481a8/', '2020-05-06 16:41:01', '2020-05-06 16:41:01'),
(98, 'Mahmoud shahin', 'Member', 'YZ9chhlt52uuu5isI3gcgrLrCgnJOI4O6IPDyYBo.jpeg', 2018, 2021, 'https://www.facebook.com/profile.php?id=100008163630558', '#', '#', '#', '2020-05-06 16:42:14', '2020-06-22 22:25:48'),
(99, 'Ghaidaa Eldsoky', 'Member', 'dTaZcjAbf12IOcwvm9Ng5JdySbtxzTlVWd8zQoOa.jpeg', 2020, 2021, 'https://www.facebook.com/ghaidaa.eldsoky', 'https://twitter.com/ghaidaa_gsd', '#', '#', '2020-05-06 16:44:05', '2020-06-22 22:25:59'),
(100, 'Mohamed Eid', 'Member', 'JhyWSfca3I729kYVu9mLsKIPFbezp5taTyZpMFJ6.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100003400131597', 'https://twitter.com/mohammed_eid35?s=09', '#', 'https://www.linkedin.com/in/mohammed-eid35', '2020-05-06 17:07:09', '2020-05-06 17:07:09'),
(101, 'Ahmed Eldeeb', 'Member', '5cIJ6vxhmLMghiuEYJ9qTehuJyGM5tPGz0ZDz2Lu.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100012288707204', '#', '#', 'https://www.linkedin.com/in/ahmed-eldeeb-66227216a', '2020-05-06 17:09:01', '2020-06-22 22:26:31'),
(102, 'Mostafa Mahmoud', 'Member', 'deCWDxY9PDpdSth0XeAKjnET85CZdl0wdx32PX90.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100008333743050', 'https://twitter.com/Mustafa63012620?s=09', '#', 'https://www.linkedin.com/in/mustafa-mahmoud-9712821a0', '2020-05-06 17:13:27', '2020-05-06 17:13:27'),
(103, 'hussein zayed', 'Member', '9GOEf1QhM0uzPITenIBmHjM7D9QMGNsJpsTgVMll.jpeg', 2020, 2021, 'https://www.facebook.com/husseinzayed11', '#', '#', 'https://www.linkedin.com/in/hussein-zayed-46227a16a/', '2020-05-06 17:15:04', '2020-06-22 22:28:12'),
(104, 'Mohamed Hatem', 'Member', 'vr8INj1nzn3HJZOVRrmvMj2wYt9uqO4aw6R6xlAG.png', 2020, 2021, 'https://www.facebook.com/mohamed.hatem.56232938', NULL, '#', NULL, '2020-05-06 17:16:14', '2020-05-06 17:16:14'),
(105, 'Mahmoud Khalid', 'Member', 'Ysn8Q1JI0ZRdEHwXF6Lp75BtvXj3663RAGaO2kHb.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100010126149207', 'https://twitter.com/Mahmoud43109824?s=09', '#', 'https://www.linkedin.com/in/mahmoudkhalid112', '2020-05-06 17:18:09', '2020-05-06 17:18:09'),
(106, 'Shimaa khallaf', 'Member', '625PY32YaWA8JaoKjsil0qSeFyE7SPM3mA67IhaM.jpeg', 2020, 2021, 'https://www.facebook.com/shams.elhyah.967', 'https://twitter.com/ShimaaM61012691?s=07', '#', 'https://www.linkedin.com/in/shimaa-khallaf-6b541b167', '2020-05-06 17:19:55', '2020-05-06 17:19:55'),
(107, 'Ahmed Abdelsalam', 'Member', 'M2w3ycmhjuNpmL8vQhjlpWAhaFcI7dW0vKjMmCjs.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100018107943462', '#', '#', 'https://www.linkedin.com/in/ahmed-abdelsalam-9152061a7/', '2020-05-06 17:21:08', '2020-06-22 22:29:11'),
(108, 'Mohamed Hamza', 'Member', 'tlDNeVmmGrnAxKHuydZbVPkOaqPE0ZmuyM9iMBjb.jpeg', 2020, 2021, 'https://www.facebook.com/mohammed.hamzawy10', 'https://twitter.com/Muhamme83533065?s=09', '#', 'https://www.linkedin.com/mwlite/in/muhammed-hamza-05318b1a5', '2020-05-06 17:22:38', '2020-05-06 17:22:38'),
(109, 'Mahmoud Safan', 'Member', 'KSxS1zc5R4VZioeXFClm3xlpjoiCU4et6fVJMX3M.jpeg', 2018, 2021, 'https://www.facebook.com/mahmoud.safan.771', 'https://twitter.com/MahmoudSafan55', '#', 'https://www.linkedin.com/in/mahmoud-safan-2ba362192', '2020-05-06 17:24:26', '2020-05-06 17:34:13'),
(110, 'Samira Elshon', 'Member', 'Flk5VVl4Qrvcq5JQNWDoV3Iortxbdztaq90QitdH.jpeg', 2018, 2021, 'https://www.facebook.com/profile.php?id=100010892530888', 'https://twitter.com/ElshonSamira?s=09', '#', 'https://www.linkedin.com/in/samira-elshon-40b4481a9', '2020-05-06 17:26:15', '2020-05-06 17:34:28'),
(111, 'Ahmed salama', 'Member', 'g6lFTGcxxUwAAcPW3qm4nSdLZH5478qpSvHUEGDl.jpeg', 2020, 2021, 'https://www.facebook.com/abo.salama.50', 'https://twitter.com/AhmedSa12102666?s=09', '#', '#', '2020-05-06 17:27:44', '2020-06-22 22:30:08'),
(112, 'Soaad khaled', 'Member', 'yautVQZPytv2eECTCZmsC4z8FQ6yCGRdOI2DeFm6.jpeg', 2020, 2021, 'https://www.facebook.com/soaad.khaled.35', 'https://twitter.com/soaadkh2001?s=09', '#', '#', '2020-05-06 17:29:06', '2020-06-22 22:30:17'),
(113, 'Fadi Refaay', 'Member', 'UIjZVOpFYmufOuxAfhuhlitYx0h0BEoNglfKnWcY.jpeg', 2017, 2021, 'https://www.facebook.com/fadirefaay010', 'https://twitter.com/fadi01010?s=09', '#', '#', '2020-05-06 17:31:32', '2020-06-22 22:30:32'),
(114, 'Hajar Mohamed', 'Member', '4zqYP6hC73X339g4cStUxyrBIrY4HEH95YGIMAcK.jpeg', 2020, 2021, 'https://www.facebook.com/hager.hager.948011', 'https://twitter.com/HajarMuhmd?s=08', '#', '#', '2020-05-06 17:36:33', '2020-06-22 22:30:43'),
(115, 'Manar Elshenawy', 'Member', 'an0425gE8EuR3Vjrz9RsLugryKLlSYv5no034Z8j.jpeg', 2020, 2021, 'https://www.facebook.com/manar.fathy.9883', 'https://twitter.com/MaNarElshenawy3?s=08', '#', '#', '2020-05-06 17:37:49', '2020-06-22 22:31:13'),
(116, 'Mohamed Shebl', 'Member', '0PgQQZYzH3m6UgEstmIT8U8xFnqyWHCbarySqUIf.jpeg', 2018, 2021, 'https://www.facebook.com/md5sh', 'https://twitter.com/MD_S5?s=08', '#', 'https://www.linkedin.com/in/mohammed-shebl-7483aa196/', '2020-05-06 17:39:24', '2020-05-06 17:39:24'),
(117, 'Mayar Hefnawy', 'Member', 'fawjaai0oYYHl94jbL7B66SyzGIXE8cTrSzYLqSQ.jpeg', 2017, 2021, 'https://www.facebook.com/mayer.mohamed.9809', 'https://twitter.com/HefnawyMayar', '#', '#', '2020-05-06 17:41:07', '2020-06-22 22:32:19'),
(118, 'Abdulrahman Sami', 'Member', 'pFfJBRlIGBC7I4BcwvgFqwNL1D0V3hHl28KLfU4W.jpeg', 2018, 2021, 'https://www.facebook.com/allu.arjan', 'https://twitter.com/Abdulra78971826?fbclid=IwAR0jGVKaStMkjd3oumGNQoWYu5nQzMq_psOCJgTLIzz6YPvSMlMuiR87CmI', '#', '#', '2020-05-06 17:42:36', '2020-06-22 22:32:34'),
(119, 'Ahmed Mega', 'Member', 'lzSKOx8n8ZyQ7iETWeUwZbI6eDVK2sMgOPlN5DMl.jpeg', 2016, 2021, 'https://www.facebook.com/ahmed.mega.soft.eng', '#', '#', '#', '2020-05-06 17:45:53', '2020-06-22 22:34:57'),
(120, 'Farah Mohap', 'Member', '1O5tP7UdUM9Xfo88I7LMi7QvZ2NQ67mRrbWqidZr.jpeg', 2020, 2021, 'https://www.facebook.com/farhanen.faroha', 'https://twitter.com/FarahMohap?s=09', '#', 'https://www.linkedin.com/in/farah-mohap-4922bb1b1', '2020-05-06 17:47:34', '2020-06-28 12:45:54'),
(121, 'Mahmoud Elsaqqa', 'Member', 'cxBcxR2g6mcBMFwIw6vyl81oZHeE2CpplMuv1CMT.jpeg', 2018, 2021, 'https://www.facebook.com/mahmoud.elsaka.796', '#', '#', '#', '2020-05-06 22:59:46', '2020-06-22 22:35:32'),
(122, 'Heba Elsaid', 'leader', 'y6UFc4WzBfdHIi0nfmngHTOgUt5obALTlivPF52b.png', 2017, 2021, 'https://www.facebook.com/profile.php?id=100005884905567', 'https://twitter.com/Heba35576808?s=09', '#', 'https://www.linkedin.com/in/heba-elsaid-b7620a190', '2020-05-06 23:03:29', '2020-06-21 23:13:25'),
(123, 'Mohamed Ali', 'leader', '7oLDdtFpTnjYLPBaIULOC9itHWa5i3fsw9nGnEOm.jpeg', 2017, 2021, 'https://www.facebook.com/hamo.daana.77377', 'https://twitter.com/Mohamed73914137?s=08', '#', '#', '2020-05-06 23:09:22', '2020-06-22 22:32:06'),
(124, 'Mohamed Tarek', 'leader', 'Krrdac4aF2sUlVoZimbruGAQRe4HRD0XjbdtwsTt.jpeg', 2018, 2021, 'https://www.facebook.com/profile.php?id=100006295150852', 'https://twitter.com/mohamedtarek173?s=08', '#', '#', '2020-05-06 23:13:59', '2020-06-22 22:31:27'),
(125, 'Ghada Ragab', 'leader', 'ZHeCTf3r1ZTBA6ZqjscANZc9JsDsGloKRbnwPElJ.jpeg', 2018, 2021, 'https://www.facebook.com/ghada.ragab.5203', 'https://twitter.com/GhadaRa82459228?s=09', '#', 'https://www.linkedin.com/in/ghada-ragab-5a0659189', '2020-05-06 23:18:26', '2020-05-06 23:18:26'),
(126, 'Ahmed Sabra', 'Member', '8XFzG0KfKEbjqlZnajz3gNkwbyKoIAaDvE2SZnHx.jpeg', 2020, 2021, 'https://www.facebook.com/ahmad.sabra.336', 'https://twitter.com/AhmedSabra01?s=09', '#', '#', '2020-05-06 23:37:56', '2020-06-22 22:26:14'),
(127, 'Eman Ali', 'Member', '45SqdCLkJT6KzzhLGSWHB1g181CllpLCAGWA8qwb.jpeg', 2020, 2021, 'https://www.facebook.com/profile.php?id=100028168783324', '#', '#', '#', '2020-06-22 22:40:29', '2020-06-22 22:40:29'),
(128, 'Hatem_Mohamed', 'Application Manager', 'SVyrPOQK38ARRH18bIzt22FlU5cud9rGcsxIzBbQ.jpeg', 2019, 2021, 'https://www.facebook.com/hatem.elsheref.73', '#', '#', 'https://www.linkedin.com/in/hatem-mohamed-31b8901a2/', '2020-09-02 16:20:00', '2020-09-02 16:20:38');

-- --------------------------------------------------------

--
-- Table structure for table `member_team`
--

CREATE TABLE `member_team` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member_team`
--

INSERT INTO `member_team` (`id`, `team_id`, `member_id`, `created_at`, `updated_at`) VALUES
(44, 9, 26, NULL, NULL),
(49, 7, 29, NULL, NULL),
(51, 11, 30, NULL, NULL),
(52, 8, 31, NULL, NULL),
(55, 9, 32, NULL, NULL),
(57, 7, 33, NULL, NULL),
(60, 11, 34, NULL, NULL),
(62, 10, 35, NULL, NULL),
(64, 11, 36, NULL, NULL),
(66, 10, 37, NULL, NULL),
(67, 10, 38, NULL, NULL),
(68, 9, 39, NULL, NULL),
(71, 12, 40, NULL, NULL),
(72, 11, 41, NULL, NULL),
(73, 9, 42, NULL, NULL),
(74, 9, 43, NULL, NULL),
(76, 9, 44, NULL, NULL),
(80, 12, 45, NULL, NULL),
(81, 10, 46, NULL, NULL),
(82, 7, 47, NULL, NULL),
(83, 11, 48, NULL, NULL),
(84, 9, 49, NULL, NULL),
(86, 10, 50, NULL, NULL),
(87, 9, 51, NULL, NULL),
(89, 12, 52, NULL, NULL),
(90, 10, 53, NULL, NULL),
(91, 9, 54, NULL, NULL),
(93, 9, 55, NULL, NULL),
(94, 9, 56, NULL, NULL),
(95, 10, 57, NULL, NULL),
(96, 7, 58, NULL, NULL),
(98, 10, 59, NULL, NULL),
(99, 9, 60, NULL, NULL),
(100, 12, 61, NULL, NULL),
(101, 9, 62, NULL, NULL),
(103, 9, 64, NULL, NULL),
(104, 10, 65, NULL, NULL),
(106, 9, 67, NULL, NULL),
(107, 11, 68, NULL, NULL),
(108, 10, 69, NULL, NULL),
(109, 9, 70, NULL, NULL),
(110, 9, 71, NULL, NULL),
(111, 7, 72, NULL, NULL),
(113, 8, 73, NULL, NULL),
(115, 10, 14, NULL, NULL),
(116, 9, 74, NULL, NULL),
(117, 9, 75, NULL, NULL),
(118, 8, 76, NULL, NULL),
(119, 11, 77, NULL, NULL),
(121, 10, 78, NULL, NULL),
(122, 10, 79, NULL, NULL),
(123, 8, 80, NULL, NULL),
(126, 8, 82, NULL, NULL),
(127, 10, 83, NULL, NULL),
(128, 10, 84, NULL, NULL),
(129, 12, 85, NULL, NULL),
(130, 12, 87, NULL, NULL),
(131, 11, 88, NULL, NULL),
(132, 12, 89, NULL, NULL),
(133, 11, 90, NULL, NULL),
(134, 11, 91, NULL, NULL),
(135, 10, 92, NULL, NULL),
(136, 10, 93, NULL, NULL),
(137, 8, 94, NULL, NULL),
(138, 12, 95, NULL, NULL),
(139, 8, 96, NULL, NULL),
(140, 10, 97, NULL, NULL),
(141, 11, 98, NULL, NULL),
(142, 11, 99, NULL, NULL),
(143, 9, 100, NULL, NULL),
(144, 9, 101, NULL, NULL),
(145, 9, 102, NULL, NULL),
(146, 9, 103, NULL, NULL),
(147, 7, 104, NULL, NULL),
(148, 9, 105, NULL, NULL),
(149, 9, 106, NULL, NULL),
(150, 10, 107, NULL, NULL),
(151, 10, 108, NULL, NULL),
(152, 9, 109, NULL, NULL),
(154, 10, 110, NULL, NULL),
(155, 10, 111, NULL, NULL),
(157, 10, 112, NULL, NULL),
(158, 10, 113, NULL, NULL),
(159, 7, 86, NULL, NULL),
(160, 10, 114, NULL, NULL),
(161, 7, 115, NULL, NULL),
(162, 11, 116, NULL, NULL),
(163, 10, 117, NULL, NULL),
(165, 11, 118, NULL, NULL),
(166, 11, 119, NULL, NULL),
(167, 7, 120, NULL, NULL),
(168, 8, 121, NULL, NULL),
(169, 10, 122, NULL, NULL),
(172, 9, 123, NULL, NULL),
(173, 12, 124, NULL, NULL),
(174, 8, 125, NULL, NULL),
(175, 12, 126, NULL, NULL),
(176, 10, 127, NULL, NULL),
(177, 9, 128, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_04_09_133409_create_posts_table', 1),
(5, '2020_04_09_170916_create_categories_table', 1),
(6, '2020_04_09_224902_create_tags_table', 1),
(7, '2020_04_10_004018_post_tag', 1),
(8, '2020_04_11_004647_create_activities_table', 1),
(9, '2020_04_11_031454_create_activity_posts_table', 1),
(10, '2020_04_11_064130_create_testimonials_table', 1),
(11, '2020_04_12_191856_create_filemanagers_table', 1),
(12, '2020_04_14_082253_create_members_table', 1),
(13, '2020_04_14_084030_create_teams_table', 1),
(14, '2020_04_14_095438_create_member_team_table', 1),
(15, '2020_04_14_214140_create_boards_table', 1),
(16, '2020_04_14_233950_create_board_team_table', 1),
(17, '2020_04_14_234004_create_board_member_table', 1),
(18, '2020_04_15_144234_laratrust_setup_tables', 1),
(19, '2020_04_20_105725_create_settings_table', 1),
(20, '2020_09_12_201917_create_articles_table', 2),
(21, '2020_09_12_211747_create_article_tag_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('hatem@app.com', '$2y$10$xBOW4a77ca4waPAV0X3yvuzBpcsZ877DGzPL/i1eslqqr3PTq.NVO', '2020-09-12 17:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create_users', 'Create Users', 'Create Users', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(2, 'read_users', 'Read Users', 'Read Users', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(3, 'update_users', 'Update Users', 'Update Users', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(4, 'delete_users', 'Delete Users', 'Delete Users', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(5, 'create_posts', 'Create Posts', 'Create Posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(6, 'read_posts', 'Read Posts', 'Read Posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(7, 'update_posts', 'Update Posts', 'Update Posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(8, 'delete_posts', 'Delete Posts', 'Delete Posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(9, 'create_articles', 'Create Articles', 'Create Articles', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(10, 'read_articles', 'Read Articles', 'Read Articles', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(11, 'update_articles', 'Update Articles', 'Update Articles', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(12, 'delete_articles', 'Delete Articles', 'Delete Articles', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(13, 'create_categories', 'Create Categories', 'Create Categories', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(14, 'read_categories', 'Read Categories', 'Read Categories', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(15, 'update_categories', 'Update Categories', 'Update Categories', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(16, 'delete_categories', 'Delete Categories', 'Delete Categories', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(17, 'create_tags', 'Create Tags', 'Create Tags', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(18, 'read_tags', 'Read Tags', 'Read Tags', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(19, 'update_tags', 'Update Tags', 'Update Tags', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(20, 'delete_tags', 'Delete Tags', 'Delete Tags', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(21, 'create_activities', 'Create Activities', 'Create Activities', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(22, 'read_activities', 'Read Activities', 'Read Activities', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(23, 'update_activities', 'Update Activities', 'Update Activities', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(24, 'delete_activities', 'Delete Activities', 'Delete Activities', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(25, 'create_activity-posts', 'Create Activity-posts', 'Create Activity-posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(26, 'read_activity-posts', 'Read Activity-posts', 'Read Activity-posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(27, 'update_activity-posts', 'Update Activity-posts', 'Update Activity-posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(28, 'delete_activity-posts', 'Delete Activity-posts', 'Delete Activity-posts', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(29, 'create_teams', 'Create Teams', 'Create Teams', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(30, 'read_teams', 'Read Teams', 'Read Teams', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(31, 'update_teams', 'Update Teams', 'Update Teams', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(32, 'delete_teams', 'Delete Teams', 'Delete Teams', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(33, 'create_memberes', 'Create Memberes', 'Create Memberes', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(34, 'read_memberes', 'Read Memberes', 'Read Memberes', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(35, 'update_memberes', 'Update Memberes', 'Update Memberes', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(36, 'delete_memberes', 'Delete Memberes', 'Delete Memberes', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(37, 'create_board', 'Create Board', 'Create Board', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(38, 'read_board', 'Read Board', 'Read Board', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(39, 'update_board', 'Update Board', 'Update Board', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(40, 'delete_board', 'Delete Board', 'Delete Board', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(41, 'create_uploader', 'Create Uploader', 'Create Uploader', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(42, 'read_uploader', 'Read Uploader', 'Read Uploader', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(43, 'update_uploader', 'Update Uploader', 'Update Uploader', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(44, 'delete_uploader', 'Delete Uploader', 'Delete Uploader', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(45, 'create_testimonial', 'Create Testimonial', 'Create Testimonial', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(46, 'read_testimonial', 'Read Testimonial', 'Read Testimonial', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(47, 'update_testimonial', 'Update Testimonial', 'Update Testimonial', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(48, 'delete_testimonial', 'Delete Testimonial', 'Delete Testimonial', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(49, 'read_setting', 'Read Setting', 'Read Setting', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(50, 'update_setting', 'Update Setting', 'Update Setting', '2020-09-12 19:32:21', '2020-09-12 19:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`permission_id`, `user_id`, `user_type`) VALUES
(1, 2, 'App\\User'),
(2, 2, 'App\\User'),
(3, 2, 'App\\User'),
(4, 2, 'App\\User'),
(5, 2, 'App\\User'),
(6, 2, 'App\\User'),
(7, 2, 'App\\User'),
(8, 2, 'App\\User'),
(9, 2, 'App\\User'),
(10, 2, 'App\\User'),
(11, 2, 'App\\User'),
(12, 2, 'App\\User'),
(13, 2, 'App\\User'),
(14, 2, 'App\\User'),
(15, 2, 'App\\User'),
(16, 2, 'App\\User'),
(17, 2, 'App\\User'),
(18, 2, 'App\\User'),
(19, 2, 'App\\User'),
(20, 2, 'App\\User'),
(21, 2, 'App\\User'),
(22, 2, 'App\\User'),
(23, 2, 'App\\User'),
(24, 2, 'App\\User'),
(25, 2, 'App\\User'),
(26, 2, 'App\\User'),
(27, 2, 'App\\User'),
(28, 2, 'App\\User'),
(29, 2, 'App\\User'),
(30, 2, 'App\\User'),
(31, 2, 'App\\User'),
(32, 2, 'App\\User'),
(33, 2, 'App\\User'),
(34, 2, 'App\\User'),
(35, 2, 'App\\User'),
(36, 2, 'App\\User'),
(37, 2, 'App\\User'),
(38, 2, 'App\\User'),
(39, 2, 'App\\User'),
(40, 2, 'App\\User'),
(41, 2, 'App\\User'),
(42, 2, 'App\\User'),
(43, 2, 'App\\User'),
(44, 2, 'App\\User'),
(45, 2, 'App\\User'),
(46, 2, 'App\\User'),
(47, 2, 'App\\User'),
(48, 2, 'App\\User'),
(49, 2, 'App\\User'),
(50, 2, 'App\\User'),
(5, 3, 'App\\User'),
(6, 3, 'App\\User'),
(7, 3, 'App\\User'),
(8, 3, 'App\\User'),
(9, 3, 'App\\User'),
(10, 3, 'App\\User'),
(11, 3, 'App\\User'),
(12, 3, 'App\\User'),
(13, 3, 'App\\User'),
(14, 3, 'App\\User'),
(15, 3, 'App\\User'),
(16, 3, 'App\\User'),
(17, 3, 'App\\User'),
(18, 3, 'App\\User'),
(19, 3, 'App\\User'),
(20, 3, 'App\\User'),
(21, 3, 'App\\User'),
(22, 3, 'App\\User'),
(23, 3, 'App\\User'),
(24, 3, 'App\\User'),
(25, 3, 'App\\User'),
(26, 3, 'App\\User'),
(27, 3, 'App\\User'),
(28, 3, 'App\\User'),
(29, 3, 'App\\User'),
(30, 3, 'App\\User'),
(31, 3, 'App\\User'),
(32, 3, 'App\\User'),
(33, 3, 'App\\User'),
(34, 3, 'App\\User'),
(35, 3, 'App\\User'),
(36, 3, 'App\\User'),
(37, 3, 'App\\User'),
(38, 3, 'App\\User'),
(39, 3, 'App\\User'),
(40, 3, 'App\\User'),
(41, 3, 'App\\User'),
(42, 3, 'App\\User'),
(43, 3, 'App\\User'),
(44, 3, 'App\\User'),
(45, 3, 'App\\User'),
(46, 3, 'App\\User'),
(47, 3, 'App\\User'),
(48, 3, 'App\\User'),
(49, 3, 'App\\User'),
(50, 3, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dir` enum('ltr','rtl') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ltr',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `content`, `category_id`, `user_id`, `image`, `status`, `dir`, `created_at`, `updated_at`) VALUES
(6, 'What Ai ?', 'وايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر', '<p dir=\"rtl\">خبار السوشيال ميديا معاكوا أي ..؟ 😄<br />\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔<br />\r\n💥حاسبات و معلومات الكلية دي سمعت عنها؟😄<br />\r\nهى اللي بتطلع كل الناس دي المهتمين بال &quot;Computer Science&quot;&nbsp;<br />\r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر<br />\r\nبرضه مش فاهم 🙄<br />\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)<br />\r\nاحنا اللي بنعمل الابليكاشنز و ال &quot;websites&quot; زي موقع نتيجة الثانوية العامة كدا😃<br />\r\nاللي عامل الموقع دا &quot;software engineer&quot; ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات<br />\r\nطب بندرس ايه في الكلية..؟ 🤔<br />\r\nبندرس لغات برمجة زي &quot;c++&quot; و &quot;java&quot; و حاجات تانية زي &quot;Data structure&quot; و &quot;Algorithms&quot;<br />\r\nوكل دي حاجات مجرد Tools بتساعدك عشان تبدأ تدخل أي مجال أنت حابه 💖<br />\r\n&nbsp;ممكن متبقاش فاهم المصطلحات دي دلوقتي 😅<br />\r\nلكن من الاخر احنا بنفضل في الكلية ناخد حاجات تساعدنا في ال &quot;Problem solving&quot; و في الشغل بعد الكلية كمان<br />\r\nو بناخد &quot;basics&quot; لكل &quot;Technology&quot; واحنا بنقولك من دلوقتي ان احنا بناخد &quot;basics&quot; عشان تبقى عارف ان الكلية معتمدة على ال &quot;Self Study&quot; خصوصًا ان مجالنا دا &quot;dynamic&quot; يعني بتفضل طول عمرك تتعلم و ما أكثرها ال &quot;materials&quot; الموجودة على النت و المواقع اللي بتقدم كورسات كويسة جدََا &quot;online&quot;<br />\r\nكمان لازم تعرف إن مواد الكلية &nbsp;مهمة جدََا، أنت بتدرس في الكلية مواد كتير وكل مادة ليها هدف بتساعدك بشكل كبير جدََا لما تيجي تبدأ في مجال معين زي مثلََا أنت بتاخد Math 1 و احصا1 واحصا2 &nbsp;وهكذا المواد دي بتساعك في مجالات كتير زي ال &quot;Machine Learning&quot; ودا مجال كبير جدََا<br />\r\nبيستخدم في التقنيات الحديثة *زي عربية تسلا أو صواريخ الفضاء * كل دول و أكتر حاجات تقدر تكودها وتكتبها 👨🏻&zwj;💻<br />\r\nبتسأل نفسك هو مجال الكلية دا ليه شغل ولا لأ!! 🤔<br />\r\nأنت لو دخلت على جوجل وعملت سيرش عن الوظائف المطلوب دايمََاا هتلاقي المبرمج مكتسح، ودا مش علشان هما اللي مصممين الموقع ولا حاجة لأ 😂 دا لأن فعلََا هي من أكتر الوظائف المطلوبة حاليََا 🔥<br />\r\nطب ايه شروط الالتحاق بالكلية!! 🤔<br />\r\n💥الكلية بتدخلها من شعبة علمي رياضة و السنة اللي فاتت اخدت من حد أدنى 90.7% بيختلف حسب تنسيق كل سنة<br />\r\n&nbsp;و فيه اقسام خاصة لعلمي علوم المتاح في جامعة المنوفية هو &quot;bioinformatics&quot; هنبقا نتكلم عنه في بوستات جاية😌</p>\r\n\r\n<p dir=\"rtl\">💥الكلية مش بتحتاج امتحان قدرات أو حاجة و هى بتنزل في التنسيق عادي بس مش بتنزل لطلاب علوم طالب علمي علوم لازم يروح يحول اوراقه للكلية<br />\r\n💥مش ضروري خالص ابدََا ابدََا يكون عندك خلفية عن البرمجة أو الكمبيوتر بشكل عام أنت بتدخل الكلية بتعرف كل حاجة❤️</p>\r\n\r\n<p dir=\"rtl\">📎الكلية الدراسة فيها اربع سنين و فيها أقسام تخصص هنبقا نتكلم عنها في بوستات جاية🙏 و التخصص بيكون في سنة تالتة أو رابعة على حسب كل جامعة و في جامعة المنوفية من سنة تالتة</p>\r\n\r\n<p dir=\"rtl\">📎الدراسة بتكون بالانجليزي و دا مش هيكون عائق خالص بالمناسبة و هتتعود بعد فترة قصيرة جدََا 😅</p>\r\n\r\n<p dir=\"rtl\">#FCI<br />\r\n#Software_Engineer<br />\r\n#MUFIX_Communit</p>', 2, 1, 'xciBW2XPhHTmH76hv517XJDr5hUybsfiDpTGVYO2.jpeg', 1, 'ltr', '2020-09-12 22:47:04', '2020-09-12 22:47:12'),
(7, 'game by Amira amira', 'حاسبات و معلومات الكلية دي سمعت عنها', '<p>أخبار السوشيال ميديا معاكوا أي ..؟ 😄<br />\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔<br />\r\n💥حاسبات و معلومات الكلية دي سمعت عنها؟😄<br />\r\nهى اللي بتطلع كل الناس دي المهتمين بال &quot;Computer Science&quot;&nbsp;<br />\r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر<br />\r\nبرضه مش فاهم 🙄<br />\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)<br />\r\nاحنا اللي بنعمل الابليكاشنز و ال &quot;websites&quot; زي موقع نتيجة الثانوية العامة كدا😃<br />\r\nاللي عامل الموقع دا &quot;software engineer&quot; ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات<br />\r\nطب بندرس ايه في الكلية..؟ 🤔<br />\r\nبندرس لغات برمجة زي &quot;c++&quot; و &quot;java&quot; و حاجات تانية زي &quot;Data structure&quot; و &quot;Algorithms&quot;<br />\r\nوكل دي حاجات مجرد Tools بت</p>\r\n\r\n<p>أخبار السوشيال ميديا معاكوا أي ..؟ 😄<br />\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔<br />\r\n💥حاسبات و معلومات الكلية دي سمعت عنها؟😄<br />\r\nهى اللي بتطلع كل الناس دي المهتمين بال &quot;Computer Science&quot;&nbsp;<br />\r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر<br />\r\nبرضه مش فاهم 🙄<br />\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)<br />\r\nاحنا اللي بنعمل الابليكاشنز و ال &quot;websites&quot; زي موقع نتيجة الثانوية العامة كدا😃<br />\r\nاللي عامل الموقع دا &quot;software engineer&quot; ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات<br />\r\nطب بندرس ايه في الكلية..؟ 🤔<br />\r\nبندرس لغات برمجة زي &quot;c++&quot; و &quot;java&quot; و حاجات تانية زي &quot;Data structure&quot; و &quot;Algorithms&quot;<br />\r\nوكل دي حاجات مجرد Tools بت</p>\r\n\r\n<p>أخبار السوشيال ميديا معاكوا أي ..؟ 😄<br />\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔<br />\r\n💥حاسبات و معلومات الكلية دي سمعت عنها؟😄<br />\r\nهى اللي بتطلع كل الناس دي المهتمين بال &quot;Computer Science&quot;&nbsp;<br />\r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر<br />\r\nبرضه مش فاهم 🙄<br />\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)<br />\r\nاحنا اللي بنعمل الابليكاشنز و ال &quot;websites&quot; زي موقع نتيجة الثانوية العامة كدا😃<br />\r\nاللي عامل الموقع دا &quot;software engineer&quot; ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات<br />\r\nطب بندرس ايه في الكلية..؟ 🤔<br />\r\nبندرس لغات برمجة زي &quot;c++&quot; و &quot;java&quot; و حاجات تانية زي &quot;Data structure&quot; و &quot;Algorithms&quot;<br />\r\nوكل دي حاجات مجرد Tools بت</p>\r\n\r\n<p>أخبار السوشيال ميديا معاكوا أي ..؟ 😄<br />\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔<br />\r\n💥حاسبات و معلومات الكلية دي سمعت عنها؟😄<br />\r\nهى اللي بتطلع كل الناس دي المهتمين بال &quot;Computer Science&quot;&nbsp;<br />\r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر<br />\r\nبرضه مش فاهم 🙄<br />\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)<br />\r\nاحنا اللي بنعمل الابليكاشنز و ال &quot;websites&quot; زي موقع نتيجة الثانوية العامة كدا😃<br />\r\nاللي عامل الموقع دا &quot;software engineer&quot; ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات<br />\r\nطب بندرس ايه في الكلية..؟ 🤔<br />\r\nبندرس لغات برمجة زي &quot;c++&quot; و &quot;java&quot; و حاجات تانية زي &quot;Data structure&quot; و &quot;Algorithms&quot;<br />\r\nوكل دي حاجات مجرد Tools بت</p>', 11, 3, 'hT0MpBrbRv8U7wyYhm9jhP7LZra0n2bHuDLA7tlX.jpeg', 1, 'ltr', '2020-09-12 23:08:05', '2020-09-12 23:08:20'),
(8, 'Java SE  by amira', 'تعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖', '<p>ولسه مكملين معاكم رحلتنا اللي بدأناها عن ال Java 🔥&nbsp;<br />\r\nوبعد ما اتكلمنا في البوست اللي فات عن بداية ال Java كانت فين وازاي؟ وتقدر تقرأ البوست دا * https://bit.ly/3k5W8Mk &nbsp;* عشان تسترجع معلوماتك ولو مقرأتهوش هتستفيد جدََا برضو لما تقرأه 💖<br />\r\nودلوقتي هنكمل كلامنا وهنتكلم عن أنواع ال Java وازاي اذاكرها كويس وأبدأ فيها صح ومنين! 🤔<br />\r\nال&quot;Java&quot; ليها أكتر من نوع &nbsp;زي :<br />\r\n▪️Java SE&nbsp;<br />\r\n▪️ Java ME&nbsp;<br />\r\n▪️ Java EE&nbsp;<br />\r\nطب حلو يعني اي بقي SE و ME و EE &nbsp;؟!<br />\r\nتعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖<br />\r\n🖇️ال Java SE هي اختصار Java Standard Edition وهي &nbsp;تعتبر Core &nbsp;ال Java &nbsp;وعلشان تبدأ صح في اي مجال متعلق بال Java فأنت محتاج أنك تبدأ في ال Java SE وتبدأ تذاكرها كويس وتطبق عليها كتير 👌🏻<br />\r\nوبعد ما تتعلمها تقدر أنك تتعلم Java EE أو Java for Android وبعدها تقدر تدخل ف</p>\r\n\r\n<p>ولسه مكملين معاكم رحلتنا اللي بدأناها عن ال Java 🔥&nbsp;<br />\r\nوبعد ما اتكلمنا في البوست اللي فات عن بداية ال Java كانت فين وازاي؟ وتقدر تقرأ البوست دا * https://bit.ly/3k5W8Mk &nbsp;* عشان تسترجع معلوماتك ولو مقرأتهوش هتستفيد جدََا برضو لما تقرأه 💖<br />\r\nودلوقتي هنكمل كلامنا وهنتكلم عن أنواع ال Java وازاي اذاكرها كويس وأبدأ فيها صح ومنين! 🤔<br />\r\nال&quot;Java&quot; ليها أكتر من نوع &nbsp;زي :<br />\r\n▪️Java SE&nbsp;<br />\r\n▪️ Java ME&nbsp;<br />\r\n▪️ Java EE&nbsp;<br />\r\nطب حلو يعني اي بقي SE و ME و EE &nbsp;؟!<br />\r\nتعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖<br />\r\n🖇️ال Java SE هي اختصار Java Standard Edition وهي &nbsp;تعتبر Core &nbsp;ال Java &nbsp;وعلشان تبدأ صح في اي مجال متعلق بال Java فأنت محتاج أنك تبدأ في ال Java SE وتبدأ تذاكرها كويس وتطبق عليها كتير 👌🏻<br />\r\nوبعد ما تتعلمها تقدر أنك تتعلم Java EE أو Java for Android وبعدها تقدر تدخل ف</p>\r\n\r\n<p>ولسه مكملين معاكم رحلتنا اللي بدأناها عن ال Java 🔥&nbsp;<br />\r\nوبعد ما اتكلمنا في البوست اللي فات عن بداية ال Java كانت فين وازاي؟ وتقدر تقرأ البوست دا * https://bit.ly/3k5W8Mk &nbsp;* عشان تسترجع معلوماتك ولو مقرأتهوش هتستفيد جدََا برضو لما تقرأه 💖<br />\r\nودلوقتي هنكمل كلامنا وهنتكلم عن أنواع ال Java وازاي اذاكرها كويس وأبدأ فيها صح ومنين! 🤔<br />\r\nال&quot;Java&quot; ليها أكتر من نوع &nbsp;زي :<br />\r\n▪️Java SE&nbsp;<br />\r\n▪️ Java ME&nbsp;<br />\r\n▪️ Java EE&nbsp;<br />\r\nطب حلو يعني اي بقي SE و ME و EE &nbsp;؟!<br />\r\nتعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖<br />\r\n🖇️ال Java SE هي اختصار Java Standard Edition وهي &nbsp;تعتبر Core &nbsp;ال Java &nbsp;وعلشان تبدأ صح في اي مجال متعلق بال Java فأنت محتاج أنك تبدأ في ال Java SE وتبدأ تذاكرها كويس وتطبق عليها كتير 👌🏻<br />\r\nوبعد ما تتعلمها تقدر أنك تتعلم Java EE أو Java for Android وبعدها تقدر تدخل ف</p>\r\n\r\n<p>ولسه مكملين معاكم رحلتنا اللي بدأناها عن ال Java 🔥&nbsp;<br />\r\nوبعد ما اتكلمنا في البوست اللي فات عن بداية ال Java كانت فين وازاي؟ وتقدر تقرأ البوست دا * https://bit.ly/3k5W8Mk &nbsp;* عشان تسترجع معلوماتك ولو مقرأتهوش هتستفيد جدََا برضو لما تقرأه 💖<br />\r\nودلوقتي هنكمل كلامنا وهنتكلم عن أنواع ال Java وازاي اذاكرها كويس وأبدأ فيها صح ومنين! 🤔<br />\r\nال&quot;Java&quot; ليها أكتر من نوع &nbsp;زي :<br />\r\n▪️Java SE&nbsp;<br />\r\n▪️ Java ME&nbsp;<br />\r\n▪️ Java EE&nbsp;<br />\r\nطب حلو يعني اي بقي SE و ME و EE &nbsp;؟!<br />\r\nتعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖<br />\r\n🖇️ال Java SE هي اختصار Java Standard Edition وهي &nbsp;تعتبر Core &nbsp;ال Java &nbsp;وعلشان تبدأ صح في اي مجال متعلق بال Java فأنت محتاج أنك تبدأ في ال Java SE وتبدأ تذاكرها كويس وتطبق عليها كتير 👌🏻<br />\r\nوبعد ما تتعلمها تقدر أنك تتعلم Java EE أو Java for Android وبعدها تقدر تدخل ف</p>', 3, 3, 'sBXzOc9vSfNSsxFoGmPimrssJjKRKpIvYe2SIinY.jpeg', 1, 'ltr', '2020-09-12 23:13:34', '2020-09-12 23:13:34'),
(9, 'Post by hatem', 'This post ger error by miram', '<p>يجماعه بقالي اسبوعين فايرور والله مابينحل المشكله ان مافيش ايرور بيظهر مباشر كل مااعمل submit للفورم من نوع post لازم post عشان حجم الداتا بيحصل redirect ومش بيدخل فالكونترولر اصلا بيرجع ع صفحه index مش عارف اصلا اشمعنا هيا اللي بيرجعلها بيحصل redirect مع ايرور 403 السيستم شغال لوكال كويس بابلك بيحصل كده</p>', 3, 1, 'uqPXc8PbpVhbszFigGoxpOcUjO34nfMmigQPWofx.png', 1, 'ltr', '2020-09-12 23:19:44', '2020-09-12 23:19:44'),
(10, 'social media maram', 'أخبار السوشيال ميديا معاكوا أي ..؟ 😄\r\nبخصوص السوشيال ميديا مسألتش نفسك هو مين اللي بيعمل الفيسبوك ومين المسؤولين عن تصميم الابليكاشنز دي كلها عموما ! 🤔', '<p>&nbsp;💥حاسبات و معلومات الكلية دي سمعت عنها؟😄<br />\r\nهى اللي بتطلع كل الناس دي المهتمين بال &quot;Computer Science&quot;&nbsp;<br />\r\nوايوا اه هى حاسبات مش حسابات يعني احنا مش محاسبين ولا دي قسم في تجارة زي ما البعض فاكر<br />\r\nبرضه مش فاهم 🙄<br />\r\nالأول خليني اقولك ان احنا مش بتوع سايبر ولا بنهكر اكونت كراش حد ومش بنصلح كمبيوترات (مش بنصلح كمبيوترات)<br />\r\nاحنا اللي بنعمل الابليكاشنز و ال &quot;websites&quot; زي موقع نتيجة الثانوية العامة كدا😃<br />\r\nاللي عامل الموقع دا &quot;software engineer&quot; ايوا هو دا المسمى الوظيفي بتاعنا مهندس برمجيات<br />\r\nطب بندرس ايه في الكلية..؟ 🤔<br />\r\nبندرس لغات برمجة زي &quot;c++&quot; و &quot;java&quot; و حاجات تانية زي &quot;Data structure&quot; و &quot;Algorithms&quot;<br />\r\nوكل دي حاجات مجرد Tools بتساعدك عشان تبدأ تدخل أي مجال أنت حابه 💖<br />\r\n&nbsp;ممكن متبقاش فاهم المصطلحات دي دلوقتي 😅</p>', 3, 3, 'kf7qyAUHR5ImEvLuJOp7X5baZYLs9UakvVVI2ftk.jpeg', 1, 'ltr', '2020-09-12 23:20:07', '2020-09-12 23:20:07'),
(11, 'java maram', 'طب حلو يعني اي بقي SE و ME و EE  ؟!\r\nتعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖\r\n🖇️ال Java SE هي اختصار Java Standard Edition وهي  تعتبر Core  ال Java  وعلشان تبدأ صح في اي مجال متعلق بال Java فأنت محتاج أنك تبدأ في ال Java SE وتبدأ تذاكرها كويس وتطبق عليها كتير', '<p>وبعد ما تتعلمها تقدر أنك تتعلم Java EE أو Java for Android وبعدها تقدر تدخل في أي مجال أنت حابب تشتغل فيه بال Java زي ال web أو ال Mobile Application أو Desktop Application وغيرهم..&nbsp;<br />\r\nوطبعََا Java SE ليها أكتر من إصدار وكل إصدار بيهدف لتطوير اللغة، وتوفير مجموعة معتمدة من واجهات برمجة التطبيقات (Gui) اللي بيحتاجها أي Java Developer .&nbsp;<br />\r\n🖇️و دلوقتي تعالى نعرف أي هي ال Resources اللي تقدر تذاكر منها؟ 🤔<br />\r\n⚡ للناس اللي بتحب الكتب والمراجع تقدر تبدأ تذاكر من كتب كتير 👇🏻<br />\r\nلو انت مش عارف &nbsp;برمجة &nbsp;وحابب تبدأ من الصفر ف هتلاقي أول مرجعين دول كويسين جدََا كبداية يعني تقدر تبدأ بأي واحد منهم&nbsp;<br />\r\n&nbsp;1-Java how to program 10th edition<br />\r\n2- introduction to Java 10th edition&nbsp;<br />\r\nوطبعََا في كتب كتير تانية زي &nbsp;&nbsp;<br />\r\n3- Big Java Early objects &nbsp;6th edition&nbsp;<br />\r\nوالكتاب دا جميل جدََا ومستواه أعلى شوية من اللي فوق ومهتم أكتر بال OOP&nbsp;<br />\r\n4- Core Java volume i<br />\r\nو الكتاب دا من أحسن الكتب اللي هتخليك توصل لمرحلة قوية جدََا في Java SE وبيشرح كل حاجة بالتفصيل بس ميفضلش أنك تبدأ بيه يعني&nbsp;<br />\r\n5- &nbsp; Core Java Volume ii<br />\r\nو دا الجزء التاني بتاع Core Java Volume &nbsp;i وطبعََا الكتاب دا فيه حاجات مهمة جدََا زي&nbsp;<br />\r\n📎Security&nbsp;<br />\r\n📎 Network programming<br />\r\n📎 Scripting&nbsp;<br />\r\n6- Java the complete reference</p>', 4, 3, 'XWgfxrLwpNM35WtOUgFFINybR9sRoMBvRfQWsPDP.jpeg', 1, 'ltr', '2020-09-12 23:22:20', '2020-09-12 23:22:20'),
(12, 'Computer science by maram', 'The earliest foundations of what would become computer science predate the invention of the modern digital computer. Machines for calculating fixed numerical tasks such as the abacus have existed since antiquity, aiding in computations such as multiplication and division. Algorithms for performing computations have existed since antiquity, even before the development of sophisticated computing equipment.', '<p><a href=\"https://en.wikipedia.org/wiki/Wilhelm_Schickard\">Wilhelm Schickard</a>&nbsp;designed and constructed the first working&nbsp;<a href=\"https://en.wikipedia.org/wiki/Mechanical_calculator\">mechanical calculator</a>&nbsp;in 1623.<a href=\"https://en.wikipedia.org/wiki/Computer_science#cite_note-13\">[13]</a>&nbsp;In 1673,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Gottfried_Leibniz\">Gottfried Leibniz</a>&nbsp;demonstrated a digital mechanical calculator, called the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Stepped_Reckoner\">Stepped Reckoner</a>.<a href=\"https://en.wikipedia.org/wiki/Computer_science#cite_note-14\">[14]</a>&nbsp;Leibniz may be considered the first computer scientist and information theorist, for, among other reasons, documenting the binary number system. In 1820,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Charles_Xavier_Thomas\">Thomas de Colmar</a>&nbsp;launched the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Mechanical_calculator\">mechanical calculator</a>&nbsp;industry<a href=\"https://en.wikipedia.org/wiki/Computer_science#cite_note-15\">[note 1]</a>&nbsp;when he invented his simplified&nbsp;<a href=\"https://en.wikipedia.org/wiki/Arithmometer\">arithmometer</a>, the first calculating machine strong enough and reliable enough to be used daily in an office environment.&nbsp;<a href=\"https://en.wikipedia.org/wiki/Charles_Babbage\">Charles Babbage</a>&nbsp;started the design of the first&nbsp;<em>automatic mechanical calculator</em>, his&nbsp;<a href=\"https://en.wikipedia.org/wiki/Difference_Engine\">Difference Engine</a>, in 1822, which eventually gave him the idea of the first&nbsp;<em>programmable mechanical calculator</em>, his&nbsp;<a href=\"https://en.wikipedia.org/wiki/Analytical_Engine\">Analytical Engine</a>.<a href=\"https://en.wikipedia.org/wiki/Computer_science#cite_note-16\">[15]</a></p>', 5, 3, 'IKuWzgmjQUZtM3vBxlAL6tz1imT04FWYApVyxsFX.jpeg', 1, 'ltr', '2020-09-12 23:24:47', '2020-09-12 23:24:47'),
(13, '\"Private Cloud\"  by Amira', '\" Hybrid Cloud\" تعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات. ', '<p>ال &quot;Cloud computing&quot; أو السحابة الحاسوبية من أهم التراكات اللي ظهرت الفترة دي بشكل كبير جدََا، ومن بداية ظهورها وهي غيرت فكرة شركات كتير جدََا في السوق خصوصََا شركات ال &quot;Hardware&quot; وال &quot;Software&quot; في طريقة الإنتاج والتعامل مع العملاء.&nbsp;</p>\r\n\r\n<p>تعالوا نعرف ازاي قدر ال &quot;Cloud computing&quot; يعمل تغيير في عالمنا؟! 👀<br />\r\nوعشان أفكارك متتلغبطش تعالي ناخدها خطوة خطوة 😃<br />\r\n▪️ اول حاجة محتاجين نعرفها يعني أي &quot;Cloud computing&quot;؟!...<br />\r\nكلمة &quot;Cloud computing&quot; معناها سحابة حاسوبية وهي بختصار كدا عبارة عن تجمع بعض الخدمات بحيث إنها تسهل على الناس القيام بحاجات كتير..&nbsp;<br />\r\n▪️اكيد جه في بالك اي هي أصلََا مكوناتها؟!..&nbsp;<br />\r\nتعالي نعرف أي هي مكونات ال&quot;Cloud computing&quot; ونتكلم عن كل مكون منهم.&nbsp;<br />\r\n1- Applications<br />\r\nهي البرامج والخدمات اللي يقدر يشغلها العميل في السحابة&nbsp;<br />\r\n2- Client<br />\r\nهو المستخدم &nbsp;اللي بيستخدم جهازه ( سواء كان موبايل او كمبيوتر للاستفادة من الخدمة .<br />\r\n3- Infrastructure<br />\r\nهي البنية التحتية للسحابة<br />\r\n4 - Platform<br />\r\nهي المنصة التي تستخدمها في السحابة , زي Python Django , Java Google Web Toolkit في جوجل.<br />\r\n5- Service<br />\r\nهي الخدمة اللي بتستخدمها على السحابة، وهي عملية تحويل منتجات الحاسب إلى خدمـات.<br />\r\n▪️ بعد ما عرفنا أي هو ال&quot;Cloud computing&quot; وأي مكوناتها جه وقت أنك تعرف هي بدأت ازاي تنتشر في العالم 🤔<br />\r\n▪️تعالي ناخد رحلة في بداية ال&quot;Cloud Computing&quot;..&nbsp;<br />\r\nبدأت رحلة ال &quot;Cloud computing&quot; مع شركات كبيرة لما بدأت الأسعار تزيد والمبيعات بتاعتها تقل ومن هنا بدأت فكرة التأجير واللي بيتبني عليها ال &quot;Cloud computing&quot; ومن هنا بدأ حل المشاكل اللي بتواجه الشركات عن طريق السحابة الحاسوبية وهما 3 حلول هما :<br />\r\n🔸ال &quot;SAAS&quot; اختصار ل &quot;Software as a service&quot;&nbsp;<br />\r\nاحنا كل يوم بنتعامل معاه وإحنا مش واخدين بالنا زي ال&quot;Gmail&quot; و&quot;Google Drive&quot; دي كلها عبارة عن online service وإحنا بنستخدمها يوميََا.&nbsp;<br />\r\nمن مميزاتها :&nbsp;<br />\r\n🔹التوفير فهي بتوفرلك كتير لأن زي ما قولنا التكلفة بتكون عالية جدََا&nbsp;<br />\r\n🔹مش بتحتاج أنك تعمل Set up أنت بتقدر تستخدم المنصة دي من غير ما تنزلها.&nbsp;<br />\r\n🔹تجنب مشاكل الإدارة لأنها بتكون من مسؤولية الشركة اللي بتوفر الخدمة دي.&nbsp;<br />\r\n🔹ال&quot;Backup&quot; و &quot;Restore&quot; وكل ال&quot;Data&quot; تبقى من مسؤولية الشركة وبتقدر تلاقي بياناتك موجودة ف أي وقت.&nbsp;<br />\r\n🔸️ ال&quot;PAAS&quot; وهو اختصار ل &quot;Platform as a service&quot;&nbsp;<br />\r\nهي عبارة عن شركة بتقدم بيئة عمل متكاملة بتتعامل بسهولة مع ال &quot;Server&quot; عن طريق ال&quot;control panel&quot; و بتخليك قادر تغير أو تعدل في ال &quot; Setting&quot; زي مانت عايز وامثلة على كدا ال &quot;Google Cloud&quot; وال &nbsp;Microsoft egor&quot; وبالتالي أنت بتقدر تتحكم في كل حاجة في ال &quot;Server&quot;&nbsp;<br />\r\nمميزاتها :<br />\r\n🔹بيئة عمل متكاملة بتخليك مسؤول عن كل حاجة وتقدر تعمل كل حاجة أنت عاوزها&nbsp;<br />\r\n🔹سهولة الإستخدام<br />\r\n🔹بتقدر تعمل &quot; Upgrade&quot; و &quot;Downgrade&quot; لكل الإمكانيات من غير اي ضرر للداتا&nbsp;<br />\r\n🔹أي حاجة خاصة بال &quot;Server&quot; من ال &quot;Backup&quot; أو &quot;Restore&quot; فهي من مسؤولية الشركة&nbsp;<br />\r\n🔸️ ال&quot;IAAS &quot; واللي هي اختصار ل &quot;Infrastructure as a service&quot;&nbsp;<br />\r\nوهي عبارة عن شركة بتقدملك &quot;Server&quot; بكل حاجة محتاجها و بتقدملك كمان ال &quot;CPU&quot; وال&quot;RAM&quot;، كمان بتقدملك &quot;Internet&quot; بشكل كويس&nbsp;<br />\r\nودي اللي بيقدر يستخدمها &quot;Server Admins&quot; أو شخص لازم يكون عنده خبرة في مجال ال &quot;Server Management&quot;&nbsp;<br />\r\nمن مميزاتها :<br />\r\n🔹سعره ارخص&nbsp;<br />\r\n🔹بتقدر تتحكم في ال &quot;Server&quot; بتاعك&nbsp;<br />\r\n🔹أسرع وإمكانياته أكتر&nbsp;<br />\r\n▪️ وبعد ما اتكلمنا عن الحلول اللي قدرت تقدمها لينا ال&quot;Cloud computing&quot; دلوقتى تعالوا نعرف هي أي انواعها؟!..&nbsp;<br />\r\nال&quot;Cloud computing&quot; ليه 3 أنواع طب أي هما الأنواع دي؟!🤔<br />\r\n🔺ال &quot;Public Cloud&quot;&nbsp;<br />\r\nوهي أن الشركة بتقدم الخدمات دي العامة وبيطلق على الشركة دي اسم &quot;Third party&quot; يعني الشركة دي بتقدر كمان إنها تقدم الخدمات دي لشركات تانية عشان تحصل على رسوم الاستخدام.&nbsp;<br />\r\n🔺ال &quot;Private Cloud&quot;&nbsp;<br />\r\nتعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات.&nbsp;<br />\r\n🔺ال &quot; Hybrid Cloud&quot;&nbsp;<br />\r\nوهي عبارة عن مزيج بين العامة والخاصة ودا عشان يتم الربط بين الخدمات عن طريق التقنيات الجديدة عشان تقدر توصل للخدمات بشكل عام.&nbsp;<br />\r\nبعد ما وصلنا لنهاية البوست بسلام دلوقتى نقدر نقول إنها ساعدت بشكل كبير جدََا في توفير ومساعدة شركات كتير ومش بس الشركات لا كمان أشخاص كتير من مستخدمي الشركات دي بشكل مجاني من غير تكلفة كبير 🔥👏🏻</p>\r\n\r\n<p>#MUFIX_Community<br />\r\n#Cloud_Computing</p>\r\n\r\n<p>ال &quot;Cloud computing&quot; أو السحابة الحاسوبية من أهم التراكات اللي ظهرت الفترة دي بشكل كبير جدََا، ومن بداية ظهورها وهي غيرت فكرة شركات كتير جدََا في السوق خصوصََا شركات ال &quot;Hardware&quot; وال &quot;Software&quot; في طريقة الإنتاج والتعامل مع العملاء.&nbsp;</p>\r\n\r\n<p>تعالوا نعرف ازاي قدر ال &quot;Cloud computing&quot; يعمل تغيير في عالمنا؟! 👀<br />\r\nوعشان أفكارك متتلغبطش تعالي ناخدها خطوة خطوة 😃<br />\r\n▪️ اول حاجة محتاجين نعرفها يعني أي &quot;Cloud computing&quot;؟!...<br />\r\nكلمة &quot;Cloud computing&quot; معناها سحابة حاسوبية وهي بختصار كدا عبارة عن تجمع بعض الخدمات بحيث إنها تسهل على الناس القيام بحاجات كتير..&nbsp;<br />\r\n▪️اكيد جه في بالك اي هي أصلََا مكوناتها؟!..&nbsp;<br />\r\nتعالي نعرف أي هي مكونات ال&quot;Cloud computing&quot; ونتكلم عن كل مكون منهم.&nbsp;<br />\r\n1- Applications<br />\r\nهي البرامج والخدمات اللي يقدر يشغلها العميل في السحابة&nbsp;<br />\r\n2- Client<br />\r\nهو المستخدم &nbsp;اللي بيستخدم جهازه ( سواء كان موبايل او كمبيوتر للاستفادة من الخدمة .<br />\r\n3- Infrastructure<br />\r\nهي البنية التحتية للسحابة<br />\r\n4 - Platform<br />\r\nهي المنصة التي تستخدمها في السحابة , زي Python Django , Java Google Web Toolkit في جوجل.<br />\r\n5- Service<br />\r\nهي الخدمة اللي بتستخدمها على السحابة، وهي عملية تحويل منتجات الحاسب إلى خدمـات.<br />\r\n▪️ بعد ما عرفنا أي هو ال&quot;Cloud computing&quot; وأي مكوناتها جه وقت أنك تعرف هي بدأت ازاي تنتشر في العالم 🤔<br />\r\n▪️تعالي ناخد رحلة في بداية ال&quot;Cloud Computing&quot;..&nbsp;<br />\r\nبدأت رحلة ال &quot;Cloud computing&quot; مع شركات كبيرة لما بدأت الأسعار تزيد والمبيعات بتاعتها تقل ومن هنا بدأت فكرة التأجير واللي بيتبني عليها ال &quot;Cloud computing&quot; ومن هنا بدأ حل المشاكل اللي بتواجه الشركات عن طريق السحابة الحاسوبية وهما 3 حلول هما :<br />\r\n🔸ال &quot;SAAS&quot; اختصار ل &quot;Software as a service&quot;&nbsp;<br />\r\nاحنا كل يوم بنتعامل معاه وإحنا مش واخدين بالنا زي ال&quot;Gmail&quot; و&quot;Google Drive&quot; دي كلها عبارة عن online service وإحنا بنستخدمها يوميََا.&nbsp;<br />\r\nمن مميزاتها :&nbsp;<br />\r\n🔹التوفير فهي بتوفرلك كتير لأن زي ما قولنا التكلفة بتكون عالية جدََا&nbsp;<br />\r\n🔹مش بتحتاج أنك تعمل Set up أنت بتقدر تستخدم المنصة دي من غير ما تنزلها.&nbsp;<br />\r\n🔹تجنب مشاكل الإدارة لأنها بتكون من مسؤولية الشركة اللي بتوفر الخدمة دي.&nbsp;<br />\r\n🔹ال&quot;Backup&quot; و &quot;Restore&quot; وكل ال&quot;Data&quot; تبقى من مسؤولية الشركة وبتقدر تلاقي بياناتك موجودة ف أي وقت.&nbsp;<br />\r\n🔸️ ال&quot;PAAS&quot; وهو اختصار ل &quot;Platform as a service&quot;&nbsp;<br />\r\nهي عبارة عن شركة بتقدم بيئة عمل متكاملة بتتعامل بسهولة مع ال &quot;Server&quot; عن طريق ال&quot;control panel&quot; و بتخليك قادر تغير أو تعدل في ال &quot; Setting&quot; زي مانت عايز وامثلة على كدا ال &quot;Google Cloud&quot; وال &nbsp;Microsoft egor&quot; وبالتالي أنت بتقدر تتحكم في كل حاجة في ال &quot;Server&quot;&nbsp;<br />\r\nمميزاتها :<br />\r\n🔹بيئة عمل متكاملة بتخليك مسؤول عن كل حاجة وتقدر تعمل كل حاجة أنت عاوزها&nbsp;<br />\r\n🔹سهولة الإستخدام<br />\r\n🔹بتقدر تعمل &quot; Upgrade&quot; و &quot;Downgrade&quot; لكل الإمكانيات من غير اي ضرر للداتا&nbsp;<br />\r\n🔹أي حاجة خاصة بال &quot;Server&quot; من ال &quot;Backup&quot; أو &quot;Restore&quot; فهي من مسؤولية الشركة&nbsp;<br />\r\n🔸️ ال&quot;IAAS &quot; واللي هي اختصار ل &quot;Infrastructure as a service&quot;&nbsp;<br />\r\nوهي عبارة عن شركة بتقدملك &quot;Server&quot; بكل حاجة محتاجها و بتقدملك كمان ال &quot;CPU&quot; وال&quot;RAM&quot;، كمان بتقدملك &quot;Internet&quot; بشكل كويس&nbsp;<br />\r\nودي اللي بيقدر يستخدمها &quot;Server Admins&quot; أو شخص لازم يكون عنده خبرة في مجال ال &quot;Server Management&quot;&nbsp;<br />\r\nمن مميزاتها :<br />\r\n🔹سعره ارخص&nbsp;<br />\r\n🔹بتقدر تتحكم في ال &quot;Server&quot; بتاعك&nbsp;<br />\r\n🔹أسرع وإمكانياته أكتر&nbsp;<br />\r\n▪️ وبعد ما اتكلمنا عن الحلول اللي قدرت تقدمها لينا ال&quot;Cloud computing&quot; دلوقتى تعالوا نعرف هي أي انواعها؟!..&nbsp;<br />\r\nال&quot;Cloud computing&quot; ليه 3 أنواع طب أي هما الأنواع دي؟!🤔<br />\r\n🔺ال &quot;Public Cloud&quot;&nbsp;<br />\r\nوهي أن الشركة بتقدم الخدمات دي العامة وبيطلق على الشركة دي اسم &quot;Third party&quot; يعني الشركة دي بتقدر كمان إنها تقدم الخدمات دي لشركات تانية عشان تحصل على رسوم الاستخدام.&nbsp;<br />\r\n🔺ال &quot;Private Cloud&quot;&nbsp;<br />\r\nتعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات.&nbsp;<br />\r\n🔺ال &quot; Hybrid Cloud&quot;&nbsp;<br />\r\nوهي عبارة عن مزيج بين العامة والخاصة ودا عشان يتم الربط بين الخدمات عن طريق التقنيات الجديدة عشان تقدر توصل للخدمات بشكل عام.&nbsp;<br />\r\nبعد ما وصلنا لنهاية البوست بسلام دلوقتى نقدر نقول إنها ساعدت بشكل كبير جدََا في توفير ومساعدة شركات كتير ومش بس الشركات لا كمان أشخاص كتير من مستخدمي الشركات دي بشكل مجاني من غير تكلفة كبير 🔥👏🏻</p>\r\n\r\n<p>#MUFIX_Community<br />\r\n#Cloud_Computing</p>', 8, 3, 'dEroXtz5Pdm94Qg3TlORaPVKnDJbSYEQTRFdLoRC.png', 1, 'ltr', '2020-09-12 23:26:13', '2020-09-12 23:26:13'),
(14, '\"Private Cloud\"  by Amira amira', '\" Hybrid Cloud\" تعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات. ', '<p>ال &quot;Cloud computing&quot; أو السحابة الحاسوبية من أهم التراكات اللي ظهرت الفترة دي بشكل كبير جدََا، ومن بداية ظهورها وهي غيرت فكرة شركات كتير جدََا في السوق خصوصََا شركات ال &quot;Hardware&quot; وال &quot;Software&quot; في طريقة الإنتاج والتعامل مع العملاء.&nbsp;</p>\r\n\r\n<p>تعالوا نعرف ازاي قدر ال &quot;Cloud computing&quot; يعمل تغيير في عالمنا؟! 👀<br />\r\nوعشان أفكارك متتلغبطش تعالي ناخدها خطوة خطوة 😃<br />\r\n▪️ اول حاجة محتاجين نعرفها يعني أي &quot;Cloud computing&quot;؟!...<br />\r\nكلمة &quot;Cloud computing&quot; معناها سحابة حاسوبية وهي بختصار كدا عبارة عن تجمع بعض الخدمات بحيث إنها تسهل على الناس القيام بحاجات كتير..&nbsp;<br />\r\n▪️اكيد جه في بالك اي هي أصلََا مكوناتها؟!..&nbsp;<br />\r\nتعالي نعرف أي هي مكونات ال&quot;Cloud computing&quot; ونتكلم عن كل مكون منهم.&nbsp;<br />\r\n1- Applications<br />\r\nهي البرامج والخدمات اللي يقدر يشغلها العميل في السحابة&nbsp;<br />\r\n2- Client<br />\r\nهو المستخدم &nbsp;اللي بيستخدم جهازه ( سواء كان موبايل او كمبيوتر للاستفادة من الخدمة .<br />\r\n3- Infrastructure<br />\r\nهي البنية التحتية للسحابة<br />\r\n4 - Platform<br />\r\nهي المنصة التي تستخدمها في السحابة , زي Python Django , Java Google Web Toolkit في جوجل.<br />\r\n5- Service<br />\r\nهي الخدمة اللي بتستخدمها على السحابة، وهي عملية تحويل منتجات الحاسب إلى خدمـات.<br />\r\n▪️ بعد ما عرفنا أي هو ال&quot;Cloud computing&quot; وأي مكوناتها جه وقت أنك تعرف هي بدأت ازاي تنتشر في العالم 🤔<br />\r\n▪️تعالي ناخد رحلة في بداية ال&quot;Cloud Computing&quot;..&nbsp;<br />\r\nبدأت رحلة ال &quot;Cloud computing&quot; مع شركات كبيرة لما بدأت الأسعار تزيد والمبيعات بتاعتها تقل ومن هنا بدأت فكرة التأجير واللي بيتبني عليها ال &quot;Cloud computing&quot; ومن هنا بدأ حل المشاكل اللي بتواجه الشركات عن طريق السحابة الحاسوبية وهما 3 حلول هما :<br />\r\n🔸ال &quot;SAAS&quot; اختصار ل &quot;Software as a service&quot;&nbsp;<br />\r\nاحنا كل يوم بنتعامل معاه وإحنا مش واخدين بالنا زي ال&quot;Gmail&quot; و&quot;Google Drive&quot; دي كلها عبارة عن online service وإحنا بنستخدمها يوميََا.&nbsp;<br />\r\nمن مميزاتها :&nbsp;<br />\r\n🔹التوفير فهي بتوفرلك كتير لأن زي ما قولنا التكلفة بتكون عالية جدََا&nbsp;<br />\r\n🔹مش بتحتاج أنك تعمل Set up أنت بتقدر تستخدم المنصة دي من غير ما تنزلها.&nbsp;<br />\r\n🔹تجنب مشاكل الإدارة لأنها بتكون من مسؤولية الشركة اللي بتوفر الخدمة دي.&nbsp;<br />\r\n🔹ال&quot;Backup&quot; و &quot;Restore&quot; وكل ال&quot;Data&quot; تبقى من مسؤولية الشركة وبتقدر تلاقي بياناتك موجودة ف أي وقت.&nbsp;<br />\r\n🔸️ ال&quot;PAAS&quot; وهو اختصار ل &quot;Platform as a service&quot;&nbsp;<br />\r\nهي عبارة عن شركة بتقدم بيئة عمل متكاملة بتتعامل بسهولة مع ال &quot;Server&quot; عن طريق ال&quot;control panel&quot; و بتخليك قادر تغير أو تعدل في ال &quot; Setting&quot; زي مانت عايز وامثلة على كدا ال &quot;Google Cloud&quot; وال &nbsp;Microsoft egor&quot; وبالتالي أنت بتقدر تتحكم في كل حاجة في ال &quot;Server&quot;&nbsp;<br />\r\nمميزاتها :<br />\r\n🔹بيئة عمل متكاملة بتخليك مسؤول عن كل حاجة وتقدر تعمل كل حاجة أنت عاوزها&nbsp;<br />\r\n🔹سهولة الإستخدام<br />\r\n🔹بتقدر تعمل &quot; Upgrade&quot; و &quot;Downgrade&quot; لكل الإمكانيات من غير اي ضرر للداتا&nbsp;<br />\r\n🔹أي حاجة خاصة بال &quot;Server&quot; من ال &quot;Backup&quot; أو &quot;Restore&quot; فهي من مسؤولية الشركة&nbsp;<br />\r\n🔸️ ال&quot;IAAS &quot; واللي هي اختصار ل &quot;Infrastructure as a service&quot;&nbsp;<br />\r\nوهي عبارة عن شركة بتقدملك &quot;Server&quot; بكل حاجة محتاجها و بتقدملك كمان ال &quot;CPU&quot; وال&quot;RAM&quot;، كمان بتقدملك &quot;Internet&quot; بشكل كويس&nbsp;<br />\r\nودي اللي بيقدر يستخدمها &quot;Server Admins&quot; أو شخص لازم يكون عنده خبرة في مجال ال &quot;Server Management&quot;&nbsp;<br />\r\nمن مميزاتها :<br />\r\n🔹سعره ارخص&nbsp;<br />\r\n🔹بتقدر تتحكم في ال &quot;Server&quot; بتاعك&nbsp;<br />\r\n🔹أسرع وإمكانياته أكتر&nbsp;<br />\r\n▪️ وبعد ما اتكلمنا عن الحلول اللي قدرت تقدمها لينا ال&quot;Cloud computing&quot; دلوقتى تعالوا نعرف هي أي انواعها؟!..&nbsp;<br />\r\nال&quot;Cloud computing&quot; ليه 3 أنواع طب أي هما الأنواع دي؟!🤔<br />\r\n🔺ال &quot;Public Cloud&quot;&nbsp;<br />\r\nوهي أن الشركة بتقدم الخدمات دي العامة وبيطلق على الشركة دي اسم &quot;Third party&quot; يعني الشركة دي بتقدر كمان إنها تقدم الخدمات دي لشركات تانية عشان تحصل على رسوم الاستخدام.&nbsp;<br />\r\n🔺ال &quot;Private Cloud&quot;&nbsp;<br />\r\nتعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات.&nbsp;<br />\r\n🔺ال &quot; Hybrid Cloud&quot;&nbsp;<br />\r\nوهي عبارة عن مزيج بين العامة والخاصة ودا عشان يتم الربط بين الخدمات عن طريق التقنيات الجديدة عشان تقدر توصل للخدمات بشكل عام.&nbsp;<br />\r\nبعد ما وصلنا لنهاية البوست بسلام دلوقتى نقدر نقول إنها ساعدت بشكل كبير جدََا في توفير ومساعدة شركات كتير ومش بس الشركات لا كمان أشخاص كتير من مستخدمي الشركات دي بشكل مجاني من غير تكلفة كبير 🔥👏🏻</p>\r\n\r\n<p>#MUFIX_Community<br />\r\n#Cloud_Computing</p>\r\n\r\n<p>ال &quot;Cloud computing&quot; أو السحابة الحاسوبية من أهم التراكات اللي ظهرت الفترة دي بشكل كبير جدََا، ومن بداية ظهورها وهي غيرت فكرة شركات كتير جدََا في السوق خصوصََا شركات ال &quot;Hardware&quot; وال &quot;Software&quot; في طريقة الإنتاج والتعامل مع العملاء.&nbsp;</p>\r\n\r\n<p>تعالوا نعرف ازاي قدر ال &quot;Cloud computing&quot; يعمل تغيير في عالمنا؟! 👀<br />\r\nوعشان أفكارك متتلغبطش تعالي ناخدها خطوة خطوة 😃<br />\r\n▪️ اول حاجة محتاجين نعرفها يعني أي &quot;Cloud computing&quot;؟!...<br />\r\nكلمة &quot;Cloud computing&quot; معناها سحابة حاسوبية وهي بختصار كدا عبارة عن تجمع بعض الخدمات بحيث إنها تسهل على الناس القيام بحاجات كتير..&nbsp;<br />\r\n▪️اكيد جه في بالك اي هي أصلََا مكوناتها؟!..&nbsp;<br />\r\nتعالي نعرف أي هي مكونات ال&quot;Cloud computing&quot; ونتكلم عن كل مكون منهم.&nbsp;<br />\r\n1- Applications<br />\r\nهي البرامج والخدمات اللي يقدر يشغلها العميل في السحابة&nbsp;<br />\r\n2- Client<br />\r\nهو المستخدم &nbsp;اللي بيستخدم جهازه ( سواء كان موبايل او كمبيوتر للاستفادة من الخدمة .<br />\r\n3- Infrastructure<br />\r\nهي البنية التحتية للسحابة<br />\r\n4 - Platform<br />\r\nهي المنصة التي تستخدمها في السحابة , زي Python Django , Java Google Web Toolkit في جوجل.<br />\r\n5- Service<br />\r\nهي الخدمة اللي بتستخدمها على السحابة، وهي عملية تحويل منتجات الحاسب إلى خدمـات.<br />\r\n▪️ بعد ما عرفنا أي هو ال&quot;Cloud computing&quot; وأي مكوناتها جه وقت أنك تعرف هي بدأت ازاي تنتشر في العالم 🤔<br />\r\n▪️تعالي ناخد رحلة في بداية ال&quot;Cloud Computing&quot;..&nbsp;<br />\r\nبدأت رحلة ال &quot;Cloud computing&quot; مع شركات كبيرة لما بدأت الأسعار تزيد والمبيعات بتاعتها تقل ومن هنا بدأت فكرة التأجير واللي بيتبني عليها ال &quot;Cloud computing&quot; ومن هنا بدأ حل المشاكل اللي بتواجه الشركات عن طريق السحابة الحاسوبية وهما 3 حلول هما :<br />\r\n🔸ال &quot;SAAS&quot; اختصار ل &quot;Software as a service&quot;&nbsp;<br />\r\nاحنا كل يوم بنتعامل معاه وإحنا مش واخدين بالنا زي ال&quot;Gmail&quot; و&quot;Google Drive&quot; دي كلها عبارة عن online service وإحنا بنستخدمها يوميََا.&nbsp;<br />\r\nمن مميزاتها :&nbsp;<br />\r\n🔹التوفير فهي بتوفرلك كتير لأن زي ما قولنا التكلفة بتكون عالية جدََا&nbsp;<br />\r\n🔹مش بتحتاج أنك تعمل Set up أنت بتقدر تستخدم المنصة دي من غير ما تنزلها.&nbsp;<br />\r\n🔹تجنب مشاكل الإدارة لأنها بتكون من مسؤولية الشركة اللي بتوفر الخدمة دي.&nbsp;<br />\r\n🔹ال&quot;Backup&quot; و &quot;Restore&quot; وكل ال&quot;Data&quot; تبقى من مسؤولية الشركة وبتقدر تلاقي بياناتك موجودة ف أي وقت.&nbsp;<br />\r\n🔸️ ال&quot;PAAS&quot; وهو اختصار ل &quot;Platform as a service&quot;&nbsp;<br />\r\nهي عبارة عن شركة بتقدم بيئة عمل متكاملة بتتعامل بسهولة مع ال &quot;Server&quot; عن طريق ال&quot;control panel&quot; و بتخليك قادر تغير أو تعدل في ال &quot; Setting&quot; زي مانت عايز وامثلة على كدا ال &quot;Google Cloud&quot; وال &nbsp;Microsoft egor&quot; وبالتالي أنت بتقدر تتحكم في كل حاجة في ال &quot;Server&quot;&nbsp;<br />\r\nمميزاتها :<br />\r\n🔹بيئة عمل متكاملة بتخليك مسؤول عن كل حاجة وتقدر تعمل كل حاجة أنت عاوزها&nbsp;<br />\r\n🔹سهولة الإستخدام<br />\r\n🔹بتقدر تعمل &quot; Upgrade&quot; و &quot;Downgrade&quot; لكل الإمكانيات من غير اي ضرر للداتا&nbsp;<br />\r\n🔹أي حاجة خاصة بال &quot;Server&quot; من ال &quot;Backup&quot; أو &quot;Restore&quot; فهي من مسؤولية الشركة&nbsp;<br />\r\n🔸️ ال&quot;IAAS &quot; واللي هي اختصار ل &quot;Infrastructure as a service&quot;&nbsp;<br />\r\nوهي عبارة عن شركة بتقدملك &quot;Server&quot; بكل حاجة محتاجها و بتقدملك كمان ال &quot;CPU&quot; وال&quot;RAM&quot;، كمان بتقدملك &quot;Internet&quot; بشكل كويس&nbsp;<br />\r\nودي اللي بيقدر يستخدمها &quot;Server Admins&quot; أو شخص لازم يكون عنده خبرة في مجال ال &quot;Server Management&quot;&nbsp;<br />\r\nمن مميزاتها :<br />\r\n🔹سعره ارخص&nbsp;<br />\r\n🔹بتقدر تتحكم في ال &quot;Server&quot; بتاعك&nbsp;<br />\r\n🔹أسرع وإمكانياته أكتر&nbsp;<br />\r\n▪️ وبعد ما اتكلمنا عن الحلول اللي قدرت تقدمها لينا ال&quot;Cloud computing&quot; دلوقتى تعالوا نعرف هي أي انواعها؟!..&nbsp;<br />\r\nال&quot;Cloud computing&quot; ليه 3 أنواع طب أي هما الأنواع دي؟!🤔<br />\r\n🔺ال &quot;Public Cloud&quot;&nbsp;<br />\r\nوهي أن الشركة بتقدم الخدمات دي العامة وبيطلق على الشركة دي اسم &quot;Third party&quot; يعني الشركة دي بتقدر كمان إنها تقدم الخدمات دي لشركات تانية عشان تحصل على رسوم الاستخدام.&nbsp;<br />\r\n🔺ال &quot;Private Cloud&quot;&nbsp;<br />\r\nتعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات.&nbsp;<br />\r\n🔺ال &quot; Hybrid Cloud&quot;&nbsp;<br />\r\nوهي عبارة عن مزيج بين العامة والخاصة ودا عشان يتم الربط بين الخدمات عن طريق التقنيات الجديدة عشان تقدر توصل للخدمات بشكل عام.&nbsp;<br />\r\nبعد ما وصلنا لنهاية البوست بسلام دلوقتى نقدر نقول إنها ساعدت بشكل كبير جدََا في توفير ومساعدة شركات كتير ومش بس الشركات لا كمان أشخاص كتير من مستخدمي الشركات دي بشكل مجاني من غير تكلفة كبير 🔥👏🏻</p>\r\n\r\n<p>#MUFIX_Community<br />\r\n#Cloud_Computing</p>', 8, 3, 'kVZ32LwcbKrgYsOISNLMx3oSQZx12vasnuitNSXZ.jpeg', 1, 'ltr', '2020-09-12 23:26:54', '2020-09-12 23:26:54'),
(15, 'information system maram', 'Information system, an integrated set of components for collecting, storing, and processing data and for providing information, knowledge, and digital products. Business firms and other organizations rely on information systems to carry out and manage their operations, interact with their customers and suppliers, and compete in the marketplace.', '<p>Information systems are used to run interorganizational supply chains and electronic markets. For instance, corporations use information systems to process financial accounts, to manage their human resources, and to reach their potential customers with online promotions. Many major companies are built entirely around information systems. These include&nbsp;<a href=\"https://www.britannica.com/topic/eBay\">eBay</a>, a largely auction marketplace;&nbsp;<a href=\"https://www.britannica.com/topic/Amazoncom\">Amazon</a>, an expanding electronic mall and provider of&nbsp;<a href=\"https://www.britannica.com/technology/cloud-computing\">cloud computing</a>&nbsp;services; Alibaba, a business-to-business e-marketplace; and&nbsp;<a href=\"https://www.britannica.com/topic/Google-Inc\">Google</a>, a&nbsp;<a href=\"https://www.britannica.com/technology/search-engine\">search engine</a>&nbsp;company that derives most of its revenue from keyword advertising on&nbsp;<a href=\"https://www.britannica.com/technology/Internet\">Internet</a>&nbsp;searches. Governments&nbsp;<a href=\"https://www.merriam-webster.com/dictionary/deploy\">deploy</a>&nbsp;information systems to provide services cost-effectively to citizens. Digital goods&mdash;such as&nbsp;<a href=\"https://www.britannica.com/technology/e-book\">electronic books</a>, video products, and&nbsp;<a href=\"https://www.britannica.com/technology/software\">software</a>&mdash;and online services, such as gaming and&nbsp;<a href=\"https://www.britannica.com/technology/social-network\">social networking</a>, are delivered with information systems. Individuals rely on information systems, generally Internet-based, for conducting much of their personal lives: for socializing, study, shopping, banking, and entertainment.</p>', 2, 3, 'QwYoFvmSwnLcq5iMTYyT5Jk5o01ZbwSaUFTUWopi.jpeg', 1, 'ltr', '2020-09-12 23:29:03', '2020-09-12 23:47:34'),
(16, 'machine learning maram', 'Artificial intelligence is a technology that is already impacting how users interact with, and are affected by the Internet. In the near future, its impact is likely to only continue to grow.', '<p>Artificial intelligence is a technology that is already impacting how users interact with, and are affected by the Internet. In the near future, its impact is likely to only continue to grow. AI has the potential to vastly change the way that humans interact, not only with the digital world, but also with each other, through their work and through other socioeconomic institutions &ndash; for better or for worse.</p>\r\n\r\n<p>If we are to ensure that the impact of artificial intelligence will be positive, it will be essential that all stakeholders participate in the debates surrounding AI.</p>\r\n\r\n<p>In this paper, we seek to provide an introduction to AI to policymakers and other stakeholders in the wider Internet ecosystem.</p>', 4, 3, 'wFUQo4q6UZVls8v3bQ42xDeo3X0zgclCfJrDWIBl.jpeg', 1, 'ltr', '2020-09-12 23:33:19', '2020-09-12 23:33:19'),
(17, '\"What is a Control System?\" maram', 'Controls Systems are everywhere, undetected by the naked eye. Control Systems are used to continually regulate the behaviour of devices in a repeatable and predicted way. They may be simple electronic or electrical devices or very sophisticated computer-controlled systems.\r\nThe fundamentals of a Control System include measuring an error signal and then adjusting the system to reach the desired course or outcome', '<p>If the system deviates too fast and the control system is unable to adjust the course or outcome, the Control System is inadequate to manage the disturbances.<br />\r\nSome basic examples of a control system include a cruise control in a motor vehicle, temperature control in a building, the chemical concentrations in drinking water, the speed of a conveyor belt in a process plant.<br />\r\nControl Systems are used in domestic applications, general industry, military and virtually every modern vehicle in the world. Control Systems are very common in SCADA and Industrial Automation systems.<br />\r\nControl Systems are used in Industrial Automation to regulate how devices operate in real time. In a closed-loop control system the controller (RTU, PLCS, DCS) feedback (error) signal is used to adjust the control variable such that the process is constantly trying to match the operational set point. The system described here is generally referred to as a control-loop. The controller must have a suitable dynamic response to be able to adjust the system to remain stable. If the controller is unable to adjust to a stable condition (ie with minimal hunting) the control-loop is said to be &ldquo;out of control&rdquo;. In very complex processes a single closed-loop control system may be insufficient to stabilise the process. Cascaded control-loops may be used in situations where there is moderate complexity and the process is &ldquo;changing quickly&rdquo; in respect to time, however Advanced Process Controllers may be required for slow moving processes like distilling, bio-reactors etc.</p>', 10, 3, 'J4MspTew2tiBRacGQzqaRZL6s0Z9icPvrYc4RL08.png', 1, 'ltr', '2020-09-12 23:36:08', '2020-09-12 23:36:08'),
(18, '\"What is IT?\" maram', 'The most basic information technology definition is that it\'s the application of technology to solve business or organizational problems on a broad scale. No matter the role, a member of an IT department works with others to solve technology problems, both big and small.', '<p>Simply put, the work of most organizations would slow to a crawl without functioning IT systems. You&rsquo;d be hard-pressed to find a business that doesn&rsquo;t at least partially rely on computers and the networks that connect them. Maintaining a standard level of service, security and connectivity is a huge task, but it&rsquo;s not the only priority or potential challenge on their plates.</p>\r\n\r\n<p>More and more companies want to implement more intuitive and sophisticated solutions. &ldquo;IT can provide the edge a company needs to outsmart, outpace and out-deliver competitors,&rdquo; says Edward Kiledjian, a Chief Information Security Officer and&nbsp;<a href=\"https://www.kiledjian.com/\">technology blogger</a>. Let&rsquo;s take a look at the needs that current and future IT specialists will be working on:</p>\r\n\r\n<ul>\r\n	<li><strong>Data overload:</strong>&nbsp;Businesses need to process huge amounts of data. This requires large amounts of processing power, sophisticated software and human analytical skills.</li>\r\n	<li><strong>Mobile and wireless usages:</strong>&nbsp;More employers are offering remote work options that require smartphones, tablets and laptops with wireless hotspots and roaming ability.</li>\r\n	<li><strong>Cloud services:</strong>&nbsp;Most businesses no longer operate their own &ldquo;server farms&rdquo; to store massive amounts of data. Many businesses now work with cloud services&mdash;third-party hosting platforms that maintain that data.</li>\r\n	<li><strong>Bandwidth for video hosting:</strong>&nbsp;Videoconferencing solutions have become more and more popular, so more network bandwidth is needed to support them sufficiently.</li>\r\n</ul>', 11, 3, 'p0r74kuXyvGwoXPsp2APxFVyUhbdhQ3ZWeXoEZ5f.jpeg', 1, 'ltr', '2020-09-12 23:39:03', '2020-09-12 23:39:03'),
(19, 'مؤهلات المبرمج الناجح | هاجر', 'ممكن أكون مبرمج كويس وأقدم على وظيفة واترفض ؟ للأسف أيوا. \r\nسوق العمل اتغير كتير مع مرور الوقت، مؤهلاتك ومهاراتك في البرمجة بس مش دايما هيجبولك الوظيفة', '<p>ممكن أكون مبرمج كويس وأقدم على وظيفة واترفض ؟ للأسف أيوا.</p>\r\n\r\n<p>سوق العمل اتغير كتير مع مرور الوقت، مؤهلاتك ومهاراتك في البرمجة بس مش دايما هيجبولك الوظيفة</p>\r\n\r\n<p>سوق العمل ملئ بخريجي الجامعات، بعضهم اللي عنده مواهب وشغف والبعض الأخر بيتظاهر بكده،</p>\r\n\r\n<p>و أصحاب العمل بيختاروا الأفضل دايما واللي عندهم معرفه بمجالات ثانيه.</p>\r\n\r\n<p>ودي بعض الطرق عشان تثير إعجاب المحاور وتثبت إنك تستحق الوظيفة .</p>\r\n\r\n<p>- تشارك في مشاريع مفتوحة المصدر &quot; Contribute to open source projects &quot;</p>\r\n\r\n<p>إيه المميز في إنك تشارك في مشاريع مفتوحة المصدر ؟ في الحقيقة في ميزات كتيرة وأهمها انك بتتعمل من ناس خبراء في صناعة البرمجيات. ازاي ؟ طبعا معظم مشاركاتك مش هتتقبل. ايه ده دي ميزة ؟ أيوا طبعا ميزة، أي مساهمة ليك بتتراجع من ناس خبيره زي ما قلنا ولو فيه حاجه لازم تتعدل في مساهمتك، الناس اللي بتراجع هتقولك بعض الإرشادات وكيفية التعديل على الحاجه دي، الموضوع شبيه بإن حد بيشرحلك ويوجهك للصح. وغير كده في ميزات كتير زي إنك تتعلم مهارات جديدة و هتقابل ناس عندهم نفس اهتماماتك. وطبعا لو قدمت أي مساهمة هتبقا نقطه قويه لإثارة إعجاب المحاور الخاص بيك.</p>\r\n\r\n<p>ده مقال بيعرفك ازاي تشارك في مشروعات مفتوحة المصدر.</p>\r\n\r\n<p>- تجاوب أسئله على &quot;Stack Overflow&quot;</p>\r\n\r\n<p>طبعا كلنا عارفين أفضل صديق للمبرمجين ولو عندك مشكلة بتلجأله هو &quot;Stack Overflow&quot; ، هتكون حاجه جميلة لو جاوبت على سؤال على &quot;Stack Overflow&quot; لان لأي حساب نقاط سمعه وأي سؤال بتجاوب عليه وبناءا على دقة إجابتك يمكن للمستخدمين التصويت لإجابتك وده هيزود نقاط سمعتك، ومن خلال ده ممكن لصاحب العمل يحدد قد إيه انت موهوب.</p>\r\n\r\n<p>- الاسكور على &quot;HackerRank , Codeforces , Leetcode , GeeksforGeeks&quot;</p>\r\n\r\n<p>دي مواقع &quot;HackerRank , Codeforces , Leetcode , GeeksforGeeks&quot; لتحسين مهاراتك في الكود عن طريق حل تحديات ومشاكل بلغة البرمجة المفضلة ليك. وانت اقدر تختار موضوع معين وتحل مسائل وتحديات عليه وممكن تحدد درجة صعوبة التحديات وتبدأ تحل، وبناءا على حلك الاسكور بتاعك هيزيد، والاسكور بتاعك هيحدد موهبتك في ال</p>\r\n\r\n<p>&quot;Problem Solving , Algorithms , Data Structure&quot; ...</p>\r\n\r\n<p>- عمل مشاريع جديدة</p>\r\n\r\n<p>الخروج بأفكار إبداعية وتطوير مشاريعك الخاصه من أفضل الطرق عشان تطبق اللي اتعلمته، ممكن تكون اتعلمت تقنيات مختلفة ولغات برمجة مختلفة بس لو انت مش بتقدر تطبق ده في صورك برنامج أو موقع ويب أو موبايل أبلكيشن يبقى جهدك ضاع. وعمل مشاريع جديدة مش هيحسن مهارات الكود بس لا وممكن تستخدم المشاريع دي في حياتك الشخصية.</p>\r\n\r\n<p>- إظهار حماسك بصناعة البرمجيات على مواقع التواصل</p>\r\n\r\n<p>الفتره الأخيره كتير من أصحاب العمل بقوا مهتمين يشوفوا أكونت الشخص على السوشيال ميديا، وطبعا كلنا عارفين ان الغرض من السوشيال ميديا هي المتعة في الوقت الفاضي اللي عندنا، وطبعا أنا مش بقول نوقف الضحك والميمز على السوشيال ميديا. بس حاجه جميله انك تشير ميمز ليها علاقة بالبرمجة والتكنولوجيا وممكن برضه تشير الفيديوهات والمقالات اللي الناس بتحب تقرأها.</p>\r\n\r\n<p><a href=\"https://www.facebook.com/hashtag/mufix_community?__eep__=6&amp;__cft__[0]=AZWsVPqq1_4o3apwUCiswQk0P55GMLcglpKh2at6jc1HPSf0RVO_6NY5OsRbSuvQYmv-Ix0v9pOJ5I8vY_jN2t5kRgVKmEjLKUApgWid9eWnZv8Ok8Y6vWaVjKdGlRuOmNb0W_zVX_508SpxFCiNn_QfhC0T5LP4EtUI1zyk0R7LOw&amp;__tn__=*NK-R\" tabindex=\"0\">#MUFIX_Community</a></p>', 5, 3, 'FjltZd8ZeDmSs888IPab7z01np0wY5nxLvyHEpnZ.png', 1, 'ltr', '2020-09-13 06:28:13', '2020-09-13 06:33:08');
INSERT INTO `posts` (`id`, `title`, `description`, `content`, `category_id`, `user_id`, `image`, `status`, `dir`, `created_at`, `updated_at`) VALUES
(20, 'data science | hager', 'یه هو الا\"Data Science\"؟\r\n📌هو علم بیستخدم الأسالیب العلمیة والمعالجات وال\"Algorithms\"عشان تستخرج المعلومات من البیانات وبیعتمد على تعلم الآلة\r\nوالذكاء الصناعي وبرامج معالجة البیانات الضخمة عشان تقدم الدعم اللازم لاتخاذ القرارات.', '<p>&nbsp;كتیر مننا نفسه یدخل فى مجال ال &quot;Data Science &quot; ویشتغل فیه، لكن مش عارف یذاكر ایه او یبدأ ازاي احنا في المقال دا هنحاول نساعد انك تكون عارف علي الاقل ایه اللي مطالب انك تكون<br />\r\nمذاكره بحیث تبقي ناجح ومتمیز في المجال. 🔥<br />\r\nایه هو ال&quot;Data Science&quot;؟<br />\r\n📌هو علم بیستخدم الأسالیب العلمیة والمعالجات وال&quot;Algorithms&quot;عشان تستخرج المعلومات من البیانات وبیعتمد على تعلم الآلة<br />\r\nوالذكاء الصناعي وبرامج معالجة البیانات الضخمة عشان تقدم الدعم اللازم لاتخاذ القرارات.<br />\r\nا أنت محتاج أنك تكون على درایة كافیة بال&quot;Topics &quot;اللي مذكورة تحت دي👇🔥<br />\r\nوعشان تدخل المجال مبدئَیَ<br />\r\n- &quot;linear Algebra&quot;<br />\r\n- &quot;Convex Optimization&quot;<br />\r\n- &quot;Differential Equations&quot;<br />\r\n- &quot;Algorithms&quot;<br />\r\n- &quot;Distributed Computing&quot;<br />\r\n- قواعد البیانات(&quot;NoSQL&quot;,&quot;SQL&quot;)<br />\r\n- &quot;Machine Learning&quot;<br />\r\n- &quot;Probabilistic Modeling&quot;<br />\r\n- &quot;Deep Learning&quot;<br />\r\n- &quot;Natural Language Processing&quot;<br />\r\n- &quot;Data Visualization&quot;<br />\r\n- &quot;Data Big, Design Experimental&quot;<br />\r\n- &quot;scala &quot;للبرمجة الوظیفیة<br />\r\n- &quot;Hadoop &quot;Functional Analysis&quot;</p>\r\n\r\n<p>📌طبعا بتفكر تقلب البوست وبتقول أنا هفضل طول حیاتي اذاكر كدا ومش هشتغل 😅<br />\r\nعایزك بس تصبر وتركز في الكلام اللي هقولهولك دا مجال الداتا ساینس من أصعب المجالات اللي تقدر تدخلها و فرص العمل فیه<br />\r\nمتوفرة بطریقة كویسة جدا ودا لندرة الناس اللى شغاله فیه ال&quot;Topics &quot;اللي فوق دي لازم تذاكر أكبر عدد ممكن منها وتكون<br />\r\nعارف معلومات كتیر خاصة بكل توبیك فیهم عشان تقدر تكون متمیز في المجال دا<br />\r\n📌وعلشان نساعدك أكتر جبنالك اقتراحات ل&quot;Courses &quot;كویسة بتشرح بعض من ال&quot;Topics &quot;اللي فوق تقدر تذاكر منهم لو<br />\r\nحابب.🔥<br />\r\nوده كورسات وبعض المحاضرات اللي تقدر تفیدك في ال&quot;learning machine👇&quot;<br />\r\n●https://www.coursera.org/learn/machine-learning<br />\r\n●https://www.youtube.com/watch?v=FqcKTNcRH88&amp;list=PL_onPhFCkVQhUzcTVgQiC8W2<br />\r\nShZKWlm0s<br />\r\n●https://www.youtube.com/watch?v=pid0lUH467o&amp;list=PLE6Wd9FR--Ecf_5nCbnSQMHqO<br />\r\nRpiChfJf<br />\r\n●https://www.youtube.com/watch?v=w2OtwL5T1ow&amp;list=PLE6Wd9FR--EdyJ5lbFl8UuGjecv<br />\r\nVw66F6<br />\r\nودي كورسات علشان ال&quot;NoSQL👇&quot;<br />\r\n●https://www.mongodb.com/nosql-explained<br />\r\n●https://www.youtube.com/watch?v=vb8xZ-bvxbg&amp;list=PLLAZ4kZ9dFpOFJ9JcVW9u4PlSW<br />\r\nO-VFoao<br />\r\nوده كورس علشان ال&quot;SQL👇&quot;<br />\r\n●https://www.youtube.com/watch?v=l3DCBn2gkZs&amp;list=PL158D091D26F47358<br />\r\nوده كورس علشان&quot;algorithms👇&quot;<br />\r\n●https://www.youtube.com/playlist?list=PLEBRPBUkZ4maAlTZw3eZFwfwIGXaln0in<br />\r\nوده كتاب&quot; algorithms to introduction&quot;هیساعدك برضو فى ال&quot;algorithms👇&quot;<br />\r\n●https://www.amazon.com/Introduction-Algorithms-3rd-MIT-Press/dp/0262033844<br />\r\n#DataScience<br />\r\n#MUFIX_Community<br />\r\n&nbsp;</p>', 6, 3, 'p0pMuJ1tmQaerTasehfRQvl71zBLnnybOLXfMe2j.jpeg', 1, 'ltr', '2020-09-13 07:06:10', '2020-09-13 07:06:34'),
(21, 'Post by hatem12', 'Post by hatem get error in hager pc', '<p>ال &quot;Cloud computing&quot; أو السحابة الحاسوبية من أهم التراكات اللي ظهرت الفترة دي بشكل كبير جدََا، ومن بداية ظهورها وهي غيرت فكرة شركات كتير جدََا في السوق خصوصََا شركات ال &quot;Hardware&quot; وال &quot;Software&quot; في طريقة الإنتاج والتعامل مع العملاء.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>تعالوا نعرف ازاي قدر ال &quot;Cloud computing&quot; يعمل تغيير في عالمنا؟! 👀</p>\r\n\r\n<p>وعشان أفكارك متتلغبطش تعالي ناخدها خطوة خطوة 😃</p>\r\n\r\n<p>▪️ اول حاجة محتاجين نعرفها يعني أي &quot;Cloud computing&quot;؟!...</p>\r\n\r\n<p>كلمة &quot;Cloud computing&quot; معناها سحابة حاسوبية وهي بختصار كدا عبارة عن تجمع بعض الخدمات بحيث إنها تسهل على الناس القيام بحاجات كتير..&nbsp;</p>\r\n\r\n<p>▪️اكيد جه في بالك اي هي أصلََا مكوناتها؟!..&nbsp;</p>\r\n\r\n<p>تعالي نعرف أي هي مكونات ال&quot;Cloud computing&quot; ونتكلم عن كل مكون منهم.&nbsp;</p>\r\n\r\n<p>1- Applications</p>\r\n\r\n<p>هي البرامج والخدمات اللي يقدر يشغلها العميل في السحابة&nbsp;</p>\r\n\r\n<p>2- Client</p>\r\n\r\n<p>هو المستخدم اللي بيستخدم جهازه ( سواء كان موبايل او كمبيوتر للاستفادة من الخدمة .</p>\r\n\r\n<p>3- Infrastructure</p>\r\n\r\n<p>هي البنية التحتية للسحابة</p>\r\n\r\n<p>4 - Platform</p>\r\n\r\n<p>هي المنصة التي تستخدمها في السحابة , زي Python Django , Java Google Web Toolkit في جوجل.</p>\r\n\r\n<p>5- Service</p>\r\n\r\n<p>هي الخدمة اللي بتستخدمها على السحابة، وهي عملية تحويل منتجات الحاسب إلى خدمـات.</p>\r\n\r\n<p>▪️ بعد ما عرفنا أي هو ال&quot;Cloud computing&quot; وأي مكوناتها جه وقت أنك تعرف هي بدأت ازاي تنتشر في العالم 🤔</p>\r\n\r\n<p>▪️تعالي ناخد رحلة في بداية ال&quot;Cloud Computing&quot;..&nbsp;</p>\r\n\r\n<p>بدأت رحلة ال &quot;Cloud computing&quot; مع شركات كبيرة لما بدأت الأسعار تزيد والمبيعات بتاعتها تقل ومن هنا بدأت فكرة التأجير واللي بيتبني عليها ال &quot;Cloud computing&quot; ومن هنا بدأ حل المشاكل اللي بتواجه الشركات عن طريق السحابة الحاسوبية وهما 3 حلول هما :</p>\r\n\r\n<p>🔸ال &quot;SAAS&quot; اختصار ل &quot;Software as a service&quot;&nbsp;</p>\r\n\r\n<p>احنا كل يوم بنتعامل معاه وإحنا مش واخدين بالنا زي ال&quot;Gmail&quot; و&quot;Google Drive&quot; دي كلها عبارة عن online service وإحنا بنستخدمها يوميََا.&nbsp;</p>\r\n\r\n<p>من مميزاتها :&nbsp;</p>\r\n\r\n<p>🔹التوفير فهي بتوفرلك كتير لأن زي ما قولنا التكلفة بتكون عالية جدََا&nbsp;</p>\r\n\r\n<p>🔹مش بتحتاج أنك تعمل Set up أنت بتقدر تستخدم المنصة دي من غير ما تنزلها.&nbsp;</p>\r\n\r\n<p>🔹تجنب مشاكل الإدارة لأنها بتكون من مسؤولية الشركة اللي بتوفر الخدمة دي.&nbsp;</p>\r\n\r\n<p>🔹ال&quot;Backup&quot; و &quot;Restore&quot; وكل ال&quot;Data&quot; تبقى من مسؤولية الشركة وبتقدر تلاقي بياناتك موجودة ف أي وقت.&nbsp;</p>\r\n\r\n<p>🔸️ ال&quot;PAAS&quot; وهو اختصار ل &quot;Platform as a service&quot;&nbsp;</p>\r\n\r\n<p>هي عبارة عن شركة بتقدم بيئة عمل متكاملة بتتعامل بسهولة مع ال &quot;Server&quot; عن طريق ال&quot;control panel&quot; و بتخليك قادر تغير أو تعدل في ال &quot; Setting&quot; زي مانت عايز وامثلة على كدا ال &quot;Google Cloud&quot; وال Microsoft egor&quot; وبالتالي أنت بتقدر تتحكم في كل حاجة في ال &quot;Server&quot;&nbsp;</p>\r\n\r\n<p>مميزاتها :</p>\r\n\r\n<p>🔹بيئة عمل متكاملة بتخليك مسؤول عن كل حاجة وتقدر تعمل كل حاجة أنت عاوزها&nbsp;</p>\r\n\r\n<p>🔹سهولة الإستخدام</p>\r\n\r\n<p>🔹بتقدر تعمل &quot; Upgrade&quot; و &quot;Downgrade&quot; لكل الإمكانيات من غير اي ضرر للداتا&nbsp;</p>\r\n\r\n<p>🔹أي حاجة خاصة بال &quot;Server&quot; من ال &quot;Backup&quot; أو &quot;Restore&quot; فهي من مسؤولية الشركة&nbsp;</p>\r\n\r\n<p>🔸️ ال&quot;IAAS &quot; واللي هي اختصار ل &quot;Infrastructure as a service&quot;&nbsp;</p>\r\n\r\n<p>وهي عبارة عن شركة بتقدملك &quot;Server&quot; بكل حاجة محتاجها و بتقدملك كمان ال &quot;CPU&quot; وال&quot;RAM&quot;، كمان بتقدملك &quot;Internet&quot; بشكل كويس&nbsp;</p>\r\n\r\n<p>ودي اللي بيقدر يستخدمها &quot;Server Admins&quot; أو شخص لازم يكون عنده خبرة في مجال ال &quot;Server Management&quot;&nbsp;</p>\r\n\r\n<p>من مميزاتها :</p>\r\n\r\n<p>🔹سعره ارخص&nbsp;</p>\r\n\r\n<p>🔹بتقدر تتحكم في ال &quot;Server&quot; بتاعك&nbsp;</p>\r\n\r\n<p>🔹أسرع وإمكانياته أكتر&nbsp;</p>\r\n\r\n<p>▪️ وبعد ما اتكلمنا عن الحلول اللي قدرت تقدمها لينا ال&quot;Cloud computing&quot; دلوقتى تعالوا نعرف هي أي انواعها؟!..&nbsp;</p>\r\n\r\n<p>ال&quot;Cloud computing&quot; ليه 3 أنواع طب أي هما الأنواع دي؟!🤔</p>\r\n\r\n<p>🔺ال &quot;Public Cloud&quot;&nbsp;</p>\r\n\r\n<p>وهي أن الشركة بتقدم الخدمات دي العامة وبيطلق على الشركة دي اسم &quot;Third party&quot; يعني الشركة دي بتقدر كمان إنها تقدم الخدمات دي لشركات تانية عشان تحصل على رسوم الاستخدام.&nbsp;</p>\r\n\r\n<p>🔺ال &quot;Private Cloud&quot;&nbsp;</p>\r\n\r\n<p>تعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات.&nbsp;</p>\r\n\r\n<p>🔺ال &quot; Hybrid Cloud&quot;&nbsp;</p>\r\n\r\n<p>وهي عبارة عن مزيج بين العامة والخاصة ودا عشان يتم الربط بين الخدمات عن طريق التقنيات الجديدة عشان تقدر توصل للخدمات بشكل عام.&nbsp;</p>\r\n\r\n<p>بعد ما وصلنا لنهاية البوست بسلام دلوقتى نقدر نقول إنها ساعدت بشكل كبير جدََا في توفير ومساعدة شركات كتير ومش بس الشركات لا كمان أشخاص كتير من مستخدمي الشركات دي بشكل مجاني من غير تكلفة كبير 🔥👏🏻</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>#MUFIX_Community</p>\r\n\r\n<p>#Cloud_Computing</p>', 3, 1, 'LpKGQj9fVYYwYiZK4aqA8jcVG4UP59ctgeSfPWHV.png', 1, 'ltr', '2020-09-13 07:28:51', '2020-09-13 08:56:45'),
(22, 'Post by hatem2', 'Post2 by hatem get error in hager pc', '<p>ال &quot;Cloud computing&quot; أو السحابة الحاسوبية من أهم التراكات اللي</p>\r\n\r\n<p>ولسه مكملين معاكم رحلتنا اللي بدأناها عن ال Java 🔥&nbsp;<br />\r\nوبعد ما اتكلمنا في البوست اللي فات عن بداية ال Java كانت فين وازاي؟ وتقدر تقرأ البوست دا * https://bit.ly/3k5W8Mk&nbsp; * عشان تسترجع معلوماتك ولو مقرأتهوش هتستفيد جدََا برضو لما تقرأه 💖<br />\r\nودلوقتي هنكمل كلامنا وهنتكلم عن أنواع ال Java وازاي اذاكرها كويس وأبدأ فيها صح ومنين! 🤔<br />\r\nال&quot;Java&quot; ليها أكتر من نوع&nbsp; زي :<br />\r\n▪️Java SE&nbsp;<br />\r\n▪️ Java ME&nbsp;<br />\r\n▪️ Java EE&nbsp;<br />\r\nطب حلو يعني اي بقي SE و ME و EE&nbsp; ؟!<br />\r\nتعالوا نعرف كل واحدة لوحدها، والنهاردة هنتكلم عن ال Java SE إن شاء الله 💖<br />\r\n🖇️ال Java SE هي اختصار Java Standard Edition وهي&nbsp; تعتبر Core&nbsp; ال Java&nbsp; وعلشان تبدأ صح في اي مجال متعلق بال Java فأنت محتاج أنك تبدأ في ال Java SE وتبدأ تذاكرها كويس وتطبق عليها كتير 👌🏻<br />\r\nوبعد ما تتعلمها تقدر أنك تتعلم Java EE أو Java for Android وبعدها تقدر تدخل في أي مجال أنت حابب تشتغل فيه بال Java زي ال web أو ال Mobile Application أو Desktop Application وغيرهم..&nbsp;<br />\r\nوطبعََا Java SE ليها أكتر من إصدار وكل إصدار بيهدف لتطوير اللغة، وتوفير مجموعة معتمدة من واجهات برمجة التطبيقات (Gui) اللي بيحتاجها أي Java Developer .&nbsp;<br />\r\n🖇️و دلوقتي تعالى نعرف أي هي ال Resources اللي تقدر تذاكر منها؟ 🤔<br />\r\n⚡ للناس اللي بتحب الكتب والمراجع تقدر تبدأ تذاكر من كتب كتير 👇🏻<br />\r\nلو انت مش عارف&nbsp; برمجة&nbsp; وحابب تبدأ من الصفر ف هتلاقي أول مرجعين دول كويسين جدََا كبداية يعني تقدر تبدأ بأي واحد منهم&nbsp;<br />\r\n&nbsp;1-Java how to program 10th edition<br />\r\n2- introduction to Java 10th edition&nbsp;<br />\r\nوطبعََا في كتب كتير تانية زي&nbsp; &nbsp;<br />\r\n3- Big Java Early objects&nbsp; 6th edition&nbsp;<br />\r\nوالكتاب دا جميل جدََا ومستواه أعلى شوية من اللي فوق ومهتم أكتر بال OOP&nbsp;<br />\r\n4- Core Java volume i<br />\r\nو الكتاب دا من أحسن الكتب اللي هتخليك توصل لمرحلة قوية جدََا في Java SE وبيشرح كل حاجة بالتفصيل بس ميفضلش أنك تبدأ بيه يعني&nbsp;<br />\r\n5-&nbsp; &nbsp;Core Java Volume ii<br />\r\nو دا الجزء التاني بتاع Core Java Volume&nbsp; i وطبعََا الكتاب دا فيه حاجات مهمة جدََا زي&nbsp;<br />\r\n📎Security&nbsp;<br />\r\n📎 Network programming<br />\r\n📎 Scripting&nbsp;<br />\r\n6- Java the complete reference<br />\r\nالكتاب دا&nbsp; تبع oracle اللي هي الشركة المسؤولة عن Java حاليََا&nbsp; وبرضو فيه حاجات حلوة ممكن تبقي ترجعلها قدام.&nbsp;<br />\r\n⚡ و للناس اللي بتفضل ال Videos&nbsp; العربي تقدر تتابع دول 👇🏻<br />\r\n&diams;️ https://bit.ly/3k8tFoQ<br />\r\nعبدالله عيد Java (101)<br />\r\n&diams;️ https://bit.ly/2DeVF9J<br />\r\nعبدالله عيد Java (102)<br />\r\n&diams;️ https://bit.ly/3gA17Cs<br />\r\nحسونة أكاديمي (Java Zero to Hero )<br />\r\n&diams;️ https://bit.ly/33mzKrY<br />\r\nمحمد عيسى(Java programming)&nbsp;<br />\r\n&diams;️ https://bit.ly/39OZqyJ<br />\r\nمحمد عيسى(Java Advanced )<br />\r\n&diams;️ https://bit.ly/2Xm5OZ3&nbsp;<br />\r\nحسين الربيعي(Java basic to OPP )<br />\r\n⚡ ولو أنت بتفضل الانجلش تقدر برضو تتابع&nbsp; دول 👇🏻<br />\r\n&diams;️ https://bit.ly/3fn4Kuq<br />\r\n(Java By Bucky Roberts )&nbsp;<br />\r\n&diams;️ https://bit.ly/3fn4FqC<br />\r\n(Java tutorial for Beginners)&nbsp;<br />\r\n&diams;️ https://bit.ly/3fpjLeW&nbsp;<br />\r\n(Java essential training)&nbsp;<br />\r\n⚡ وكمان لو حابب تعرف أكتر وتذاكر اكتر عن ال Java تقدر تشوف الموقعين دول هيفيدوك جدََا 👇🏻<br />\r\n&diams;️ Java tpoint&nbsp;<br />\r\nhttps://www.javatpoint.com&nbsp;<br />\r\n&diams;️Java tutorial&nbsp;<br />\r\nhttps://www.tutorialspoint.com/java/index.htm&nbsp;<br />\r\n🖇️وعشان تقدر تبدأ تطبق أنت محتاج تنزل IDE أو البرنامج اللي هتشتغل عليه، وطبعََا في أكتر من IDE تقدر تشتغل عليهم&nbsp; Java زي 👇🏻<br />\r\n💥Netbeans<br />\r\n💥Eclipse<br />\r\n💥 IntelliJ IDEA Community Edition<br />\r\n🖇️وفي حاجة مهمة&nbsp; أنت محتاج تعرفها اسمها ال JDK وهي اختصار ل&nbsp; Java Development Kit&nbsp; وهي عبارة عن حاجة كبيرة&nbsp; كدا بتحتوي علي tools بتاعة ال Java اللي لازم تكون موجودة علي جهازك واللي بتساعدنا في عملية ال Development&nbsp; ف طبعََا لازم تنزلها على&nbsp; جهاز الكمبيوتر بتاعك مع البرنامج اللي هتطبق عليه وبعدها تقدر تبدأ تكتب الكود بتاعك 👨🏻&zwj;💻<br />\r\nوبكدا نكون خلصنا انهاردة&nbsp; رحلتنا عن ال Java SE، استنونا في الرحلة الجاية واللي هناخدكم فيها ال Java EE وال Java ME وإن شاء الله هتستمتعوا بيها 😃💖</p>\r\n\r\n<p>#Java<br />\r\n#MUFIX_Community98</p>\r\n\r\n<p>الفترة دي بشكل كبير جدََا، ومن بداية ظهورها وهي غيرت فكرة شركات كتير جدََا في السوق خصوصََا شركات ال &quot;Hardware&quot; وال &quot;Software&quot; في طريقة الإنتاج والتعامل مع العملاء.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>تعالوا نعرف ازاي قدر ال &quot;Cloud computing&quot; يعمل تغيير في عالمنا؟! 👀</p>\r\n\r\n<p>وعشان أفكارك متتلغبطش تعالي ناخدها خطوة خطوة 😃</p>\r\n\r\n<p>▪️ اول حاجة محتاجين نعرفها يعني أي &quot;Cloud computing&quot;؟!...</p>\r\n\r\n<p>كلمة &quot;Cloud computing&quot; معناها سحابة حاسوبية وهي بختصار كدا عبارة عن تجمع بعض الخدمات بحيث إنها تسهل على الناس القيام بحاجات كتير..&nbsp;</p>\r\n\r\n<p>▪️اكيد جه في بالك اي هي أصلََا مكوناتها؟!..&nbsp;</p>\r\n\r\n<p>تعالي نعرف أي هي مكونات ال&quot;Cloud computing&quot; ونتكلم عن كل مكون منهم.&nbsp;</p>\r\n\r\n<p>1- Applications</p>\r\n\r\n<p>هي البرامج والخدمات اللي يقدر يشغلها العميل في السحابة&nbsp;</p>\r\n\r\n<p>2- Client</p>\r\n\r\n<p>هو المستخدم اللي بيستخدم جهازه ( سواء كان موبايل او كمبيوتر للاستفادة من الخدمة .</p>\r\n\r\n<p>3- Infrastructure</p>\r\n\r\n<p>هي البنية التحتية للسحابة</p>\r\n\r\n<p>4 - Platform</p>\r\n\r\n<p>هي المنصة التي تستخدمها في السحابة , زي Python Django , Java Google Web Toolkit في جوجل.</p>\r\n\r\n<p>5- Service</p>\r\n\r\n<p>هي الخدمة اللي بتستخدمها على السحابة، وهي عملية تحويل منتجات الحاسب إلى خدمـات.</p>\r\n\r\n<p>▪️ بعد ما عرفنا أي هو ال&quot;Cloud computing&quot; وأي مكوناتها جه وقت أنك تعرف هي بدأت ازاي تنتشر في العالم 🤔</p>\r\n\r\n<p>▪️تعالي ناخد رحلة في بداية ال&quot;Cloud Computing&quot;..&nbsp;</p>\r\n\r\n<p>بدأت رحلة ال &quot;Cloud computing&quot; مع شركات كبيرة لما بدأت الأسعار تزيد والمبيعات بتاعتها تقل ومن هنا بدأت فكرة التأجير واللي بيتبني عليها ال &quot;Cloud computing&quot; ومن هنا بدأ حل المشاكل اللي بتواجه الشركات عن طريق السحابة الحاسوبية وهما 3 حلول هما :</p>\r\n\r\n<p>🔸ال &quot;SAAS&quot; اختصار ل &quot;Software as a service&quot;&nbsp;</p>\r\n\r\n<p>احنا كل يوم بنتعامل معاه وإحنا مش واخدين بالنا زي ال&quot;Gmail&quot; و&quot;Google Drive&quot; دي كلها عبارة عن online service وإحنا بنستخدمها يوميََا.&nbsp;</p>\r\n\r\n<p>من مميزاتها :&nbsp;</p>\r\n\r\n<p>🔹التوفير فهي بتوفرلك كتير لأن زي ما قولنا التكلفة بتكون عالية جدََا&nbsp;</p>\r\n\r\n<p>🔹مش بتحتاج أنك تعمل Set up أنت بتقدر تستخدم المنصة دي من غير ما تنزلها.&nbsp;</p>\r\n\r\n<p>🔹تجنب مشاكل الإدارة لأنها بتكون من مسؤولية الشركة اللي بتوفر الخدمة دي.&nbsp;</p>\r\n\r\n<p>🔹ال&quot;Backup&quot; و &quot;Restore&quot; وكل ال&quot;Data&quot; تبقى من مسؤولية الشركة وبتقدر تلاقي بياناتك موجودة ف أي وقت.&nbsp;</p>\r\n\r\n<p>🔸️ ال&quot;PAAS&quot; وهو اختصار ل &quot;Platform as a service&quot;&nbsp;</p>\r\n\r\n<p>هي عبارة عن شركة بتقدم بيئة عمل متكاملة بتتعامل بسهولة مع ال &quot;Server&quot; عن طريق ال&quot;control panel&quot; و بتخليك قادر تغير أو تعدل في ال &quot; Setting&quot; زي مانت عايز وامثلة على كدا ال &quot;Google Cloud&quot; وال Microsoft egor&quot; وبالتالي أنت بتقدر تتحكم في كل حاجة في ال &quot;Server&quot;&nbsp;</p>\r\n\r\n<p>مميزاتها :</p>\r\n\r\n<p>🔹بيئة عمل متكاملة بتخليك مسؤول عن كل حاجة وتقدر تعمل كل حاجة أنت عاوزها&nbsp;</p>\r\n\r\n<p>🔹سهولة الإستخدام</p>\r\n\r\n<p>🔹بتقدر تعمل &quot; Upgrade&quot; و &quot;Downgrade&quot; لكل الإمكانيات من غير اي ضرر للداتا&nbsp;</p>\r\n\r\n<p>🔹أي حاجة خاصة بال &quot;Server&quot; من ال &quot;Backup&quot; أو &quot;Restore&quot; فهي من مسؤولية الشركة&nbsp;</p>\r\n\r\n<p>🔸️ ال&quot;IAAS &quot; واللي هي اختصار ل &quot;Infrastructure as a service&quot;&nbsp;</p>\r\n\r\n<p>وهي عبارة عن شركة بتقدملك &quot;Server&quot; بكل حاجة محتاجها و بتقدملك كمان ال &quot;CPU&quot; وال&quot;RAM&quot;، كمان بتقدملك &quot;Internet&quot; بشكل كويس&nbsp;</p>\r\n\r\n<p>ودي اللي بيقدر يستخدمها &quot;Server Admins&quot; أو شخص لازم يكون عنده خبرة في مجال ال &quot;Server Management&quot;&nbsp;</p>\r\n\r\n<p>من مميزاتها :</p>\r\n\r\n<p>🔹سعره ارخص&nbsp;</p>\r\n\r\n<p>🔹بتقدر تتحكم في ال &quot;Server&quot; بتاعك&nbsp;</p>\r\n\r\n<p>🔹أسرع وإمكانياته أكتر&nbsp;</p>\r\n\r\n<p>▪️ وبعد ما اتكلمنا عن الحلول اللي قدرت تقدمها لينا ال&quot;Cloud computing&quot; دلوقتى تعالوا نعرف هي أي انواعها؟!..&nbsp;</p>\r\n\r\n<p>ال&quot;Cloud computing&quot; ليه 3 أنواع طب أي هما الأنواع دي؟!🤔</p>\r\n\r\n<p>🔺ال &quot;Public Cloud&quot;&nbsp;</p>\r\n\r\n<p>وهي أن الشركة بتقدم الخدمات دي العامة وبيطلق على الشركة دي اسم &quot;Third party&quot; يعني الشركة دي بتقدر كمان إنها تقدم الخدمات دي لشركات تانية عشان تحصل على رسوم الاستخدام.&nbsp;</p>\r\n\r\n<p>🔺ال &quot;Private Cloud&quot;&nbsp;</p>\r\n\r\n<p>تعتبر خاصة بفئة معينة من الأشخاص والشركات وهي تعتبر مركز البيانات.&nbsp;</p>\r\n\r\n<p>🔺ال &quot; Hybrid Cloud&quot;&nbsp;</p>\r\n\r\n<p>وهي عبارة عن مزيج بين العامة والخاصة ودا عشان يتم الربط بين الخدمات عن طريق التقنيات الجديدة عشان تقدر توصل للخدمات بشكل عام.&nbsp;</p>\r\n\r\n<p>بعد ما وصلنا لنهاية البوست بسلام دلوقتى نقدر نقول إنها ساعدت بشكل كبير جدََا في توفير ومساعدة شركات كتير ومش بس الشركات لا كمان أشخاص كتير من مستخدمي الشركات دي بشكل مجاني من غير تكلفة كبير 🔥👏🏻</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>#MUFIX_Community</p>\r\n\r\n<p>#Cloud_Computing</p>', 3, 1, 'jfElrXzhsu6NZK0rggHWvbSOnX92yL25cFLmVzHV.png', 1, 'ltr', '2020-09-13 07:31:05', '2020-09-13 07:31:05'),
(23, 'Testing from mobile by hatem', 'This is post to test error 403 forbidden from the host', '<p>Hello testing</p>', 4, 1, 'vjBkOscZRsEhtI5vLlalSfegzyWkwZWHoJNHhvyv.png', 1, 'ltr', '2020-09-13 08:52:32', '2020-09-13 08:56:38'),
(24, 'Computer Vision', 'مرحبا بكم في الدرس السادس و العشرون من دروس الذكاء الإصطناعي و اليوم مع تطبيق جديد من تطبيقات الذكاء الإصطناعي ألا و هو : رؤية الحاسوب Computer Vision، و هو عبارة عدة مفاهيم', '<p dir=\"rtl\">مرحبا بكم في الدرس السادس و العشرون من دروس الذكاء الإصطناعي و اليوم مع تطبيق جديد من تطبيقات الذكاء الإصطناعي ألا و هو :&nbsp;<strong>رؤية الحاسوب Computer Vision</strong>، و هو عبارة عدة مفاهيم و تقنيات و أفكار متنوعة من عدة مجالات سنذكر منها في هذا الدرس، و لكن المهم في فهم هذا الدرس أنك تراه اليوم و في المستقبل القريب، يستخدم هذا التطبيق من الذكاء الإصطناعي على التعرف على الوجه و البصمة مثلاً و أشياء أخرى سنذكرها أيضاً في هذا الدرس بإذن الله...</p>\r\n\r\n<h1 dir=\"rtl\"><input name=\"aa\" type=\"checkbox\" value=\"aa\" /></h1>\r\n\r\n<p dir=\"rtl\">هي خليط من المفاهيم، و التقنيات و الأفكار من عدة مجالات نذكر منها : معالجات الصور الرقمية Digital Image Processing، و التعرف على الأنماط Patten Recognition، و الذكاء الإصطناعي، و الرسم بالحاسوب أيضاً Computer Graphics.</p>\r\n\r\n<p dir=\"rtl\">و نحن نعلم أن العالم حولنا يتكون من عدة أشكالٍ ثلاثية الأبعاد 3D، و لكن المدخلات إلى العين البشرية و الكاميرات الحاسوبية مثلاً هي ثنائية البعد 2D فقط.</p>\r\n\r\n<p dir=\"rtl\">يوجد بعض البرامج المفيدة التي تعمل ببعدين 2D ، لكن الرؤية الكلية للحاسوب تتطلب معلومات جزئية ثلاثية الأبعاد 3D، و التي ليست فقط مجموعة فقط مجموعة من مشاهد ثنائية البعد 2D.</p>\r\n\r\n<p dir=\"rtl\">و لكن في الوقت الحاظر توجد فقط طرق محدودة لتمثيل المعلومات الثلاثية الأبعاد 3D مباشرةً، و هي ليست جيدة كما هو الحال مع الإستخدامات الواضحة للإنسان.</p>\r\n\r\n<h4 dir=\"rtl\">بعض الأمثلة للدرس Examples :</h4>\r\n\r\n<ul dir=\"rtl\">\r\n	<li><strong>التعرف على الوجه Face Recognition :</strong>&nbsp;تستخدم البنوك و المتاجر الكبيرة كهذه البرامج للتقليل من ... و ...</li>\r\n	<li><strong>القيادة المستقلة Autonomous Driving :</strong>&nbsp;و تستخدم مثلاً في القطارات السريعة و المركبات الذاتية القيادة و الأشياء الأخرى المشابهة و الطائرات الذاتية و هكذا...</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\">و هذه بعض الإستعمالات الأخرى لهذا التطبيق من تطبيقات الذكاء الإصطناعي : التعرف على كتابة اليد Handwriting، فحص الأمتعة Baggage Inspection، اللفحص المصنعي Munufacturing Inspection، و تفسير و فهم الصور أيضاً، و أشياء عديدة جداً سنتمعن فيها أكثر في الدروس القادمة إن شاء الله...</p>', 6, 1, 'FBCHdaXW8ADESHzSZ27JxfRLF7957erQyPvgJpJG.jpeg', 1, 'ltr', '2020-09-13 12:19:38', '2020-09-13 12:26:24');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `tag_id`, `post_id`, `created_at`, `updated_at`) VALUES
(4, 1, 4, NULL, NULL),
(6, 19, 6, NULL, NULL),
(7, 20, 6, NULL, NULL),
(8, 25, 6, NULL, NULL),
(9, 1, 7, NULL, NULL),
(10, 2, 7, NULL, NULL),
(11, 3, 7, NULL, NULL),
(12, 6, 7, NULL, NULL),
(13, 1, 8, NULL, NULL),
(14, 2, 8, NULL, NULL),
(15, 3, 8, NULL, NULL),
(16, 4, 8, NULL, NULL),
(17, 5, 8, NULL, NULL),
(18, 6, 8, NULL, NULL),
(19, 2, 9, NULL, NULL),
(20, 1, 10, NULL, NULL),
(21, 2, 10, NULL, NULL),
(22, 5, 10, NULL, NULL),
(23, 14, 11, NULL, NULL),
(24, 15, 11, NULL, NULL),
(25, 16, 11, NULL, NULL),
(26, 6, 12, NULL, NULL),
(27, 11, 12, NULL, NULL),
(28, 1, 13, NULL, NULL),
(29, 2, 13, NULL, NULL),
(30, 3, 13, NULL, NULL),
(31, 4, 13, NULL, NULL),
(32, 5, 13, NULL, NULL),
(33, 6, 13, NULL, NULL),
(34, 1, 14, NULL, NULL),
(35, 2, 14, NULL, NULL),
(36, 3, 14, NULL, NULL),
(37, 4, 14, NULL, NULL),
(38, 5, 14, NULL, NULL),
(39, 6, 14, NULL, NULL),
(40, 4, 15, NULL, NULL),
(41, 5, 15, NULL, NULL),
(42, 6, 15, NULL, NULL),
(43, 1, 16, NULL, NULL),
(44, 2, 16, NULL, NULL),
(45, 3, 16, NULL, NULL),
(46, 4, 16, NULL, NULL),
(47, 11, 17, NULL, NULL),
(48, 12, 17, NULL, NULL),
(49, 4, 18, NULL, NULL),
(50, 5, 18, NULL, NULL),
(51, 1, 19, NULL, NULL),
(52, 2, 19, NULL, NULL),
(53, 4, 19, NULL, NULL),
(54, 5, 19, NULL, NULL),
(55, 6, 19, NULL, NULL),
(56, 7, 19, NULL, NULL),
(57, 11, 19, NULL, NULL),
(58, 12, 19, NULL, NULL),
(59, 14, 19, NULL, NULL),
(60, 16, 19, NULL, NULL),
(61, 20, 19, NULL, NULL),
(62, 3, 20, NULL, NULL),
(63, 7, 20, NULL, NULL),
(64, 11, 20, NULL, NULL),
(65, 12, 20, NULL, NULL),
(66, 14, 20, NULL, NULL),
(67, 2, 21, NULL, NULL),
(68, 2, 22, NULL, NULL),
(69, 6, 23, NULL, NULL),
(70, 7, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super_admin', 'Super Admin', 'Super Admin', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(2, 'moderator', 'Moderator', 'Moderator', '2020-09-12 19:32:21', '2020-09-12 19:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(2, 3, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `general_site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `general_site_logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_feature_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_feature_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_feature_3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_feature_4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_main_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `communication_fb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `communication_tw` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `communication_inst` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `communication_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `communication_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_feature_1_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_feature_1_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_feature_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_feature_2_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_feature_2_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_feature_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_1_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_1_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_2_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_2_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_3_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_3_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_4_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_4_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_feature_4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `general_site_name`, `general_site_logo`, `community_title`, `community_description`, `community_feature_1`, `community_feature_2`, `community_feature_3`, `community_feature_4`, `community_main_image`, `communication_fb`, `communication_tw`, `communication_inst`, `communication_email`, `communication_address`, `youtube_feature_1_icon`, `youtube_feature_1_title`, `youtube_feature_1`, `youtube_feature_2_icon`, `youtube_feature_2_title`, `youtube_feature_2`, `youtube_video`, `app_feature_1_icon`, `app_feature_1_title`, `app_feature_1`, `app_feature_2_icon`, `app_feature_2_title`, `app_feature_2`, `app_feature_3_icon`, `app_feature_3_title`, `app_feature_3`, `app_feature_4_icon`, `app_feature_4_title`, `app_feature_4`, `app_image`, `created_at`, `updated_at`) VALUES
(1, 'Mufix', 'logo.png', 'For the next great business', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo tempora cumque eligendi in nostrum labore omnis quaerat.', 'Officia quaerat eaque neque', 'Possimus aut consequuntur incidunt', 'Lorem ipsum dolor sit amet', 'Consectetur adipisicing elit', 'main.jpg', '#', '#', '#', 'support@mufix.org', 'Shebin-ElKom Menoufia Egypt', 'fa fa-user', 'Web & Mobile Specialties', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.', 'fa fa-book', 'Intuitive Thinkers', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.', 'https://www.youtube.com/embed/9rIC_X7Zg9k', 'fa fa-heart', 'Web & Mobile Specialties', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.', 'fa fa-times', 'Intuitive Thinkers', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.', 'fa fa-fire', 'Web & Mobile Specialties', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.', 'fa fa-warning', 'Intuitive Thinkers', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.', 'app.png', '2020-09-12 10:53:21', '2020-09-12 10:53:21'),
(2, 'Mufix', 'logo.png', 'For the next great business', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo tempora cumque eligendi in nostrum labore omnis quaerat.', 'Officia quaerat eaque neque', 'Possimus aut consequuntur incidunt', 'Lorem ipsum dolor sit amet', 'Consectetur adipisicing elit', 'main.jpg', '#', '#', '#', 'support@mufix.org', 'Shebin-ElKom Menoufia Egypt', 'fa fa-user', 'Web & Mobile Specialties', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.', 'fa fa-book', 'Intuitive Thinkers', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.', 'https://www.youtube.com/embed/9rIC_X7Zg9k', 'fa fa-heart', 'Web & Mobile Specialties', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.', 'fa fa-times', 'Intuitive Thinkers', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.', 'fa fa-fire', 'Web & Mobile Specialties', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis consect.', 'fa fa-warning', 'Intuitive Thinkers', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.', 'app.png', '2020-09-12 19:32:21', '2020-09-12 19:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'HTML', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(2, 'CSS', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(3, 'JS', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(4, 'PHP', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(5, 'SQL', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(6, 'MYSQL', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(7, 'PYTHON', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(8, 'VUE JS', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(9, 'REACT JS', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(10, 'ANGULAR', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(11, 'C#', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(12, 'C++', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(14, 'JAVAFX', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(15, 'RUBY', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(16, '.NET', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(17, 'SPRING', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(18, 'LARAVEL', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(19, 'CODEIGNITER', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(20, 'GITHUB', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(21, 'GIT', '2020-04-27 08:19:18', '2020-04-27 08:19:18'),
(23, 'X', '2020-09-11 11:32:47', '2020-09-11 11:32:47'),
(24, 'qqqqqqqqqqqqqqqq', '2020-09-11 11:42:50', '2020-09-11 11:42:50'),
(25, 'programing', '2020-09-12 05:16:53', '2020-09-12 05:16:53');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `created_at`, `updated_at`) VALUES
(7, 'HR', '2020-04-28 13:44:39', '2020-04-28 13:44:39'),
(8, 'PR', '2020-04-28 13:44:51', '2020-04-28 13:44:51'),
(9, 'Development', '2020-04-28 13:44:59', '2020-04-28 13:44:59'),
(10, 'Organization', '2020-04-28 13:45:18', '2020-04-28 13:45:18'),
(11, 'Multimedia', '2020-04-28 13:45:24', '2020-05-01 19:15:11'),
(12, 'Social', '2020-04-28 13:45:29', '2020-04-28 13:45:29');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feedback` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `feedback`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Eng Mostafa Saad', 'Thanks a lot for your efforts in organising events and helping students 🙂. It was for my pleasure to have a session @ Menofia University under your organisational efforts.', 'tZrQHKdEnp10EWZTxezkJhgHgwbqu2upUDK10j1Y.jpeg', 1, '2020-08-23 21:58:57', '2020-08-23 21:58:57'),
(4, 'Eng Karim Ebrahim', 'التنظيم والاحترافية والمسؤولية في أبهى صورها الصراحة.\r\nمن أول ما وصلت لحد ما يومي خلص في الـ Event وانا ماحستش خالص بأنه في أى مشكلة مع ان العدد كان كبير , حرفياً كل خطوة كانت بتتعمل كانت بتتعمل بشكل محترم جداا .. \r\nأنا ماشوفتش تيم بيشتغل بالسلاسة دى مع بعض من فترة كبير , فـ أحب اشكرهم فعلاا على المستوى اللى بيقدموه ❤ ❤ \r\nشكراً MUFIX Community ❤', '8FInlUpsIrZdJmK4joooee6Pq4oCF45u23JQFuEk.jpeg', 1, '2020-08-23 22:01:16', '2020-08-23 22:01:16'),
(5, 'Mohamed Elshafie', 'فريق MUFIX من الفرق الرائعة اللى تشرفت بالتعاون معاكم\r\nفريق منظم وشغوف بمساعدة غيره \r\nاتمنى لكم التوفيق فى كل مشروعاتكم', '1UEEkXmm9SyAuOXsrEyOHe9tud5Ds73XOY1zrIJM.jpeg', 1, '2020-08-23 22:03:58', '2020-08-23 22:03:58'),
(6, 'Eng Ahmed Nabil', 'الحقيقة دي مش أول مرة أشارك في حدث من تنظيم نشاط طلابي في الجامعة، لكن فعلا بحيي فريق MUFIX على تنظيمهم الجميل للحدث Software freedom day يوم ٣١/١٠/٢٠١٩ \r\nالشباب فعلا عاملين مجهود وكانوا مهتمين على قد ما يقدروا بالتفاصيل، وكمان حابين اللي بيعملوه، \r\nشكرا ليكم يا شباب وربنا يوفقكم ودائما تقدموا حاجة مفيدة. سعيد جدا إني كنت معاكم. ويشرفني أكون معاكم في أحداث تانية قادمة إن شاء الله', 'inKdYSNFrEHTf9rJaHxAAO2oMS3iCckOIlZiN21F.jpeg', 1, '2020-08-23 22:04:38', '2020-09-02 16:34:26'),
(7, 'Hatem Mohamed', 'Very Nice Team', 'oxb3N4fH9TLrOWXSwXnQhXzhSQ1LIC3SVQBOyoJC.jpeg', 1, '2020-09-02 18:22:21', '2020-09-02 18:22:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'default-user.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `image`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hatem Mohammed', 'Elsheref', 'hatem@app.com', '01090703457', 'default-user.png', NULL, '$2y$10$2KeDC1sfo8smQUhaVpbStePzh2Dm6MoBb74XbBZY9jzGtY3Suw1hq', 'xdRZtiMVsbGldTfPb7nvrf8cOaQUjTKfUHTmkfC42XqgmT1hH3VzTpHL6KdF', '2020-09-12 19:32:21', '2020-09-12 19:32:21'),
(2, 'Hatem', 'Elsheref', 'hatem@mufix.org', '423', 'vbqPINnZeEMCK4PhuTi4IFbdjg0G0dl9JZQgsfen.jpeg', NULL, '$2y$10$YpZ7VnxX86Kw0MBywl2EB.hf46tvOOfF/J6CqL5Je9uUsE/9vPb2q', NULL, '2020-09-12 20:09:40', '2020-09-12 20:09:40'),
(3, 'test', 'mufix', 'mufix@app.com', '42342', 'default-user.png', NULL, '$2y$10$REHlWNVsdV7pf/tyjI1gJOtQbyt2Rz2f/W9eSJDPVlbhVK6OyNsrW', '9RMhuWCOaDFhETZbvmW8mw7YA3aRQ34Gl50k4FztwPrDH9gJMABYWhtENLDh', '2020-09-12 20:39:45', '2020-09-12 22:55:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_posts`
--
ALTER TABLE `activity_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_member`
--
ALTER TABLE `board_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_team`
--
ALTER TABLE `board_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filemanagers`
--
ALTER TABLE `filemanagers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_team`
--
ALTER TABLE `member_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `activity_posts`
--
ALTER TABLE `activity_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `article_tag`
--
ALTER TABLE `article_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `board_member`
--
ALTER TABLE `board_member`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `board_team`
--
ALTER TABLE `board_team`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `filemanagers`
--
ALTER TABLE `filemanagers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `member_team`
--
ALTER TABLE `member_team`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
